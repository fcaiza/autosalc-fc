<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
shell_exec("rm -rf " . dirname(__FILE__) . "/backend/web/assets/*");
require_once dirname(__FILE__) . '/vendor/oyejorge/less.php/lib/Less/Autoloader.php';
Less_Autoloader::register();

$parser = new Less_Parser();
try {
    $origen = dirname(__FILE__) . "/bootstrap/less/bootstrap.less";
    $destino = dirname(__FILE__) . "/vendor/bower-asset/bootstrap/dist/css/bootstrap.css";
    $parser->parseFile($origen);
    $css = $parser->getCss();
    $imported_files = $parser->allParsedFiles();
    file_put_contents($destino, $css);

} catch (Exception $e) {
    $error_message = $e->getMessage();
}


/*
$origen = dirname(__FILE__) . "/bootstrap/less/bootstrap.less";
$destino = dirname(__FILE__) . "/vendor/bower-asset/bootstrap/dist/css/bootstrap.css";
$cadena = "lessc $origen $destino";
$salida = shell_exec($cadena);

$destino = dirname(__FILE__) . "/vendor/bower-asset/bootstrap/dist/css/bootstrap.min.css";
$cadena = "lessc $origen $destino -x";
$salida = shell_exec($cadena);

shell_exec("rm -rf ". dirname(__FILE__) . "/backend/web/assets/*");

     */
