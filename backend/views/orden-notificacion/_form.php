<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
//use backend\modules\transferencia\controllers\AdminSeleccionController;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\OrdenNotificacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orden-notificacion-form">

    <?php $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_HORIZONTAL,
                'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_LARGE]
    ]); ?>

    <?php   $lista=ArrayHelper::map('cambiar-a-modelo-relacional' , 'id', 'nombre-campo');
			echo $form->field($model, 'orden_proceso_id')->widget(Select2::classname(), [
				'data' => $lista,
				'options' => ['placeholder' => 'Seleccione ...'],
				'pluginOptions' => ['allowClear' => true]
			])
 ?>

    <?php echo $form->field($model, 'fecha_envio')->textInput() ?>

    <?php echo $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'mensaje')->textarea(['rows' => 2]) ?>

    <?php echo $form->field($model, 'observacion')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'cliente')->checkbox() ?>

    <?php echo $form->field($model, 'entidad')->checkbox() ?>

    <?php echo $form->field($model, 'adjunto')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
