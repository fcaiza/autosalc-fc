<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\OrdenNotificacion */
?>
<div class="orden-notificacion-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'orden_proceso_id',
            'fecha_envio',
            'titulo',
            'mensaje:ntext',
            'observacion',
            'cliente:boolean',
            'entidad:boolean',
            'adjunto',
        ],
    ]) ?>

</div>
