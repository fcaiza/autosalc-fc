<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Vehiculo */

?>
<div class="vehiculo-create">
    <?= $this->render('_form', [
        'model' => $model,
        'cliente_id' => $cliente_id
    ]) ?>
</div>
