<?php

use kartik\grid\GridView;
use lcaiza\ajaxcrud\AurySolutionsWidget;
use mdm\admin\components\Helper;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\VehiculoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vehiculo';
$this->params['breadcrumbs'][] = $this->title;

?>

<?= AurySolutionsWidget::widget() ?>

<div class="vehiculo-index container-fluid">
    <div id="ajaxCrudDatatable2">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dataProvider,
            'pjax' => true,
            'columns' => require(__DIR__ . '/_columns_clientes.php'),
            'toolbar' => [
                ['content' =>
                    ((Helper::checkRoute('/vehiculo/create')) ? Html::a('<i class="fa fa-plus"></i>', ['/vehiculo/create', 'cliente_id' => $id],
                        ['role' => 'modal-remote', 'title' => 'Crear Nuevo', 'class' => 'btn btn-default']) : '')
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list"></i> Listado de Vehiculos ',
                'before' => '<em>* Para cambiar el tamaño de las columnas arrastre los bordes de columna.</em>',
            ],
            'exportConfig' => [
                GridView::CSV => ['label' => 'CSV'],
                GridView::PDF => [],
                GridView::EXCEL => [],
            ]
        ]) ?>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudDatatable2",
    "footer" => "",// Siempre lo necesitas para plugin jquery
    "options" => ['tabindex' => false],
    "size" => Modal::SIZE_LARGE
]) ?>
<?php Modal::end(); ?>
