<?php

use mdm\admin\components\Helper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'marca_id',
        'value' => 'marca.nombre',
        'enableSorting' => false,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'modelo_id',
        'value' => 'modelo.nombre',
        'enableSorting' => false,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'placa',
        'enableSorting' => false,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'pais_id',
        'value' => 'pais.nombre',
        'enableSorting' => false,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'anio',
        'value' => function ($model) {
            if ($model->anio)
            {
                return \common\models\CatalogoGeneral::findOne($model->anio)->nombre;
            }
            return '';
        },
        'enableSorting' => false,
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'clase_id',
//        'value' => 'clase.nombre',
//        'enableSorting' => false,
//    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'servicio_id',
//        'value' => 'servicio.nombre',
//        'enableSorting' => false,
//    ],
    [
        'class' => '\kartik\grid\BooleanColumn',
        'attribute' => 'matriculado',
        'hAlign' => 'center'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'desde',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'hasta',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'width' => '100px',
        'vAlign' => 'middle',
        'template' => Helper::filterActionColumn('{/vehiculo/view} {/vehiculo/update} {/vehiculo/delete} '),
        'buttons' => [
            '/vehiculo/view' => function ($url, $data) {
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span> ', $url, [
                            'title' => 'Ver',
                            'data-toggle' => 'tooltip',
                            'role' => 'modal-remote',
                ]);
            },
            '/vehiculo/update' => function ($url, $data) {
                return Html::a('<span class="glyphicon glyphicon-pencil"></span> ', $url, [
                            'role' => 'modal-remote',
                            'title' => 'Editar',
                            'data-toggle' => 'tooltip'
                ]);
            },
            '/vehiculo/delete' => function ($url, $data) {
                return Html::a('<span class="glyphicon glyphicon-trash"></span> ', $url, [
                            'role' => 'modal-remote', 'title' => 'Borrar',
                            'data-confirm' => false, 'data-method' => false,
                            'data-request-method' => 'post',
                            'data-toggle' => 'tooltip',
                            'data-confirm-title' => 'Esta usted seguro?',
                            'data-confirm-message' => 'Seguro que quiere borrar este item'
                ]);
            },
        ],
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key, 'bandera' => 1]);
        },
    ],
];
