<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Vehiculo */
?>
<div class="vehiculo-update">

    <?= $this->render('_form', [
        'model' => $model,
        'cliente_id' => $cliente_id
    ]) ?>

</div>
