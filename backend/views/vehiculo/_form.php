<?php

use kartik\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\DepDrop;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\Vehiculo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vehiculo-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_LARGE]
    ]); ?>
    <div class="panel panel-default">
        <div class="container-fluid">
            <div class="panel-body col-sm-6">
                <legend>Información del Vehiculo</legend>
                <?php
                if (!$cliente_id) {
                    $lista = ArrayHelper::map(\common\models\Cliente::findAll(['estado' => 1]), 'id', 'nombres');
                    echo $form->field($model, 'cliente_id')->widget(Select2::classname(), [
                        'data' => $lista,
                        'options' => ['placeholder' => 'Seleccione ...'],
                        'pluginOptions' => ['allowClear' => true]
                    ]);
                }
                ?>

                <?php $lista = ArrayHelper::map(\common\models\Marca::findAll(['estado' => 1]), 'id', 'nombre');
                echo $form->field($model, 'marca_id')->widget(Select2::classname(), [
                    'data' => $lista,
                    'options' => ['placeholder' => 'Seleccione ...'],
                    'pluginOptions' => ['allowClear' => true]
                ])
                ?>
                <?php
                if ($model->marca_id || $model->modelo_id) {
                    $data = ArrayHelper::map(\common\models\Modelo::findAll(['estado' => 1, 'marca_id' => $model->marca_id]), 'id', 'nombre', 'marca.nombre');
                    echo $form->field($model, 'modelo_id')->widget(DepDrop::classname(), [
                        'data' => $data,
                        'pluginOptions' => [
                            'depends' => ['vehiculo-marca_id'],
                            'placeholder' => 'Seleccione...',
                            'url' => Url::to(['modelos'])
                        ],
                        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                        'type' => DepDrop::TYPE_SELECT2,
                    ]);
                } else {
                    echo $form->field($model, 'modelo_id')->widget(DepDrop::classname(), [
                        'pluginOptions' => [
                            'depends' => ['vehiculo-marca_id'],
                            'placeholder' => 'Seleccione...',
                            'url' => Url::to(['modelos'])
                        ],
                        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                        'type' => DepDrop::TYPE_SELECT2,
                    ]);
                }
                ?>

                <?php echo $form->field($model, 'placa')->textInput(['maxlength' => true]) ?>

                <?php $lista = ArrayHelper::map(\common\models\Color::findAll(['estado' => 1]), 'id', 'nombre');
                echo $form->field($model, 'color_1_id')->widget(Select2::classname(), [
                    'data' => $lista,
                    'options' => ['placeholder' => 'Seleccione ...'],
                    'pluginOptions' => ['allowClear' => true]
                ])
                ?>

                <?php $lista = ArrayHelper::map(\common\models\Color::findAll(['estado' => 1]), 'id', 'nombre');
                echo $form->field($model, 'color_2_id')->widget(Select2::classname(), [
                    'data' => $lista,
                    'options' => ['placeholder' => 'Seleccione ...'],
                    'pluginOptions' => ['allowClear' => true]
                ])
                ?>

                <?php echo $form->field($model, 'anio')->dropDownList(ArrayHelper::map(\common\models\CatalogoGeneral::listarCatalogo(11), 'id', 'nombre'), ['prompt' => 'Seleccione']) ?>

                <?php $lista = ArrayHelper::map(\common\models\Pais::findAll(['estado' => 1]), 'id', 'nombre');
                echo $form->field($model, 'pais_id')->widget(Select2::classname(), [
                    'data' => $lista,
                    'options' => ['placeholder' => 'Seleccione ...'],
                    'pluginOptions' => ['allowClear' => true]
                ])
                ?>

            </div>
            <div class="panel-body col-sm-6">
                <legend>Información Adicional</legend>

                <?php $lista = ArrayHelper::map(\common\models\Clase::findAll(['estado' => 1]), 'id', 'nombre');
                echo $form->field($model, 'clase_id')->widget(Select2::classname(), [
                    'data' => $lista,
                    'options' => ['placeholder' => 'Seleccione ...'],
                    'pluginOptions' => ['allowClear' => true]
                ])
                ?>

                <?php $lista = ArrayHelper::map(\common\models\Servicio::findAll(['estado' => 1]), 'id', 'nombre');
                echo $form->field($model, 'servicio_id')->widget(Select2::classname(), [
                    'data' => $lista,
                    'options' => ['placeholder' => 'Seleccione ...'],
                    'pluginOptions' => ['allowClear' => true]
                ])
                ?>

                <?php echo $form->field($model, 'cilindraje')->textInput() ?>

                <?php echo $form->field($model, 'ramv_cpn')->textInput(['maxlength' => true]) ?>

                <?php echo $form->field($model, 'matriculado')->checkbox() ?>

                <?php
                echo $form->field($model, 'desde')->widget(DatePicker::classname(), [
                    'type' => DatePicker::TYPE_RANGE,
                    'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
                    'attribute' => 'desde',
                    'attribute2' => 'hasta',
                    'options' => ['placeholder' => 'Desde', 'value' => ($model->desde ? date('Y-m-d', strtotime($model->desde)) : '')],
                    'options2' => ['placeholder' => 'Hasta', 'value' => ($model->hasta ? date('Y-m-d', strtotime($model->hasta)) : '')],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
//                        'startDate' => date('Y-m-d', strtotime('+1 days ' . date("Y-m-d"))),
                    ],
                    'pluginEvents' => [
                        "changeDate" => "function(e) {  fechaHasta(e.date); }",
                    ]
                ])->label('Fechas Matricula');
                ?>

                <?php echo $form->field($model, 'informacion')->textarea(['rows' => 2]) ?>
            </div>
        </div>
    </div>
    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
<script>
    function fechaHasta(fecha) {
        $("#hasta").kvDatepicker({
            format: 'yyyy-mm-dd',
            clearDates: true,
            autoclose: true,
            startDate: fecha,
        });
    }
</script>
<script>
    $(function () {
        $("#vehiculo-placa").change(function () {
            var nombre = ucwords2($(this).val());
            $(this).val(nombre);
        });
    });

    function ucwords2(str) {
        return str.toUpperCase();
    }

</script>

