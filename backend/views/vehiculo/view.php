<?php

/* @var $this yii\web\View */
/* @var $model common\models\Vehiculo */
?>
<table class="table table-responsive">
    <tr>
        <td>
            <table class="table table-striped table-condensed table-responsive table-bordered">
                <thead>
                <tr>
                    <th colspan="2" class="text-info text-center">
                        Información del Vehiculo
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th><?= $model->getAttributeLabel('cliente_id') ?> </th>
                    <td><?= ($model->cliente_id) ? $model->cliente->getNombres() : '<code> --- </code>' ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('marca_id') ?> </th>
                    <td><?= ($model->marca_id) ? $model->marca->nombre : '<code> --- </code>' ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('modelo_id') ?> </th>
                    <td><?= ($model->modelo_id) ? $model->modelo->nombre : '<code> --- </code>' ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('placa') ?> </th>
                    <td><?= ($model->placa) ? $model->placa : '<code> --- </code>' ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('color_1_id') ?> </th>
                    <td><?= ($model->color_1_id) ? $model->color1->nombre : '<code> --- </code>' ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('color_2_id') ?> </th>
                    <td><?= ($model->color_2_id) ? $model->color2->nombre : '<code> --- </code>' ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('anio') ?> </th>
                    <td><?= ($model->anio) ? \common\models\CatalogoGeneral::findOne($model->anio)->nombre : '<code> --- </code>' ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('pais_id') ?> </th>
                    <td><?= ($model->pais_id) ? $model->pais->nombre : '<code> --- </code>' ?></td>
                </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="table table-striped table-condensed table-responsive table-bordered">
                <thead>
                <tr>
                    <th colspan="2" class="text-info text-center">
                        Información Adicional
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th><?= $model->getAttributeLabel('clase_id') ?> </th>
                    <td><?= ($model->clase_id) ? $model->clase->nombre : '<code> --- </code>' ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('servicio_id') ?> </th>
                    <td><?= ($model->servicio_id) ? $model->servicio->nombre : '<code> --- </code>' ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('cilindraje') ?> </th>
                    <td><?= ($model->cilindraje) ? $model->cilindraje : '<code> --- </code>' ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('ramv_cpn') ?> </th>
                    <td><?= ($model->ramv_cpn) ? $model->ramv_cpn : '<code> --- </code>' ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('matriculado') ?> </th>
                    <td><?= ($model->matriculado) ? 'Si' : 'No' ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('desde') ?> </th>
                    <td><?= ($model->desde) ? $model->desde : '<code> --- </code>' ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('hasta') ?> </th>
                    <td><?= ($model->hasta) ? $model->hasta : '<code> --- </code>' ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('informacion') ?> </th>
                    <td><?= ($model->informacion) ? $model->informacion : '<code> --- </code>' ?></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
