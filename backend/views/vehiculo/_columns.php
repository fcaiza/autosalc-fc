<?php

use kartik\grid\GridView;
use mdm\admin\components\Helper;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'cliente_id',
        'group' => true,
        'value' => 'cliente.nombres',
        'filter' => yii\helpers\ArrayHelper::map(\common\models\Cliente::find()->where(['estado' => 1])->all(), 'id', 'nombres'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true, 'placeholder' => 'Cliente'],
        ],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'marca_id',
        'value' => 'marca.nombre',
        'filter' => yii\helpers\ArrayHelper::map(\common\models\Marca::find()->where(['estado' => 1])->all(), 'id', 'nombre'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true, 'placeholder' => 'Marca'],
        ],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'modelo_id',
        'value' => 'modelo.nombre',
        'filter' => yii\helpers\ArrayHelper::map(\common\models\Modelo::find()->where(['estado' => 1])->all(), 'id', 'nombre'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true, 'placeholder' => 'Modelo'],
        ],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'placa',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'pais_id',
        'value' => 'pais.nombre',
        'filter' => yii\helpers\ArrayHelper::map(\common\models\Pais::find()->where(['estado' => 1])->all(), 'id', 'nombre'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true, 'placeholder' => 'Pais'],
        ],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'anio',
        'value' => function ($model) {
            if ($model->anio) {
                $anioD = \common\models\CatalogoGeneral::findOne($model->anio);
                return ($anioD) ? $anioD->nombre : '';
            }
            return '';
        },
        'filter' => yii\helpers\ArrayHelper::map(\common\models\CatalogoGeneral::listarCatalogo(11), 'id', 'nombre'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true, 'placeholder' => 'Año'],
        ],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'clase_id',
        'value' => 'clase.nombre',
        'filter' => yii\helpers\ArrayHelper::map(\common\models\Clase::find()->where(['estado' => 1])->all(), 'id', 'nombre'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true, 'placeholder' => 'Clase'],
        ],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'servicio_id',
        'value' => 'servicio.nombre',
        'filter' => yii\helpers\ArrayHelper::map(\common\models\Servicio::find()->where(['estado' => 1])->all(), 'id', 'nombre'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true, 'placeholder' => 'Servicio'],
        ],
    ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'color_1_id',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'color_2_id',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'cilindraje',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'ramv_cpn',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'matriculado',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'desde',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'hasta',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'informacion',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'created_at',
//         'format' => ['DateTime', 'php: Y-m-d H:i:s'],
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'updated_at',
//         'format' => ['DateTime', 'php: Y-m-d H:i:s'],
//     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'width' => '100px',
        'vAlign' => 'middle',
        'template' => Helper::filterActionColumn('{view} {update} {delete} '),
        'buttons' => [
            'ejemplo' => function ($url, $data) {
                return Html::a('<span class="fa fa-asterisk"></span> ', $url, [
                    'title' => 'Titulo', 'data-toggle' => 'tooltip',
                    'role' => 'modal-remote',
                ]);
            },
        ],
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Ver', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Editar', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Borrar',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Esta usted seguro?',
            'data-confirm-message' => 'Seguro que quiere borrar este item'],
    ],

];   