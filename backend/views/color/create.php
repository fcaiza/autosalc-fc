<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Color */

?>
<div class="color-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
