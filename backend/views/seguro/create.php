<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Seguro */

?>
<div class="seguro-create">
    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user
    ]) ?>
</div>
