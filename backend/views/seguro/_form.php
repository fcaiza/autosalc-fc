<?php

use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

//use backend\modules\transferencia\controllers\AdminSeleccionController;

/* @var $this yii\web\View */
/* @var $model common\models\Seguro */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seguro-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_LARGE]
    ]); ?>
    <div class="panel panel-default">
        <div class="container-fluid">
            <div class="panel-body col-sm-6">
                <legend>Datos Seguro</legend>

                <?php echo $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

                <?php echo $form->field($model, 'ruc')->textInput(['maxlength' => true]) ?>

                <?php
                $pasoName = '';
                $nameGuardado = 'seguro' . date('YmdHis');
                if (!$model->isNewRecord) {
                    $data = [];
                    if ($model->imagen) {
                        $pasoName = $model->imagen;
                        $path = '/img/seguro/' . $model->id;
                        $data = \backend\controllers\GlobalController::ObtenerImagenModelo($path, $model->imagen);
                    }
                    echo $form->field($model, "imagen")->widget(\kartik\widgets\FileInput::className(), [
                        'options' => [
                            'id' => 'imageFile',
                            'multiple' => false,
                        ],
                        'pluginOptions' => [
                            'maxFileSize' => 500,
                            'initialPreview' => ($data) ? $data[0] : '',
                            'initialPreviewConfig' => ($data) ? $data[1] : '',
                            'showUpload' => false,
                            'previewFileType' => 'any',
                            'initialPreviewAsData' => true,
                            'allowedFileExtensions' => ["png", "jpg", "jpeg"],
                            'uploadUrl' => yii\helpers\Url::to(['/global/guardar-generico-doc-img', 'id' => $model->id, 'tipo' => '1', 'model' => 'common\models\Seguro', 'path' => 'img/seguro', 'img' => 1, 'name' => $nameGuardado, 'atribbute' => 'imagen']),
                            'deleteUrl' => \yii\helpers\Url::to(['/global/delete-img', 'id' => $model->id, 'tipo' => '1', 'model' => 'common\models\Seguro', 'path' => 'img/seguro', 'img' => 1, 'name' => $nameGuardado, 'atribbute' => 'imagen']),
                        ]
                    ]);
                }
                ?>

            </div>
            <div class="panel-body col-sm-6">
                <legend>Información Usuario</legend>
                <?php echo $form->field($user, 'username')->textInput(['maxlength' => true]) ?>

                <?php echo $form->field($user, 'email')->textInput(['maxlength' => true]) ?>

                <?php echo $form->field($model, 'estado')->dropDownList(ArrayHelper::map(\common\models\CatalogoGeneral::listarCatalogo(1), 'value', 'nombre'), ['prompt' => 'Seleccione']) ?>

            </div>
        </div>
    </div>

    <?php echo Html::input('hidden', "imagen", '', ['id' => 'imagen']); ?>
    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    $(function () {
        $('.kv-file-remove').show();
        var paso = '<?= $pasoName ?>';
        $('#imagen').val(paso);
        var fileName = null;
        var fileUpload = $('#imageFile');

        fileUpload.on('filebeforedelete', function (e) {
            var aborted = !window.confirm('Esta seguro de borrar el archivo ?');
            return aborted;
        });
        fileUpload.on('filedeleted', function (e) {
            $.pjax.reload({container: '#crud-datatable-pjax', timeout: false});
            $('#ajaxCrudModal').modal('hide');
        });
        fileUpload.on('fileuploaded', function (event, data, previewId, index) {
            var img = data.files[0].name;
            $('#imagen').val('');
            var file = img;
            var fileName = '<?= $nameGuardado ?>';
            var clean = file.split('\\').pop();
            var punto = clean.split('.').pop();
            $('#imagen').val(fileName + "." + punto.toLowerCase());
        }).on("filebatchselected", function (event, files) {
            $('.kv-file-upload ').click();
            $('.kv-file-remove').hide();
        });
        fileUpload.on('filebatchpreupload', function (event, data) {
            var img = data.files[0].name;
            $('#imagen').val('');
            var file = img;
            var fileName = '<?= $nameGuardado ?>';
            var clean = file.split('\\').pop();
            var punto = clean.split('.').pop();
            $('#imagen').val(fileName + "." + punto.toLowerCase());
        });
        fileUpload.change(function () {
            $('#imagen').val('');
            var file = $(this).val();
            var fileName = '<?= $nameGuardado ?>';
            var clean = file.split('\\').pop();
            var punto = clean.split('.').pop();
            $('#imagen').val(fileName + "." + punto.toLowerCase());
        });

        $("#seguro-nombre").blur(function () {
            var random = guid();
            var nombre = $(this).val().substring(0, 4).toUpperCase();
            $("#user-username").val(nombre + '-' + random);
        });

        $("#seguro-nombre").change(function () {
            var nombre = ucwords($(this).val());
            $(this).val(nombre);
        });
    });

    function ucwords(str) {
        return (str + '')
            .replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
                return $1.toUpperCase();
            });
    }

    function guid() {
        var sGuid = "";
        for (var i = 0; i < 6; i++) {
            sGuid += Math.floor(Math.random() * 0xF).toString(0xF).toUpperCase();
        }
        return sGuid;
    }

</script>


