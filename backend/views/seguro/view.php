<?php

/* @var $this yii\web\View */
/* @var $model common\models\Seguro */
?>
<div class="seguro-view">
    <table class="table table-striped table-condensed table-responsive table-bordered">
        <thead>
        <tr>
            <th colspan="2" class="text-info text-center">
                Información del Seguro
            </th>
        </tr>
        </thead>
        <tbody>
        <tr class="text-center">
            <td colspan="2">
                <?php
                $img = ($model->imagen ? Yii::getAlias("@web") . '/img/seguro/' . $model->id . '/' . $model->imagen : Yii::getAlias("@web") . '/img/sin-imagen.jpg');
                ?>
                <img src="<?= $img ?>" alt="" width="150px" height="150px">
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('nombre') ?> </th>
            <td><?= ($model->nombre) ? $model->nombre : '<code> --- </code>' ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('ruc') ?> </th>
            <td><?= ($model->ruc) ? $model->ruc : '<code> --- </code>' ?></td>
        </tr>
        <tr>
            <th><?= $user->getAttributeLabel('username') ?> </th>
            <td><?= ($user->username) ? $user->username : '<code> --- </code>' ?></td>
        </tr>
        <tr>
            <th><?= $user->getAttributeLabel('email') ?> </th>
            <td><?= ($user->email) ? $user->email : '<code> --- </code>' ?></td>
        </tr>
        <tr>
            <th>Creacion </th>
            <td><?= ($model->created_at) ? date('Y-m-d H:i:s',$model->created_at) : '<code> --- </code>' ?></td>
        </tr>
        <tr>
            <th>Actualizacion </th>
            <td><?= ($model->updated_at) ? date('Y-m-d H:i:s',$model->updated_at) : '<code> --- </code>' ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('estado') ?> </th>
            <td><?= ($model->estado) ? 'Activo' : 'Inactivo' ?></td>
        </tr>
        </tbody>
    </table>
</div>
