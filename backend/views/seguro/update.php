<?php

/* @var $this yii\web\View */
/* @var $model common\models\Seguro */
?>
<div class="seguro-update">

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user
    ]) ?>

</div>
