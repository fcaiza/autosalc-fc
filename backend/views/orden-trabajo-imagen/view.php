<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\OrdenTrabajoImagen */
?>
<div class="orden-trabajo-imagen-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'orden_trabajo_id',
            'imagen',
            'envio_email:boolean',
			[
                'label' => 'Estado',
                'value' => ($model->estado == 1) ? 'Activo' : 'Inactivo',
            ],
        ],
    ]) ?>

</div>
