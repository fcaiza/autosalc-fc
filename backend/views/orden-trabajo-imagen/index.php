<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use lcaiza\ajaxcrud\CrudAsset; 
use lcaiza\ajaxcrud\BulkButtonWidget;
use mdm\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\OrdenTrabajoImagenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orden Trabajo Imagen';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="orden-trabajo-imagen-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                ['content'=>
                    ((Helper::checkRoute('create')) ? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
                    ['role'=>'modal-remote','title'=> 'Crear Nuevo','class'=>'btn btn-default']) : '').
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Recargar Grid']).
                    '{toggleData}'.
                    '{export}'
                ],
            ],          
            'striped' => true,
            'condensed' => true,
            'responsive' => true,          
            'panel' => [
                'type' => 'primary', 
                'heading' => '<i class="glyphicon glyphicon-list"></i> Listado de Orden Trabajo Imagen ',
                'before'=>'<em>* Para cambiar el tamaño de las columnas arrastre los bordes de columna.</em>',
            ],
            'exportConfig' => [
                GridView::CSV => ['label' => 'CSV'],
                GridView::PDF => [],
                GridView::EXCEL => [],
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// Siempre lo necesitas para plugin jquery
    "options"=>['tabindex' => false ],
])?>
<?php Modal::end(); ?>
