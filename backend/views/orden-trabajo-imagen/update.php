<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OrdenTrabajoImagen */
?>
<div class="orden-trabajo-imagen-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
