<?php

use yii\widgets\DetailView;
use yiister\gentelella\widgets\Panel;

/* @var $this yii\web\View */
/* @var $model common\models\OrdenProceso */
?>
<style>
    p{
        margin: 0;
        padding: 0;
    }
</style>
<div class="orden-view">

    <?php
    Panel::begin(
        [
            'header' => 'Datos de la Orden',
            'icon' => 'cog',
            'options' => [
                'class' => "x_panel data-orden"
            ]
        ]
    )
    ?>
    <div class="row">
        <div class="col-lg-6">
            <p>
                <label>Aseguradora :</label> <span><?= ($model->orden->seguro_id) ? $model->orden->seguro->nombre : '' ?></span>
            </p>
            <p>
                <label>Broker :</label> <span><?= ($model->orden->broker_id) ? $model->orden->broker->nombre : '' ?></span>
            </p>
            <p>
                <label>Empleado Recibe :</label> <span><?= ($model->orden->empleado_recibe_id) ? $model->orden->empleadoRecibe->nombres : '' ?></span>
            </p>
            <p>
                <label>Fecha Recibe :</label> <span><?= ($model->orden->fecha_recibe) ? $model->orden->fecha_recibe : '' ?></span>
            </p>
            <p>
                <label>Fecha Salida :</label> <span><?= ($model->orden->fecha_salida) ? $model->orden->fecha_salida : '' ?></span>
            </p>
            <p>
                <label>Observacion :</label> <?= ($model->orden->observacion_ingreso) ? $model->orden->observacion_ingreso : '' ?>
            </p>

        </div>
        <div class="col-lg-6">
            <p>
                <label>Cliente :</label> <span><?= $model->orden->cliente->nombres ?></span>
            </p>
            <p>
                <label>Direccion :</label> <span><?= ($model->orden->cliente->direccion) ? $model->orden->cliente->direccion : '' ?></span>
            </p>
            <p>
                <label>Telefono :</label> <span><?= ($model->orden->cliente->telefono) ? $model->orden->cliente->telefono : '' ?></span>
            </p>
            <p>
                <label>Celular :</label> <span><?= ($model->orden->cliente->celular) ? $model->orden->cliente->celular : '' ?></span>
            </p>
            <p>
                <label>Correo :</label> <span><?= ($model->orden->cliente->user) ? $model->orden->cliente->user->email : '' ?></span>
            </p>
            <p>
                <label>Vehiculo :</label> <span><?= ($model->orden->vehiculo->marca) ? $model->orden->vehiculo->marca->nombre : '' ?></span>
            </p>
            <p>
                <label>Placa :</label> <span><?= ($model->orden->vehiculo_id) ? $model->orden->vehiculo->placa : '' ?></span>
            </p>
        </div>
    </div>
    <hr />
    <table class="table table-condensed table-striped">
        <thead>
        <tr>
            <th class="col-lg-1">Item</th>
            <th class="col-lg-2">Area</th>
            <th>Observacion</th>
            <th class="col-lg-2">Fecha Inicio</th>
            <th class="col-lg-2">Fecha Fin</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if ($model->orden->ordenProcesos):
            $ordenProceso = common\models\OrdenProceso::find()
                ->where(['orden_id' => $model->orden->id])
                ->orderBy(['fase' => SORT_ASC])
                ->all();
            foreach ($ordenProceso as $proceso):
                ?>
                <tr>
                    <td><?= $proceso->fase ?></td>
                    <td><?= $proceso->proceso->nombre ?></td>
                    <td><?= $proceso->observacion ?></td>
                    <td><?= date('Y-m-d',strtotime($proceso->fecha_inicio)) ?></td>
                    <td><?= date('Y-m-d',strtotime($proceso->fecha_fin)) ?></td>
                </tr>
            <?php
            endforeach;
        else:
            echo "<tr><td colspan='5' class='text-danger text-center'>No existen datos para mostrar</td></tr>";
        endif;
        ?>
        </tbody>
    </table>
    <?php
    $directory = Yii::getAlias("@backend") . "/web/uploads/orden/$model->orden_id/galery/img/";
    if (is_dir($directory)):
        ?>
        <div class="row">
            <h4 class="text-center">Fotos del vehiculo <small>ingreso a los talleres</small></h4>
            <?php
            $images = glob("$directory{*.gif,*.jpg,*.png,*.jpeg}", GLOB_BRACE);
            foreach ($images as $v)
            {
                $nombre = str_replace($directory, '', $v);
                $img = Yii::getAlias("@web") . "/uploads/orden/$model->orden_id/galery/img/$nombre";
                echo '<img src="' . $img . '" border="0" style="width:150px;float:left;margin:5px;" />';
            }
            ?>
        </div>
    <?php
    endif;
    ?>
    <?php Panel::end() ?>
</div>
