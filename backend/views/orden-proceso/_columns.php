<?php
use yii\helpers\Url;
use mdm\admin\components\Helper;
use kartik\grid\GridView;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fase',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'orden_id',
        'value'=>'orden.numero_orden',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'proceso_id',
        'value'=>'proceso.nombre',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'responsable_id',
//    ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'asignado_id',
//     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'fecha_inicio',
     ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'observacion',
    // ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'fecha_fin',
     ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'resultado',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'notificacion',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
        // 'format' => ['DateTime', 'php: Y-m-d H:i:s'],
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_at',
        // 'format' => ['DateTime', 'php: Y-m-d H:i:s'],
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'width' => '100px',
        'vAlign'=>'middle',
        'template' => Helper::filterActionColumn('{view} {update}'),
        'buttons' => [
             'ejemplo' => function ($url, $data) {
                return Html::a('<span class="fa fa-asterisk"></span> ', $url, [
                        'title' => 'Titulo', 'data-toggle' => 'tooltip',
                        'role' => 'modal-remote',
                    ]);
                },
        ],
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Ver','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Editar', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Borrar', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Esta usted seguro?',
                          'data-confirm-message'=>'Seguro que quiere borrar este item'], 
    ],

];   