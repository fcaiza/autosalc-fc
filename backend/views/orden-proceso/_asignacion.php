<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
//use backend\modules\transferencia\controllers\AdminSeleccionController;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\OrdenProceso */
/* @var $form yii\widgets\ActiveForm */

if($error){
  ?>
    <div class="alert alert-danger">
        <strong>Error!</strong> <?=$error?>.
    </div>
<?php
}
?>

<div class="orden-proceso-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_LARGE]
    ]); ?>


    <?php
    echo $form->field($model, 'asignado_id')->widget(Select2::classname(), [
        'data' => ($lista) ? $lista :[],
        'options' => ['placeholder' => 'Seleccione ...'],
        'pluginOptions' => ['allowClear' => true]
    ])
    ?>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
