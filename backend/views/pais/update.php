<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Pais */
?>
<div class="pais-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
