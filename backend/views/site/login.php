<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Inicio de Sesion';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .message {
        /*        position: absolute;
                top: 20%;
                left: 50%;
                width: 640px;
                height: 120px;
                margin-left: -320px;
                margin-top: -60px;
                color: gray;*/
        /*opacity: 0.4;*/
    }
</style>

<div class="container-fluid">   
    <div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
        <p align="center" style="padding-top: 50px">
            <img src="<?= Yii::getAlias("@web") ?>/img/autosalc/logo-transparente.png" class="img-responsive"  width="450px"/>   
        </p>

        <h1><?= Html::encode($this->title) ?></h1>

        <p>Complete sus datos de acceso al sistema:</p>

        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Usuario') ?>
        <?= $form->field($model, 'password')->passwordInput()->label('Clave') ?>

        <?= Html::submitButton('Ingresar', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        <?php ActiveForm::end(); ?>
    </div>

</div>
