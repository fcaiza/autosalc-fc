<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\ResetPasswordForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Restablecer la clave';
if (!isset($bandera)) {
    $this->title = "Cambio de clave Inicial";
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class=" ">
    <div class="body-content" align="center" style="margin-top: 20px;">
        <img src="<?= Yii::getAlias('@web') ?>/img/autosalc/icono.jpg" width="36px" alt="..." class="img-circle">
    </div>
    <div class="login-logo">
        <a href="#" style="font-size: 40px;"><b>Autos</b>-ALC</a>
    </div>
    <div class="row">
        <div class="col-lg-offset-4 col-lg-4 site-reset-password" style="background-color: white;">
            <h1><?= Html::encode($this->title) ?></h1>

            <p>Elija su nueva contraseña:</p>

            <div class="row">
                <div class="col-lg-12">
                    <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                    <?= $form->field($model, 'password')->passwordInput(['autofocus' => true])->label('Nueva Clave') ?>

                    <div class="form-group">
                        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
