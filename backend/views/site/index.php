<?php
/* @var $this View */

use common\models\Orden;
use lcaiza\ajaxcrud\AurySolutionsWidget;
use yii\web\View;
use yiister\gentelella\widgets\StatsTile;

$this->title = 'autos ALC';
?>
<?= AurySolutionsWidget::widget() ?>
<style>
    .tile-stats .icon {
        right: 60px;
    }
</style>
<div class="row">
    <div class="col-xs-12 col-md-2">
        <?=
        StatsTile::widget(
                [
                    'icon' => 'paste',
                    'header' => 'Ordenes',
                    'text' => 'Listado Total',
                    'number' => Orden::contarOrdenes(),
                ]
        )
        ?>
    </div>
    <div class="col-xs-12 col-md-2">
        <?=
        StatsTile::widget(
                [
                    'icon' => 'hand-paper-o',
                    'header' => 'Pendientes',
                    'text' => 'Ingresadas, Aprobadas y Asignadas',
                    'number' => Orden::contarOrdenes([1, 2, 3]),
                ]
        )
        ?>
    </div>
    <div class="col-xs-12 col-md-2">
        <?=
        StatsTile::widget(
                [
                    'icon' => 'gears',
                    'header' => 'En Proceso',
                    'text' => 'Trabajando en las Areas',
                    'number' => Orden::contarOrdenes(4),
                ]
        )
        ?>
    </div>
    <div class="col-xs-12 col-md-2">
        <?=
        StatsTile::widget(
                [
                    'icon' => 'handshake-o',
                    'header' => 'En Revision',
                    'text' => 'Esperando para entrega',
                    'number' => Orden::contarOrdenes(5),
                ]
        )
        ?>
    </div>
    <div class="col-xs-12 col-md-2">
        <?=
        StatsTile::widget(
                [
                    'icon' => 'check-square-o',
                    'header' => 'Terminadas',
                    'text' => 'Trabajos Finalizados',
                    'number' => Orden::contarOrdenes(6),
                ]
        )
        ?>
    </div>
    <div class="col-xs-12 col-md-2">
        <?=
        StatsTile::widget(
                [
                    'icon' => 'times',
                    'header' => 'Canceladas',
                    'text' => 'Trabajos no realizados',
                    'number' => Orden::contarOrdenes(7),
                ]
        )
        ?>
    </div>
</div>
<div class="">
    <div class="row">
        <div class="col-xs-12 col-lg-12 col-sm-12">
            <div class="x_panel tile">
                <div class="x_title">
                    <h2>Dashboard de Procesos</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                </div>
            </div>
        </div>
    </div>
</div>
