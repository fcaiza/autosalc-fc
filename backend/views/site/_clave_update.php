<?php

use kartik\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empresa-form">

    <?php
    $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_LARGE]
    ]);
    ?>

    <div class="col-sm-12">
        <?php echo $form->field($model, 'password_hash')->textInput(['maxlength' => true])->label('Nueva Clave') ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
