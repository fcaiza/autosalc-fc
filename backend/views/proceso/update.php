<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Proceso */
?>
<div class="proceso-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
