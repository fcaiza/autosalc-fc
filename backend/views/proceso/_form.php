<?php

use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

//use backend\modules\transferencia\controllers\AdminSeleccionController;

/* @var $this yii\web\View */
/* @var $model common\models\Proceso */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proceso-form">

    <?php
    $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_HORIZONTAL,
                'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_LARGE]
    ]);
    ?>
    <?php echo $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
    <?php echo $form->field($model, 'icono')->textInput(['maxlength' => true]) ?>
    <?php echo $form->field($model, 'orden')->input('number') ?>
    <?php echo $form->field($model, 'estado')->dropDownList(ArrayHelper::map(\common\models\CatalogoGeneral::listarCatalogo(1), 'value', 'nombre'), ['prompt' => 'Seleccione']) ?>
    <?php echo $form->field($model, 'publicar')->dropDownList(ArrayHelper::map(\common\models\CatalogoGeneral::listarCatalogo(4), 'value', 'nombre'), ['prompt' => 'Seleccione', 'id' => 'publicar']) ?>
    <?php
    echo $form->field($model, 'detalle')->widget(\backend\widgets\froala\FroalaEditorWidget::className(), [
        'csrfCookieParam' => '_csrf',
        'options' => ['id' => 'plantillapreview',],
        'clientOptions' => [
            'toolbarInline' => false,
            'height' => 150,
            'theme' => 'blue', //optional: dark, red, gray, royal
            'language' => 'es',
            'toolbarButtons' => ['undo', 'redo', '|', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', '|', 'color', 'emoticons', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', '|', 'insertLink', 'fileManager', 'insertTable', '|', 'quote', 'insertHR', 'undo', 'redo', 'clearFormatting', 'selectAll', 'html', 'fullscreen',],
            'imageUploadURL' => \yii\helpers\Url::to(['/global/upload']),
            'imageManagerLoadURL' => \yii\helpers\Url::to(['/global/file-froala']),
            'fileUploadURL' => \yii\helpers\Url::to(['/global/upload-file1']),
        ],
    ])
    ?>
    <?php
    $pasoName = '';
    $nameGuardado = 'proceso-' . $model->id;
    if (!$model->isNewRecord)
    {
        $data = [];
        if ($model->imagen)
        {
            $pasoName = $model->imagen;
            $path = '/img/proceso/' . $model->id;
            $data = \backend\controllers\GlobalController::ObtenerImagenModelo($path, $model->imagen);
        }
        echo $form->field($model, "imagen")->widget(\kartik\widgets\FileInput::className(), [
            'options' => [
                'id' => 'imageFile',
                'multiple' => false,
            ],
            'pluginOptions' => [
                'maxFileSize' => 500,
                'initialPreview' => ($data) ? $data[0] : '',
                'initialPreviewConfig' => ($data) ? $data[1] : '',
                'showUpload' => false,
                'previewFileType' => 'any',
                'initialPreviewAsData' => true,
                'allowedFileExtensions' => ["png", "jpg", "jpeg"],
                'uploadUrl' => yii\helpers\Url::to(['/global/guardar-generico-doc-img', 'id' => $model->id, 'tipo' => '1', 'model' => 'common\models\Proceso', 'path' => 'img/proceso', 'img' => 1, 'name' => $nameGuardado, 'atribbute' => 'imagen']),
                'deleteUrl' => \yii\helpers\Url::to(['/global/delete-img', 'id' => $model->id, 'tipo' => '1', 'model' => 'common\models\Proceso', 'path' => 'img/proceso', 'img' => 1, 'name' => $nameGuardado, 'atribbute' => 'imagen']),
            ]
        ]);
    }
    ?>

    <?php echo Html::input('hidden', "imagen", '', ['id' => 'imagen']); ?>

        <?php if (!Yii::$app->request->isAjax)
        {
            ?>
        <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
<?php } ?>

<?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    $(function () {
        $('.kv-file-remove').show();
        var paso = '<?= $pasoName ?>';
        $('#imagen').val(paso);
        var fileName = null;
        var fileUpload = $('#imageFile');

        fileUpload.on('filebeforedelete', function (e) {
            var aborted = !window.confirm('Esta seguro de borrar el archivo ?');
            return aborted;
        });
        fileUpload.on('filedeleted', function (e) {
            $.pjax.reload({container: '#crud-datatable-pjax', timeout: false});
            $('#ajaxCrudModal').modal('hide');
        });
        fileUpload.on('fileuploaded', function (event, data, previewId, index) {
            var img = data.files[0].name;
            $('#imagen').val('');
            var file = img;
            var fileName = '<?= $nameGuardado ?>';
            var clean = file.split('\\').pop();
            var punto = clean.split('.').pop();
            $('#imagen').val(fileName + "." + punto.toLowerCase());
        }).on("filebatchselected", function (event, files) {
            $('.kv-file-upload ').click();
            $('.kv-file-remove').hide();
        });
        fileUpload.on('filebatchpreupload', function (event, data) {
            var img = data.files[0].name;
            $('#imagen').val('');
            var file = img;
            var fileName = '<?= $nameGuardado ?>';
            var clean = file.split('\\').pop();
            var punto = clean.split('.').pop();
            $('#imagen').val(fileName + "." + punto.toLowerCase());
        });
        fileUpload.change(function () {
            $('#imagen').val('');
            var file = $(this).val();
            var fileName = '<?= $nameGuardado ?>';
            var clean = file.split('\\').pop();
            var punto = clean.split('.').pop();
            $('#imagen').val(fileName + "." + punto.toLowerCase());
        });

        $(".field-plantillapreview").hide();
        if ($("#publicar").val() == 1) {
            $(".field-plantillapreview").show();
        }

        $("#publicar").change(function () {
            if ($("#publicar").val() == 1) {
                $(".field-plantillapreview").show();
            } else {
                $(".field-plantillapreview").hide();
            }
        });
    });
</script>


