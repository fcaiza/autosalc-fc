<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Proceso */
?>
<div class="proceso-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            [
                'attribute' => 'detalle',
                'format' => 'raw'
            ],
            'orden',
            'icono',
			[
                'label' => 'Estado',
                'value' => ($model->estado == 1) ? 'Activo' : 'Inactivo',
            ],
        ],
    ]) ?>

</div>
