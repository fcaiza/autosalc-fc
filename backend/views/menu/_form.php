<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\widgets\ActiveForm;
use mdm\admin\models\Menu;
use yii\helpers\Json;
use mdm\admin\AutocompleteAsset;

use rmrevin\yii\fontawesome\FA;

/* @var $this yii\web\View */
/* @var $model mdm\admin\models\Menu */
/* @var $form yii\widgets\ActiveForm */


AutocompleteAsset::register($this);
$opts = Json::htmlEncode([
            'menus' => Menu::getMenuSource(),
            'routes' => Menu::getSavedRoutes(),
        ]);
$this->registerJs("
    var _opts = $opts; 
");
$this->registerJs($this->render('_script.js'));

?>
<div class="menu-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= Html::activeHiddenInput($model, 'parent', ['id' => 'parent_id']); ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => 128, 'id' => 'name']) ?>

            <?= $form->field($model, 'parent_name', ['inputOptions' => ['autocomplete' => 'off']])->textInput(['id' => 'parent_name', 'class' => 'typeahead', 'data-provide' => 'typeahead']) ?>

            <?= $form->field($model, 'route', ['inputOptions' => ['autocomplete' => 'off']])->textInput(['id' => 'route', 'class' => 'typeahead', 'data-provide' => 'typeahead']) ?>


        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'order')->input('number') ?>
            <label>Icono &nbsp;<span id='fa-icono'></span></label>
            <?= $form->field($model, 'icon')->textInput(['id' => 'icono'])->label(false)->hint("fa fa-???") ?>

            <?= $form->field($model, 'data')->textarea(['rows' => 3]) ?>
        </div>
    </div>

    <div class="form-group text-right">
        <?=
        Html::submitButton($model->isNewRecord ? Yii::t('rbac-admin', 'Create') : Yii::t('rbac-admin', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

