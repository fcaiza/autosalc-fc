<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use execut\widget\TreeView;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel mdm\admin\models\searchs\Menu */

$this->title = Yii::t('rbac-admin', 'Menus');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-8">
        <div class="menu-index">
            <?php // echo $this->render('_search', ['model' => $searchModel]);   ?>

            <p>
                <?= Html::a(Yii::t('rbac-admin', 'Create Menu'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'icono',
                        'filter' => '',
                        'label' => Yii::t('rbac-admin', 'Icon'),
                        'format' => 'raw',
                        'value' => function($model)
                        {
                            return ($model->icon) ? rmrevin\yii\fontawesome\FA::icon($model->icon) : '';
                        }
                    ],
                    'name',
                    [
                        'attribute' => 'menuParent.name',
                        'filter' => Html::activeTextInput($searchModel, 'parent_name', [
                            'class' => 'form-control', 'id' => null
                        ]),
                        'label' => Yii::t('rbac-admin', 'Parent'),
                    ],
                    'route',
                    'order',
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
            ?>
            <?php Pjax::end(); ?>

        </div>
    </div>
    <div class="col-lg-4" style="background-color: white; min-height: 500px;">
        <?php
        $JSEventClick = <<<EOF
    function (undefined, item) {
        if (item.href !== location.pathname) {
                $(location).attr('href',item.href);
        }
    }
EOF;

        $groupsContent = TreeView::widget([
                    'data' => common\models\Menu::getArbolMenu(),
                    'size' => TreeView::SIZE_NORMAL,
                    'header' => 'Menu Actual',
                    'searchOptions' => [
                        'inputOptions' => [
                            'placeholder' => 'Buscar menu...'
                        ],
                    ],
                    'clientOptions' => [
                        'onNodeSelected' => new JsExpression($JSEventClick),
                    ],
        ]);


        echo $groupsContent;
        ?>
    </div>
</div>
