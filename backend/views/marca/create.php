<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Marca */

?>
<div class="marca-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
