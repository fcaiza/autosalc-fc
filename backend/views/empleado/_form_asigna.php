<?php

use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Empleado */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empleado-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_LARGE]
    ]); ?>
    <div class="panel panel-default">
        <div class="container-fluid">
            <div class="panel-body col-sm-6">
                <legend>Datos Empleado</legend>
                <?php echo $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

                <?php echo $form->field($model, 'apellido')->textInput(['maxlength' => true]) ?>

                <?php echo $form->field($model, 'telefono')->textInput(['maxlength' => true, 'placeholder' => "Telefono"]) ?>

                <?php echo $form->field($model, 'celular')->textInput(['maxlength' => true, 'placeholder' => "Celular"]) ?>

            </div>
            <div class="panel-body col-sm-6">
                <legend>Información Usuario</legend>
                <?php
                $data = ArrayHelper::map(\common\models\User::find()->where(['NOT IN', 'id', $users])->andWhere("id<>1")->all(), 'id', 'username');
                echo $form->field($model, 'user_id')->widget(Select2::classname(), [
                    'data' => $data,
                    'options' => ['placeholder' => 'Seleccione ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

                <?php
//                $data = ArrayHelper::map(\common\models\Sucursal::find()->where(['estado' => 1])->all(), 'id', 'nombre', 'empresa.nombre');
//                echo $form->field($model, 'sucursal_defecto')->widget(Select2::classname(), [
//                    'data' => $data,
//                    'options' => ['placeholder' => 'Seleccione ...'],
//                    'pluginOptions' => [
//                        'allowClear' => true
//                    ],
//                ]);
                ?>
                <?php
//                if ($model->sucursal_encargadas || $model->sucursal_defecto) {
//                    $data = ArrayHelper::map(\common\models\Sucursal::find()->where(['NOT IN', 'id', $model->sucursal_defecto])->all(), 'id', 'nombre', 'empresa.nombre');
//                    echo $form->field($model, 'sucursal_encargadas')->widget(DepDrop::classname(), [
//                        'data' => $data,
//                        'pluginOptions' => [
//                            'depends' => ['empleado-sucursal_defecto'],
//                            'placeholder' => 'Seleccione...',
//                            'url' => Url::to(['sucursales'])
//                        ],
//                        'select2Options' => ['pluginOptions' => ['allowClear' => true, 'multiple' => true]],
//                        'type' => DepDrop::TYPE_SELECT2,
//                    ]);
//                } else {
//                    echo $form->field($model, 'sucursal_encargadas')->widget(DepDrop::classname(), [
//                        'pluginOptions' => [
//                            'depends' => ['empleado-sucursal_defecto'],
//                            'placeholder' => 'Seleccione...',
//                            'url' => Url::to(['sucursales'])
//                        ],
//                        'select2Options' => ['pluginOptions' => ['allowClear' => true, 'multiple' => true]],
//                        'type' => DepDrop::TYPE_SELECT2,
//                    ]);
//                }
                ?>

                <?php
//                $data = ArrayHelper::map(\common\models\Proceso::find()->where(['estado' => 1])->all(), 'id', 'nombre');
//                echo $form->field($model, 'proceso_id')->widget(Select2::classname(), [
//                    'data' => $data,
//                    'options' => ['placeholder' => 'Seleccione ...'],
//                    'pluginOptions' => [
//                        'allowClear' => true
//                    ],
//                ]);
                ?>
                <?php echo $form->field($model, 'estado')->dropDownList(ArrayHelper::map(\common\models\CatalogoGeneral::listarCatalogo(1), 'value', 'nombre'), ['prompt' => 'Seleccione']) ?>
            </div>
        </div>
    </div>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $(function () {
        $("#empleado-nombre").change(function () {
            var nombre = ucwords($(this).val());
            $(this).val(nombre);
        });
        $("#empleado-apellido").change(function () {
            var nombre = ucwords($(this).val());
            $(this).val(nombre);
        });
    });

    function ucwords(str) {
        return (str + '')
            .replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
                return $1.toUpperCase();
            });
    }
</script>