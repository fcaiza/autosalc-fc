<?php

/* @var $this yii\web\View */
/* @var $model common\models\Empleado */
?>
<div class="empleado-update">

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user
    ]) ?>

</div>
