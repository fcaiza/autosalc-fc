<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use lcaiza\ajaxcrud\AurySolutionsWidget;
use mdm\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\EmpleadoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Empleado';
$this->params['breadcrumbs'][] = $this->title;

?>

<?=AurySolutionsWidget::widget()?>

<div class="empleado-index container-fluid">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                ['content'=>
                    ((Helper::checkRoute('create')) ? Html::a('<i class="fa fa-plus"></i>', ['create'],
                    ['role'=>'modal-remote','title'=> 'Crear Nuevo','class'=>'btn btn-default']) : '').
                    ((Helper::checkRoute('asigna-sucursal')) ? Html::a('<i class="fa fa-address-book"></i>', ['asigna-sucursal'],
                        ['role'=>'modal-remote','title'=> 'Asignar Sucursal Usuario Existente','class'=>'btn btn-info']) : '').
                    Html::a('<i class="fa fa-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Recargar Grid']).
                    '{toggleData}'.
                    '{export}'
                ],
            ],          
            'striped' => true,
            'condensed' => true,
            'responsive' => true,          
            'panel' => [
                'type' => 'primary', 
                'heading' => '<i class="fa fa-list"></i> Listado de Empleado ',
                'before'=>'<em>* Para cambiar el tamaño de las columnas arrastre los bordes de columna.</em>',
            ],
            'exportConfig' => [
                GridView::CSV => ['label' => 'CSV'],
                GridView::PDF => [],
                GridView::EXCEL => [],
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// Siempre lo necesitas para plugin jquery
    "options"=>['tabindex' => false ],
    "size" => Modal::SIZE_LARGE
])?>
<?php Modal::end(); ?>
