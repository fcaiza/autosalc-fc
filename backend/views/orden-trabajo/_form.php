<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
//use backend\modules\transferencia\controllers\AdminSeleccionController;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\OrdenTrabajo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orden-trabajo-form">

    <?php $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_HORIZONTAL,
                'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_LARGE]
    ]); ?>

    <?php   $lista=ArrayHelper::map('cambiaramodelorelacional' , 'id', 'nombre');
			echo $form->field($model, 'orden_proceso_id')->widget(Select2::classname(), [
				'data' => $lista,
				'options' => ['placeholder' => 'Seleccione ...'],
				'pluginOptions' => ['allowClear' => true]
			])
 ?>

    <?php echo $form->field($model, 'empleado_id')->textInput() ?>

    <?php echo $form->field($model, 'estado_id')->textInput() ?>

    <?php echo $form->field($model, 'fecha_inicio')->textInput() ?>

    <?php echo $form->field($model, 'fecha_fin')->textInput() ?>

    <?php echo $form->field($model, 'trabajo')->textarea(['rows' => 2]) ?>

    <?php echo $form->field($model, 'resultado')->textarea(['rows' => 2]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
