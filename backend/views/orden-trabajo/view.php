<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\OrdenTrabajo */
?>
<div class="orden-trabajo-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'orden_proceso_id',
            'empleado_id',
            'estado_id',
            'fecha_inicio',
            'fecha_fin',
            'trabajo:ntext',
            'resultado:ntext',
			[
                'label' => 'Creacion',
                'value' => $model->created_at,
                'format' => ['DateTime', 'php:Y-m-d H:i:s']
            ],
			[
                'label' => 'Actualizacion',
                'value' => $model->updated_at,
                'format' => ['DateTime', 'php:Y-m-d H:i:s']
            ],
        ],
    ]) ?>

</div>
