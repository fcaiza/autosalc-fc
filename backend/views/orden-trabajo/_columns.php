<?php

use yii\helpers\Url;
use mdm\admin\components\Helper;
use kartik\grid\GridView;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'ordenProceso.orden.numero_orden',
        'label' => 'Numero #'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'ordenProceso.proceso.nombre',
        'label' => 'Proceso'
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'empleado_id',
//        'value' => function ($model) {
//            return ($model->empleado_id) ? \common\models\Empleado::findOne( $model->empleado_id)->getNombres() : '';
//        },
//        'filter'=>false,
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'estado_id',
        'value' => function ($model) {
            $empleado = ($model->empleado_id) ? '-' . \common\models\Empleado::findOne($model->empleado_id)->getNombres() : '';
            return ($model->estado_id) ? \common\models\ProcesoEstado::findOne($model->estado_id)->nombre . " <code>$empleado</code>" : '';
        },
        'filter' => false,
        'format' => 'raw',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'fecha_inicio',
        'format' => 'date',
        'filter' => false,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'fecha_fin',
        'filter' => false,
        'format' => 'date'
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'trabajo',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'resultado',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // 'format' => ['DateTime', 'php: Y-m-d H:i:s'],
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'updated_at',
    // 'format' => ['DateTime', 'php: Y-m-d H:i:s'],
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'width' => '100px',
        'vAlign' => 'middle',
        'template' => Helper::filterActionColumn('{/orden-proceso/view} {imagen-orden} '),
        'buttons' => [
            '/orden-proceso/view' => function ($url, $data) {
                return \yii\helpers\Html::a('<span class="glyphicon glyphicon-eye-open"></span> ', $url, [
                    'title' => 'Ver', 'data-toggle' => 'tooltip',
                    'role' => 'modal-remote',
                ]);
            },
            'imagen-orden' => function ($url, $data) {
                if (date('Y-m-d', strtotime($data->fecha_fin)) >= date('Y-m-d')) {
                    return \yii\helpers\Html::a('<span class="fa fa-archive"></span> ', $url, [
                        'title' => 'Subir Imagen', 'data-toggle' => 'tooltip',
                        'role' => 'modal-remote',
                    ]);
                }
            },
        ],
        'urlCreator' => function ($action, $model, $key, $index) {
            if ($action == '/orden-proceso/view') {
                return Url::to([$action, 'id' => $model->ordenProceso->id]);
            }
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Ver', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Editar', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Borrar',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Esta usted seguro?',
            'data-confirm-message' => 'Seguro que quiere borrar este item'],
    ],

]; ?>
<?php
//echo((Helper::checkRoute('/global/upload-generico-doc-img')) ?
//    Html::a('<i class="fa fa-archive"></i> Galeria de Ingreso', ['/global/upload-generico-doc-img',
//        'id' => $model->id, 'tipo' => '1', 'model' => 'common\models\Orden',
//        'path' => 'uploads/orden/' . $model->id . '/galery', 'img' => '', 'bandera' => 1],
//        ['title' => 'Galeria de Ingreso', 'class' => 'btn btn-success btn-sm btn-round', 'role' => 'modal-remote']) : '');
?>
