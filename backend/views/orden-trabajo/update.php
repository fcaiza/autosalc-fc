<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OrdenTrabajo */
?>
<div class="orden-trabajo-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
