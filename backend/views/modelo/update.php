<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Modelo */
?>
<div class="modelo-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
