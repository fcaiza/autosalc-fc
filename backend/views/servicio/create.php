<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Servicio */

?>
<div class="servicio-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
