<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Servicio */
?>
<div class="servicio-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
