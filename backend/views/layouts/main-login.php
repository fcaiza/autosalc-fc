<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */
use yii\helpers\Html;

$bundle = yiister\gentelella\assets\Asset::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta charset="<?= Yii::$app->charset ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel="shortcut icon" type="image/ico" href="<?= Yii::getAlias('@web') ?>/img/autosalc/logo-final-b.png">
        <?php $this->head() ?>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <style>
            body{
                background-color: #fefefe;
                margin: 0;
                padding: 0;
            }
            html {
                margin: 0;
                padding: 0;
                /*position: relative;*/
            }
            .footer {
                background-color: #11597C;
                position: absolute;
                bottom: 0px;
                width: 100%;
                height: 60px;
                margin-left: 0px;
                color: white;
                /*color: white;*/
            }
            footer{
                margin: 0;
                padding: 0;
                padding-top: 10px;
            }
        </style>
    </head>
    <body>
        <?php $this->beginBody(); ?>
        <div class="container">

            <div class="main_login">
                <div class="clearfix"></div>
                <?= $content ?>
            </div>


            <footer class="footer">
                <div class="pull-right">
                    autos ALC - AUTO/LINEA/COLOR<br />
                    Desarrollado por <a href="https://aurysolutions.com" rel="nofollow" target="_blank" style="color: white; font-weight: bold">AurySolutions</a>
                </div>
            </footer>
        </div>
        <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>
