<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

if (class_exists('backend\assets\AppAsset'))
{
    backend\assets\AppAsset::register($this);
}
else
{
    app\assets\AppAsset::register($this);
}

$bundle = yiister\gentelella\assets\Asset::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="shortcut icon" type="image/ico" href="<?= Yii::getAlias('@web') ?>/img/autosalc/logo-final-b.png">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

        <!--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">-->
        <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">-->
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .nav-sm .navbar.nav_title a i {
                margin: 3px 0 0 3px;
            }

            .site_title i {
                padding: 2px 3px;
            }
        </style>
    </head>
    <body class="nav-<?= !empty($_COOKIE['menuIsCollapsed']) && $_COOKIE['menuIsCollapsed'] == 'true' ? 'sm' : 'md' ?>">
        <?php $this->beginBody(); ?>
        <div class="container body">

            <div class="main_container">

                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">

                        <div class="navbar nav_title" style="border: 0;">
                            <a href="<?= Yii::getAlias('@web') ?>" class="site_title">
                                <i class="fa">
                                    <img src="<?= Yii::getAlias('@web') ?>/img/autosalc/icono.jpg" width="36px" alt="..."
                                         class="img-circle">
                                </i>
                                <span>Autos ALC!</span>
                            </a>
                        </div>
                        <div class="clearfix"></div>

                        <!-- menu prile quick info -->
                        <div class="profile">
                            <div class="profile_pic">
                                <?php
                                $foto = "http://placehold.it/128x128";
                                $name = Yii::$app->user->identity->username;
                                if (Yii::$app->user->identity->empleado)
                                {
                                    if (Yii::$app->user->identity->empleado->foto)
                                    {
                                        $foto = Yii::getAlias("@web") . "/img/users/" . Yii::$app->user->identity->empleado->foto;
                                    }
                                    $name = Yii::$app->user->identity->empleado->nombres;
                                }
                                ?>
                                <img src="<?= $foto ?>" alt="..." class="img-circle profile_img">
                            </div>
                            <div class="profile_info">
                                <span>Bienvenido,</span>
                                <h2><?= $name ?></h2>
                            </div>
                        </div>
                        <!-- /menu prile quick info -->

                        <br/>

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <h3 style="color: #BAB8B8 !important;"><?= (Yii::$app->user) ? Yii::$app->user->identity->sucursal : '' ?></h3>
                                <h3>General</h3>
                                <?php
                                $menu = (\backend\controllers\BaseController::armarMenu());
                                if ($menu):
                                    echo \yiister\gentelella\widgets\Menu::widget([
                                        'items' => $menu
                                    ]);
                                endif;
                                ?>
                            </div>

                        </div>
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" title="Sucursal"
                               href="<?= Yii::getAlias("@web") ?>/empresa/escoger">
                                <span class="fa fa-home" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="">
                                <span class="glyphicon glyphicon-fullscreen1" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="">
                                <span class="glyphicon glyphicon-eye-close1" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Salir"
                               href="<?= Yii::getAlias('@web') ?>/site/logout" data-method="post">
                                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">

                    <div class="nav_menu">
                        <nav class="" role="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                                       aria-expanded="false">
                                        <img src="<?= $foto ?>" alt=""><?= $name ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <!--                                        <li><a href="javascript:;">  Profile</a>
                                                                                </li>-->
                                        <!--                                        <li>
                                                                                    <a href="javascript:;">
                                                                                        <span class="badge bg-red pull-right">50%</span>
                                                                                        <span>Settings</span>
                                                                                    </a>
                                                                                </li>-->
                                        <!--                                        <li>
                                                                                    <a href="javascript:;">Help</a>
                                                                                </li>-->
                                        <li>
                                            <a href="<?= Yii::getAlias('@web') ?>/site/logout" data-method="post"><i
                                                    class="fa fa-sign-out pull-right"></i> Salir</a>
                                        </li>
                                    </ul>
                                </li>

                                <!--                                <li role="presentation" class="dropdown">
                                                                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                                                        <i class="fa fa-envelope-o"></i>
                                                                        <span class="badge bg-green">6</span>
                                                                    </a>
                                                                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                                                        <li>
                                                                            <a>
                                                                                <span class="image">
                                                                                    <img src="http://placehold.it/128x128" alt="Profile Image" />
                                                                                </span>
                                                                                <span>
                                                                                    <span>John Smith</span>
                                                                                    <span class="time">3 mins ago</span>
                                                                                </span>
                                                                                <span class="message">
                                                                                    Film festivals used to be do-or-die moments for movie makers. They were where...
                                                                                </span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a>
                                                                                <span class="image">
                                                                                    <img src="http://placehold.it/128x128" alt="Profile Image" />
                                                                                </span>
                                                                                <span>
                                                                                    <span>John Smith</span>
                                                                                    <span class="time">3 mins ago</span>
                                                                                </span>
                                                                                <span class="message">
                                                                                    Film festivals used to be do-or-die moments for movie makers. They were where...
                                                                                </span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a>
                                                                                <span class="image">
                                                                                    <img src="http://placehold.it/128x128" alt="Profile Image" />
                                                                                </span>
                                                                                <span>
                                                                                    <span>John Smith</span>
                                                                                    <span class="time">3 mins ago</span>
                                                                                </span>
                                                                                <span class="message">
                                                                                    Film festivals used to be do-or-die moments for movie makers. They were where...
                                                                                </span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a>
                                                                                <span class="image">
                                                                                    <img src="http://placehold.it/128x128" alt="Profile Image" />
                                                                                </span>
                                                                                <span>
                                                                                    <span>John Smith</span>
                                                                                    <span class="time">3 mins ago</span>
                                                                                </span>
                                                                                <span class="message">
                                                                                    Film festivals used to be do-or-die moments for movie makers. They were where...
                                                                                </span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <div class="text-center">
                                                                                <a href="/">
                                                                                    <strong>See All Alerts</strong>
                                                                                    <i class="fa fa-angle-right"></i>
                                                                                </a>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </li>-->

                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="pull-right">
                        <?php
                        if ($this->title !== null)
                        {
                            ?>
                            <section class="content-header">
                                <?=
                                Breadcrumbs::widget([
                                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                ])
                                ?>
                            </section>
                            <?php
                        }
                        ?>
                    </div>
                    <?php if (isset($this->params['h1'])): ?>
                        <div class="page-title">
                            <div class="title_left">
                                <h1><?= $this->params['h1'] ?></h1>
                            </div>
                            <div class="title_right">
                                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search for...">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button">Go!</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
<?= Alert::widget() ?>
                    <?= $content ?>
                </div>
                <!-- /page content -->
                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                        autos ALC - AUTO/LINEA/COLOR<br/>
                        Desarrollado por <a href="https://aurysolutions.com" rel="nofollow"
                                            target="_blank">AurySolutions</a>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>

        </div>

        <!--        <div id="custom_notifications" class="custom-notifications dsp_none">
                    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
                    </ul>
                    <div class="clearfix"></div>
                    <div id="notif-group" class="tabbed_notifications"></div>
                </div>-->
        <!-- /footer content -->
        <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>