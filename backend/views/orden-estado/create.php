<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\OrdenEstado */

?>
<div class="orden-estado-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
