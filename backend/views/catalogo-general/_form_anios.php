<?php

use kartik\widgets\ActiveForm;
use yii\helpers\Html;

//use backend\modules\transferencia\controllers\AdminSeleccionController;

/* @var $this yii\web\View */
/* @var $model common\models\CatalogoGeneral */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="catalogo-general-form">

    <?php
    $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_LARGE]
    ]);
    ?>
    <div class="col-sm-12">
        <div class="col-sm-1"></div>
        <label class="form-label col-sm-2">
            Desde
        </label>
        <div class="col-sm-8">
            <?= Html::textInput('desde', $desde, ['class' => 'form-control']); ?>
        </div>
    </div>
    <br><br><br>
    <div class="col-sm-12">
        <div class="col-sm-1"></div>
        <label class="form-label col-sm-2">
            Hasta
        </label>
        <div class="col-sm-8">
            <?= Html::textInput('hasta', $hasta, ['class' => 'form-control']); ?>
        </div>
    </div>
    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton('Generar', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
