<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CatalogoGeneral */
?>
<div class="catalogo-general-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
