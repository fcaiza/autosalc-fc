<?php

use mdm\admin\components\Helper;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'catalogo_id',
        'value' => 'catalogo.nombre',
        'filter' => yii\helpers\ArrayHelper::map(\common\models\CatalogoGeneral::find()->all(), 'id', 'nombre'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'nombre',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'orden',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'value',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'valor_1',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'valor_2',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'valor_3',
    // ],
    [
        'class' => '\kartik\grid\BooleanColumn',
        'attribute' => 'estado',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'width' => '100px',
        'vAlign' => 'middle',
        'template' => Helper::filterActionColumn('{anios} {view} {update} {delete} '),
        'buttons' => [
            'anios' => function ($url, $data) {
                if ($data->id == 11) {
                    return \yii\helpers\Html::a('<span class="fa fa-asterisk"></span> ', $url, [
                        'title' => 'Generar Años', 'data-toggle' => 'tooltip',
                        'role' => 'modal-remote',
                    ]);
                }
            },
        ],
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Ver', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Editar', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Borrar',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Esta usted seguro?',
            'data-confirm-message' => 'Seguro que quiere borrar este item'],
    ],

];   