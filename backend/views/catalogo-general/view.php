<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CatalogoGeneral */
?>
<div class="catalogo-general-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'orden',
            'value',
            'valor_1',
            'valor_2',
            'valor_3',
            'catalogo_id',
			[
                'label' => 'Estado',
                'value' => ($model->estado == 1) ? 'Activo' : 'Inactivo',
            ],
        ],
    ]) ?>

</div>
