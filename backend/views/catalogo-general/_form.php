<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
//use backend\modules\transferencia\controllers\AdminSeleccionController;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\CatalogoGeneral */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="catalogo-general-form">

    <?php
    $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_HORIZONTAL,
                'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_LARGE]
    ]);
    ?>

    <?php echo $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'orden')->textInput() ?>

    <?php echo $form->field($model, 'value')->textInput() ?>

    <?php echo $form->field($model, 'valor_1')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'valor_2')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'valor_3')->textInput(['maxlength' => true]) ?>

    <?php 
    $lista = \common\models\CatalogoGeneral::find()->all();
    echo $form->field($model, 'catalogo_id')->dropDownList(ArrayHelper::map($lista, 'id', 'nombre'), ['prompt' => "Seleccione"]);  
            ?>

    <?php echo $form->field($model, 'estado')->dropDownList(ArrayHelper::map(\common\models\CatalogoGeneral::listarCatalogo(1), 'value', 'nombre'), ['prompt' => "Seleccione"]) ?>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
