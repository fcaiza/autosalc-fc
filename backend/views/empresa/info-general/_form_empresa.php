<?php

use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Empresa */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="empresa-form">

    <?php
    $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_LARGE]
    ]);
    ?>

    <div class="col-sm-12">
        <div class="col-sm-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title">
                        Datos Empresariales
                    </div>
                </div>
                <div class="panel-body" style="min-height:429px; max-height:429px;">
                    <?php echo $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

                    <?php echo $form->field($model, 'siglas')->textInput(['maxlength' => true]) ?>

                    <?php echo $form->field($model, 'web')->textInput(['maxlength' => true]) ?>

                    <?php echo $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                    <?php echo $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>

                    <?php echo $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

                    <div class="form-group highlight-addon field-empresa-telefono">
                        <div class="control-label has-star col-sm-3">
                            <label><?= $model->getAttributeLabel('celular') ?></label>
                        </div>
                        <div class="col-sm-5">
                            <?php echo $form->field($model, 'celular', ['enableLabel' => false])->textInput()->label(false) ?>
                        </div>
                        <div class="col-sm-4">
                            <?php echo $form->field($model, 'wathshap', ['enableLabel' => false])->textInput(['placeholder' => "Whatsapp"])->label(false) ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title">
                        Informacion General
                    </div>
                </div>
                <div class="panel-body" style="min-height:429px; max-height:429px;">

                    <?php
                    $pasoName = '';
                    $nameGuardado = 'empresa' . date('YmdHis');
                    if (!$model->isNewRecord) {
                        $data = [];
                        if ($model->logo) {
                            $pasoName = $model->logo;
                            $path = '/img/empresa/' . $model->id;
                            $data = \backend\controllers\GlobalController::ObtenerImagenModelo($path, $model->logo);
                        }
                        echo $form->field($model, "logo")->widget(\kartik\widgets\FileInput::className(), [
                            'options' => [
                                'id' => 'imageFile',
                                'multiple' => false,
                            ],
                            'pluginOptions' => [
                                'maxFileSize' => 500,
                                'initialPreview' => ($data) ? $data[0] : '',
                                'initialPreviewConfig' => ($data) ? $data[1] : '',
                                'showUpload' => false,
                                'previewFileType' => 'any',
                                'initialPreviewAsData' => true,
                                'allowedFileExtensions' => ["png", "jpg", "jpeg"],
                                'uploadUrl' => yii\helpers\Url::to(['/global/guardar-generico-doc-img', 'id' => $model->id, 'tipo' => '1', 'model' => 'common\models\Empresa', 'path' => 'img/empresa', 'img' => 1, 'name' => $nameGuardado, 'atribbute' => 'logo']),
                                'deleteUrl' => \yii\helpers\Url::to(['/global/delete-img', 'id' => $model->id, 'tipo' => '1', 'model' => 'common\models\Empresa', 'path' => 'img/empresa', 'img' => 1, 'name' => $nameGuardado, 'atribbute' => 'logo']),
                            ]
                        ]);
                    }
                    ?>

                    <?php echo $form->field($model, 'estado')->dropDownList(ArrayHelper::map(\common\models\CatalogoGeneral::listarCatalogo(1), 'value', 'nombre'), ['prompt' => 'Seleccione']) ?>

                    <?php //echo $form->field($model, 'celular')->textInput(['maxlength' => true]) ?>

                    <?php //echo $form->field($model, 'wathshap')->textInput(['maxlength' => true]) ?>

                </div>
            </div>
        </div>
    </div>

    <?php echo Html::input('hidden', "imagen", '', ['id' => 'imagen']); ?>
    <?php
    if (!Yii::$app->request->isAjax) {
        ?>
        <div class="form-group pull-right">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-round' : 'btn btn-primary btn-round']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    $(function () {
        $('.kv-file-remove').show();
        var paso = '<?= $pasoName ?>';
        $('#imagen').val(paso);
        var fileName = null;
        var fileUpload = $('#imageFile');

        fileUpload.on('filebeforedelete', function (e) {
            var aborted = !window.confirm('Esta seguro de borrar el archivo ?');
            return aborted;
        });
        fileUpload.on('filedeleted', function (e) {
            location.reload();
            // $.pjax.reload({container: '#crud-datatable-pjax', timeout: false});
            // $('#ajaxCrudModal').modal('hide');
        });
        fileUpload.on('fileuploaded', function (event, data, previewId, index) {
            var img = data.files[0].name;
            $('#imagen').val('');
            var file = img;
            var fileName = '<?= $nameGuardado ?>';
            var clean = file.split('\\').pop();
            var punto = clean.split('.').pop();
            $('#imagen').val(fileName + "." + punto.toLowerCase());
        }).on("filebatchselected", function (event, files) {
            $('.kv-file-upload ').click();
            $('.kv-file-remove').hide();
        });
        fileUpload.on('filebatchpreupload', function (event, data) {
            var img = data.files[0].name;
            $('#imagen').val('');
            var file = img;
            var fileName = '<?= $nameGuardado ?>';
            var clean = file.split('\\').pop();
            var punto = clean.split('.').pop();
            $('#imagen').val(fileName + "." + punto.toLowerCase());
        });
        fileUpload.change(function () {
            $('#imagen').val('');
            var file = $(this).val();
            var fileName = '<?= $nameGuardado ?>';
            var clean = file.split('\\').pop();
            var punto = clean.split('.').pop();
            $('#imagen').val(fileName + "." + punto.toLowerCase());
        });

        $("#empresa-nombre").change(function () {
            var nombre = ucwords($(this).val());
            $(this).val(nombre);
        });
        $("#empresa-siglas").change(function () {
            var nombre = ucwords2($(this).val());
            $(this).val(nombre);
        });
    });

    function ucwords(str) {
        return (str + '')
            .replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
                return $1.toUpperCase();
            });
    }

    function ucwords2(str) {
        return str.toUpperCase();
    }

</script>

