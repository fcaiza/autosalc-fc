<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use kartik\grid\GridView;
use yii\bootstrap\Modal;

?>
    <div class="row">
        <div class="notificaciones-index">
            <div id="ajaxCrudDatatable">
                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'width' => '30px',
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'nombre',
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'ruc',
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'telefono',
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'celular',
                    ],
                    [
                        'class' => '\kartik\grid\BooleanColumn',
                        'attribute' => 'estado',
                    ]
                ];
                ?>

                <?=
                GridView::widget([
                    'id' => 'crud-datatable-segu',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'pjax' => true,
                    'pjaxSettings' => [
                        'options' => [
                            'enablePushState' => false
                        ],
                    ],
                    'columns' => $gridColumns,
                    'toolbar' => [
                    ],
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'type' => 'primary',
                        'heading' => '<i class="glyphicon glyphicon-list"></i> Sucursales ',
                    ],
                    'exportConfig' => [
                        GridView::CSV => ['label' => 'CSV'],
                        GridView::PDF => [],
                        GridView::EXCEL => [],
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
<?php
Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "", // Siempre lo necesitas para plugin jquery
    "options" => ['tabindex' => false],
])
?>
<?php Modal::end(); ?>