<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use kartik\grid\GridView;
use lcaiza\ajaxcrud\AurySolutionsWidget;
use mdm\admin\components\Helper;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?= AurySolutionsWidget::widget() ?>
<div class="row">
    <div id="ajaxCrudDatatable">
        <?=
        GridView::widget([
            'id' => 'crud-datatable-sucursal',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax' => true,
            'pjaxSettings' => [
                'options' => [
                    'enablePushState' => false
                ],
            ],
            'columns' => require(__DIR__ . '/_columns.php'),
            'toolbar' => [
                ['content' =>
                    ((Helper::checkRoute('create')) ? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create', 'id_empresa' => $id], ['role' => 'modal-remote-sucursal', 'data-pjax' => 0, 'id' => 'btnAddSucursal', 'title' => 'Crear Nuevo', 'class' => 'btn btn-default']) : '') .
                    Html::a('<i class="fa fa-repeat"></i>', ['index','id'=>$id],
                        ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Recargar Grid']) .
                    '{toggleData}' .
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="glyphicon glyphicon-list"></i> Sucursales ',
            ],
            'exportConfig' => [
                GridView::CSV => ['label' => 'CSV'],
                GridView::PDF => [],
                GridView::EXCEL => [],
            ]
        ]);
        ?>
    </div>
</div>
<?php Modal::begin([
    "id" => "modal-remote-sucursal",
    "size" => "modal-lg",
    "footer" => "", // Siempre lo necesitas para plugin jquery
    "options" => ['tabindex' => false],
]);
?>
<?php Modal::end(); ?>
<script>
    $(function () {
        var modalPublic = new ModalRemote('#modal-remote-sucursal');
        // Catch click event on all buttons that want to open a modal
        $(document).on('click', '[role="modal-remote-sucursal"]', function (event) {
            event.preventDefault();
            // Open modal
            modalPublic.open(this, null);
        });
    });
</script>

<script>

    $(function () {
        $.grid_sucursal= ({
            openModal: false,
            init: function () {
                this.initEventGrid();
                $("#modal-remote-sucursal").on('hidden.bs.modal', function () {
                    if ($.grid_sucursal.openModal) {
                        $.grid_sucursal.cargarAjaxGrid();
                    }
                    $.grid_sucursal.openModal = false;
                });
                return this;
            },
            initEventGrid: function () {
                $("#btnAddSucursal,.ajax-sucursal-update,.ajax-sucursal-delete,.ajax-sucursal-view").click(function () {
                    $.grid_sucursal.openModal = true;
                });
            },
            cargarAjaxGrid: function () {
                var url = '<?= Url::to(["index", 'id' => $id]) ?>';
                $.pjax.reload({url: url, replace: false, container: '#crud-datatable-sucursal-pjax'});
            }

        }).init();

        $(this).on('pjax:success', function () {
            $.grid_sucursal.initEventGrid();
        });
    });

</script>

