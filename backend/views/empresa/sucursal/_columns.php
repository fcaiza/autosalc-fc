<?php

use mdm\admin\components\Helper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'nombre',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'ruc',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'telefono',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'celular',
    ],
    [
        'class' => '\kartik\grid\BooleanColumn',
        'attribute' => 'estado',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'width' => '100px',
        'vAlign' => 'middle',
        'template' => Helper::filterActionColumn('{view} {update} {delete} '),
        'buttons' => [
            'view' => function ($url, $data) {
                return Html::a('<span class="fa fa-eye"></span> ', $url, [
                    'title' => 'Ver', 'data-toggle' => 'tooltip',
                    'role' => 'modal-remote-sucursal',
                    'data-pjax' => 0,
                    'class' => 'ajax-sucursal-view'
                ]);
            },
            'update' => function ($url, $data) {
                return Html::a('<span class="glyphicon glyphicon-pencil"></span> ', $url, [
                    'title' => 'Update', 'data-toggle' => 'tooltip',
                    'role' => 'modal-remote-sucursal',
                    'data-pjax' => 0,
                    'class' => 'ajax-sucursal-update'
                ]);
            },
            'delete' => function ($url, $data) {
                return Html::a('<span class="glyphicon glyphicon-trash"></span> ', $url, [
                    'role' => 'modal-remote-sucursal', 'title' => 'Delete',
                    'class' => 'ajax-sucursal-delete',
                    'data-toggle' => 'tooltip', 'data-pjax' => 0,
                    'data-confirm' => false, 'data-method' => false,
                    'data-request-method' => 'post',
                    'data-toggle' => 'tooltip',
                    'data-confirm-title' => 'Esta usted seguro?',
                    'data-confirm-message' => 'Seguro que quiere borrar este item',
                ]);
            },
        ],
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
    ],

];   