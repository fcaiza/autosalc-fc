<?php

use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Empresa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empresa-form">

    <?php
    $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_LARGE]
    ]);
    ?>

    <div class="col-sm-12">
        <div class="col-sm-6">
            <?php echo $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'siglas')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'web')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-sm-6">

            <?php echo $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'celular')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'wathshap')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'estado')->dropDownList(ArrayHelper::map(\common\models\CatalogoGeneral::listarCatalogo(1), 'value', 'nombre'), ['prompt' => 'Seleccione']) ?>
        </div>
    </div>

    <?php echo Html::input('hidden', "imagen", '', ['id' => 'imagen']); ?>
    <?php
    if (!Yii::$app->request->isAjax) {
        ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $(function () {
        $("#empresa-nombre").change(function () {
            var nombre = ucwords($(this).val());
            $(this).val(nombre);
        });
        $("#empresa-siglas").change(function () {
            var nombre = ucwords2($(this).val());
            $(this).val(nombre);
        });
    });

    function ucwords(str) {
        return (str + '')
            .replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
                return $1.toUpperCase();
            });
    }

    function ucwords2(str) {
        return str.toUpperCase();
    }

</script>
