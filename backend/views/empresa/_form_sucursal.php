<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
//use backend\modules\transferencia\controllers\AdminSeleccionController;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Sucursal */
/* @var $form yii\widgets\ActiveForm */
if($sucursal){
    $model->nombre = $sucursal;
}
?>

<div class="sucursal-form">
    <div class="col-sm-6">
        <div class="x_panel tile">
            <div class="x_title">
                <h2 class="text-uppercase">configuraciones</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <?php
                $form = ActiveForm::begin([
                            'type' => ActiveForm::TYPE_HORIZONTAL,
                            'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_LARGE]
                ]);
                ?>

                <?php
                $lista = ArrayHelper::map($lista, 'id', 'nombre');
                echo $form->field($model, 'nombre')->widget(Select2::classname(), [
                    'data' => $lista,
                    'options' => ['placeholder' => 'Seleccione ...'],
                    'pluginOptions' => ['allowClear' => true]
                ])->label('Sucursal')
                ?>

                <div class="col-sm-12 text-right">
                    <?= Html::submitButton('Cambiar', ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>


            </div>
        </div>

    </div>

</div>
