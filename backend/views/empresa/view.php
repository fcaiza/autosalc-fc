<?php

/* @var $this yii\web\View */
/* @var $model common\models\Empresa */
?>
<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home"><h4>Informacion General</h4></a></li>
    <li style="display: none"><a data-toggle="tab" href="#menu1"><h4>Sucursales</h4></a></li>
</ul>
<div class="tab-content container-fluid">
    <br>
    <div id="home" class="tab-pane fade in active">
        <div class="col-sm-12">
            <div class="col-sm-6">
                <table class="table table-striped table-condensed table-responsive table-bordered">
                    <thead>
                    <tr>
                        <th colspan="2" class="text-info text-center">
                            Información de la Empresa
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="text-center">
                        <td colspan="2">
                            <?php
                            $img = ($model->logo ? Yii::getAlias("@web") . '/img/empresa/' . $model->id . '/' . $model->logo : Yii::getAlias("@web") . '/img/sin-imagen.jpg');
                            ?>
                            <img src="<?= $img ?>" alt="" width="150px" height="150px">
                        </td>
                    </tr>
                    <tr>
                        <th><?= $model->getAttributeLabel('nombre') ?> </th>
                        <td><?= ($model->nombre) ? $model->nombre : '<code> --- </code>' ?></td>
                    </tr>
                    <tr>
                        <th><?= $model->getAttributeLabel('siglas') ?> </th>
                        <td><?= ($model->siglas) ? $model->siglas : '<code> --- </code>' ?></td>
                    </tr>
                    <tr>
                        <th><?= $model->getAttributeLabel('email') ?> </th>
                        <td><?= ($model->email) ? $model->email : '<code> --- </code>' ?></td>
                    </tr>
                    <tr>
                        <th><?= $model->getAttributeLabel('direccion') ?> </th>
                        <td><?= ($model->direccion) ? $model->direccion : '<code> --- </code>' ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-sm-6">
                <table class="table table-striped table-condensed table-responsive table-bordered">
                    <thead>
                    <tr>
                        <th colspan="4" class="text-info text-center">
                            Información de contacto
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th><?= $model->getAttributeLabel('telefono') ?> </th>
                        <td><?= ($model->telefono) ? $model->telefono : '<code> --- </code>' ?></td>
                        <th><?= $model->getAttributeLabel('celular') ?> </th>
                        <td><?= ($model->celular) ? $model->celular : '<code> --- </code>' ?></td>
                    </tr>
                    <tr>
                        <th><?= $model->getAttributeLabel('wathshap') ?> </th>
                        <td colspan="3"><?= ($model->wathshap) ? $model->wathshap : '<code> --- </code>' ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="menu1" class="tab-pane fade">
        <?php
        $searchModel2 = new \common\models\search\SucursalSearch();
        $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams, $model->id);
        echo Yii::$app->controller->renderPartial('sucursal/listado', ['id' => $model->id, 'searchModel' => $searchModel2, 'dataProvider' => $dataProvider2]);
        ?>
    </div>
</div>