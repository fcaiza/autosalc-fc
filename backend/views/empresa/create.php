<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Empresa */

?>
<div class="empresa-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
