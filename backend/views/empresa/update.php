<?php

//use kartik\tabs\TabsX;
use yii\helpers\Url;

$this->title = 'Actualización Empresa';
$this->params['breadcrumbs'][] = ['label' => 'Empresas', 'url' => Url::to('../../empresa/index')];
$this->params['breadcrumbs'][] = $this->title;
?>
<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home"><h5><i class="fa fa-gears"></i> General</h5></a></li>
    <li style="display: none"><a data-toggle="tab" href="#menu1"><h5><i class="fa fa-university"></i> Sucursales</h5></a></li>
</ul>
<div class="tab-content container-fluid">
    <br>
    <div id="home" class="tab-pane fade in active">
        <?php
        echo $this->render('info-general/_form_empresa', ['model' => $model]);
        ?>
    </div>
    <div id="menu1" class="tab-pane fade">
        <?php
        echo((!$model->isNewRecord) ? Yii::$app->runAction('sucursal/index', ['id' => $model->id]) : '');
        ?>
    </div>
</div>

<script>
    $(function () {
        $("#btnSubmit").click(function () {
            $("#w0").submit();
        });
    });
</script>