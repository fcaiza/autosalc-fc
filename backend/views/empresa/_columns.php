<?php

use mdm\admin\components\Helper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'logo',
        'format' => 'raw',
        'filter' => false,
        'enableSorting' => false,
        'value' => function ($model) {
            $imagen = '';
            if ($model->logo != '') {
                $imagen = Html::img(Yii::getAlias('@web') . '/img/empresa/' . $model->id . '/' . $model->logo, ['width' => '100']);
            } else {

                $imagen = Html::img(Yii::getAlias('@web') . '/img/sin-imagen.jpg', ['width' => '100']);
            }
            return $imagen;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'nombre',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'siglas',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'email',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'web',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'telefono',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'celular',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'wathshap',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'Sucursales',
        'format' => 'raw',
        'value' => function ($model) {
            $count = (count($model->sucursals));
            return "<button class='btn btn-info btn-round btn-xs'> $count <i class='glyphicon glyphicon-home'></i> </button>";
        }
    ],
    [
        'class' => '\kartik\grid\BooleanColumn',
        'attribute' => 'estado',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'width' => '100px',
        'vAlign' => 'middle',
        'template' => Helper::filterActionColumn('{view} {update} {delete} '),
        'buttons' => [
            'ejemplo' => function ($url, $data) {
                return Html::a('<span class="fa fa-asterisk"></span> ', $url, [
                    'title' => 'Titulo', 'data-toggle' => 'tooltip',
                    'role' => 'modal-remote',
                ]);
            },
        ],
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Ver', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['title' => 'Editar', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Borrar',
            'data-confirm' => false, 'data-method' => false, // for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Esta usted seguro?',
            'data-confirm-message' => 'Seguro que quiere borrar este item'],
    ],
];
