<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

$form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_LARGE]
]);
?>
<div class="form-group highlight-addon field-ordentrabajoimagen-detalle">
    <label class="control-label has-star col-lg-5" for="ordentrabajoimagen-detalle">&ensp;</label>
    <div class="col-lg-6">
        <a id="btn-new" style="display: none" class="btn btn-info">&ensp;&ensp;&ensp; --- Ver Imagen Subida --- &ensp;&ensp;&ensp;</a>
    </div>
</div>
<div id="imagen-div">
<?php
echo $form->field($model, "imagen")->widget(\kartik\widgets\FileInput::className(), [
    'id' => 'imageFile',
    'options' => [
        'id' => 'imageFile',
        'multiple' => false,
    ],
    'pluginOptions' => [
        'initialPreview' => '',
        'initialPreviewConfig' => '',
        'showUpload' => false,
        'previewFileType' => 'any',
        'initialPreviewAsData' => true,
        'allowedFileExtensions' => ["png", "jpg", "jpeg"],
        'uploadUrl' => yii\helpers\Url::to(['/global/guardar-generico-doc-img', 'id' => $id, 'tipo' => '1', 'model' => 'common\models\OrdenTrabajoImagen', 'path' => $path, 'img' => 1, 'name' => $name]),
        'deleteUrl' => \yii\helpers\Url::to(['/global/delete-generico-img', 'id' => $id, 'tipo' => '1', 'model' => 'common\models\OrdenTrabajoImagen', 'path' => $path, 'img' => 1, 'name' => $name]),
    ]
]);
?>
</div>
<?php
echo $form->field($model, 'detalle')->textarea(['rows'=>6,'cols'=>100]);
?>
<?php echo Html::input('hidden', "imagen", '', ['id' => 'imagen']); ?>
<?php
ActiveForm::end();
?>
<script type="text/javascript">
    var paso = '<?= $name ?>';
    $(function () {
        $('.kv-file-remove').show();
        var fileName = null;
        var fileUpload = $('#imageFile');
        fileUpload.on('fileuploaded', function (event, data, previewId, index) {
            $("#fileName").val(fileName);
        }).on("filebatchselected", function (event, files) {
            $('#imageFile').fileinput("upload");
            $('#imagen').val(paso);
            $('.kv-file-remove').hide();
            $('#imagen-div').toggle();
            $('#btn-new').toggle();
        });
        fileUpload.on('filebatchpreupload', function (event, data) {
            $('#imagen').val(paso);
            $('.kv-file-remove').hide();
        });
    });
    $('#btn-new').click(function () {
        $('#imagen-div').toggle();
        $('#btn-new').toggle();
    });
    $('#imageFile').change(function () {
        $('#imagen').val(paso);
        $('.kv-file-remove').hide();
    });
</script>