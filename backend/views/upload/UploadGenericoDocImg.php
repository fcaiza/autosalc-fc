<?php

use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Url;

$data = \backend\controllers\GlobalController::ObtenerGenericoDocImg($id, $path, $tipo, $model);
$form1 = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data'],
        ]);
echo $form1->field(new common\models\UploadImagen(), "file")->widget(FileInput::className(), [
    'options' => [
        'id' => 'File',
        'multiple' => true,
    ],
    'pluginOptions' => [
        'maxFileSize' => 2000,
        'initialPreviewAsData' => true,
        'fileActionSettings' => [
            'showUpload' => false,
        ],
        'initialPreview' => $data[0],
        'initialPreviewConfig' => $data[1],
        'allowedFileExtensions' => ["xlsx", "docx", "pdf", "csv", "png", "jpg","jpeg"],
        'initialCaption' => 'Subir documentos',
        'uploadUrl' => Url::to(['/global/guardar-generico-doc-img', 'id' => $id, 'tipo' => $tipo, 'model' => $model, 'path' => $path]),
        'deleteUrl' => Url::to(['/global/delete-generico-doc-img', 'id' => $id, 'tipo' => $tipo, 'model' => $model, 'path' => $path]),
    ]
])->label('');


ActiveForm::end();
?>
<script>
    $('#File').on('filebeforedelete', function (e) {
        var aborted = !window.confirm('Esta seguro de borrar el archivo ?');
        return aborted;
    });
    $('#File').on('filedeleted', function (e) {

    });
    $('#File').on('fileuploaded', function (event, data, previewId, index) {
//        $('#ajaxCrudModal').modal('hide');
//        !window.alert('Se guardo con exito');
//        $.growlUI('<div class="alert alert-success">Información importada satisfactoriamente</div>');
    });
</script>