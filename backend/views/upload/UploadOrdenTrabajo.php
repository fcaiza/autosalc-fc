<?php

use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Url;

$form1 = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data'],
        ]);
echo $form1->field(new common\models\UploadImagen(), "file")->widget(FileInput::className(), [
    'options' => [
        'id' => 'File',
        'multiple' => true,
    ],
    'pluginOptions' => [
//        'maxFileSize' => 2000,
        'initialPreviewAsData' => true,
        'fileActionSettings' => [
            'showUpload' => false,
        ],
        'initialPreview' => [],
        'initialPreviewConfig' => [],
        'allowedFileExtensions' => ["png", "jpg","jpeg"],
        'initialCaption' => 'Subir Imagen',
        'uploadUrl' => Url::to(['guardar-img', 'id' => $id,'path' => $path]),
        'deleteUrl' => Url::to(['delete-img', 'id' => $id,'path' => $path]),
    ]
])->label('');


ActiveForm::end();
?>
<script>
    $('#File').on('filebeforedelete', function (e) {
        var aborted = !window.confirm('Esta seguro de borrar el archivo ?');
        return aborted;
    });
    $('#File').on('filedeleted', function (e) {

    });
    $('#File').on('fileuploaded', function (event, data, previewId, index) {
//        $('#ajaxCrudModal').modal('hide');
//        !window.alert('Se guardo con exito');
//        $.growlUI('<div class="alert alert-success">Información importada satisfactoriamente</div>');
    });
</script>