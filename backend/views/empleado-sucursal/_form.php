<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
//use backend\modules\transferencia\controllers\AdminSeleccionController;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\EmpleadoSucursal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empleado-sucursal-form">

    <?php $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_HORIZONTAL,
                'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_LARGE]
    ]); ?>

    <?php   $lista=ArrayHelper::map(\common\models\Sucursal::find()->where(['estado'=>1])->all() , 'id', 'nombre','empresa.nombre');
			echo $form->field($model, 'sucursal_id')->widget(Select2::classname(), [
				'data' => $lista,
				'options' => ['placeholder' => 'Seleccione ...'],
				'pluginOptions' => ['allowClear' => true]
			])
 ?>

    <?php echo $form->field($model, 'por_defecto')->checkbox() ?>

    <?php   $lista=ArrayHelper::map(\common\models\Proceso::find()->where(['estado'=>1])->all() , 'id', 'nombre');
			echo $form->field($model, 'proceso_id')->widget(Select2::classname(), [
				'data' => $lista,
				'options' => ['placeholder' => 'Seleccione ...'],
				'pluginOptions' => ['allowClear' => true]
			])
 ?>

    <?php echo $form->field($model, 'encargado_proceso')->checkbox() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
