<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\EmpleadoSucursal */
?>
<div class="empleado-sucursal-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'sucursal_id',
            'empleado_id',
            'por_defecto:boolean',
            'proceso_id',
            'encargado_proceso:boolean',
        ],
    ]) ?>

</div>
