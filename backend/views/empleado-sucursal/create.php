<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\EmpleadoSucursal */

?>
<div class="empleado-sucursal-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
