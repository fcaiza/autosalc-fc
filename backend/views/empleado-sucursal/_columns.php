<?php
use yii\helpers\Url;
use mdm\admin\components\Helper;
use kartik\grid\GridView;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'sucursal_id',
        'value' => 'sucursal.nombre',
        'enableSorting' => false,
        'group' => true,
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'empleado_id',
//    ],
    [
        'class' => '\kartik\grid\BooleanColumn',
        'attribute' => 'por_defecto',
        'width' => '200px',
        'group' => true,
        'enableSorting' => false,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'proceso_id',
        'value' => 'proceso.nombre',
        'enableSorting' => false,
    ],
    [
        'class' => '\kartik\grid\BooleanColumn',
        'attribute' => 'encargado_proceso',
        'width' => '200px',
        'enableSorting' => false,
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'width' => '100px',
        'vAlign' => 'middle',
        'template' => Helper::filterActionColumn('{update} {delete} '),
        'buttons' => [
            'ejemplo' => function ($url, $data) {
                return Html::a('<span class="fa fa-asterisk"></span> ', $url, [
                        'title' => 'Titulo', 'data-toggle' => 'tooltip',
                        'role' => 'modal-remote',
                    ]);
                },
        ],
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'sucursal_id' => $model->sucursal_id, 'empleado_id' => $model->empleado_id,'proceso_id' => $model->proceso_id]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Ver', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Editar', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Borrar',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Esta usted seguro?',
            'data-confirm-message' => 'Seguro que quiere borrar este item'],
    ],

];   