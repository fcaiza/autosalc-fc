<?php

use kartik\grid\GridView;
use lcaiza\ajaxcrud\AurySolutionsWidget;
use mdm\admin\components\Helper;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\EmpleadoSucursalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Empleado Sucursal';
$this->params['breadcrumbs'][] = $this->title;

?>

<?= AurySolutionsWidget::widget() ?>

<div class="empleado-sucursal-index container-fluid">
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'pjax' => true,
            'columns' => require(__DIR__ . '/_columns.php'),
            'toolbar' => [
                ['content' =>
                    ((Helper::checkRoute('create')) ? Html::a('<i class="fa fa-plus"></i>', ['create', 'empleado_id' => $empleado_id],
                        ['role' => 'modal-remote', 'title' => 'Crear Nuevo', 'class' => 'btn btn-default']) : '')
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'warning',
                'heading' => '<i class="fa fa-list"></i> Asignaciones Internas ',
            ],
            'exportConfig' => [
                GridView::CSV => ['label' => 'CSV'],
                GridView::PDF => [],
                GridView::EXCEL => [],
            ]
        ]) ?>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// Siempre lo necesitas para plugin jquery
    "options" => ['tabindex' => false],
    "size" => Modal::SIZE_LARGE
]) ?>
<?php Modal::end(); ?>
