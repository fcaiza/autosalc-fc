<?php

use yii\helpers\Url;
use mdm\admin\components\Helper;
use kartik\grid\GridView;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'seguro_id',
        'value' => 'seguro.nombre',
        'filter' => yii\helpers\ArrayHelper::map(\common\models\Seguro::find()->where(['estado' => 1])->all(), 'id', 'nombre'),
        'width' => '150px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'broker_id',
        'value' => 'broker.nombre',
        'filter' => yii\helpers\ArrayHelper::map(\common\models\Broker::find()->where(['estado' => 1])->all(), 'id', 'nombre'),
        'width' => '200px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'numero_orden',
        'width' => '100px',
        'label' => 'Numero Ord.'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'cliente_id',
        'value' => 'cliente.nombres',
        'filter' => ''
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'cliente_id',
        'value' => 'cliente.numero_documento',
        'width' => '100px',
        'label' => 'Numero Doc.'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'vehiculo_id',
        'value' => 'vehiculo.placa',
        'width' => '80px',
        'label' => 'Placas'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'fecha_recibe',
        'filter' => '',
        'width' => '150px',
        'format' => 'raw',
        'value' => function($model) {
            return $model->fecha_recibe . "<br><small>{$model->empleadoRecibe->user->username}</small>";
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'fecha_salida',
        'filter' => '',
        'width' => '100px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'estado_id',
        'value' => 'estado.nombre',
        'filter' => '',
        'width' => '60px',
//        'filter' => yii\helpers\ArrayHelper::map(\common\models\OrdenEstado::find()->where(['estado' => 1])->all(), 'id', 'nombre')
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => Helper::filterActionColumn('{view} {update}'),
        'buttons' => [
            'ejemplo' => function ($url, $data) {
                return Html::a('<span class="fa fa-asterisk"></span> ', $url, [
                            'title' => 'Titulo', 'data-toggle' => 'tooltip',
                            'role' => 'modal-remote',
                ]);
            },
        ],
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key, 'action' => 'planificacion']);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Ver', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['title' => 'Editar', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Borrar',
            'data-confirm' => false, 'data-method' => false, // for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Esta usted seguro?',
            'data-confirm-message' => 'Seguro que quiere borrar este item'],
    ],
];
