<?php

use kartik\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;
use kartik\widgets\Select2;
use lcaiza\ajaxcrud\AurySolutionsWidget;
use mdm\admin\components\Helper;
use unclead\multipleinput\MultipleInput;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yiister\gentelella\widgets\Panel;

/* @var $this yii\web\View */
/* @var $model common\models\Orden */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Ingresar Orden';
$this->params['breadcrumbs'][] = ['label' => 'Ordenes', 'url' => ['/orden/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?= AurySolutionsWidget::widget() ?>
<div class="orden-form">
    <?php
    $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 4, 'deviceSize' => ActiveForm::SIZE_LARGE]
    ]);
    ?>
    <?php
    Panel::begin(
        [
            'header' => 'Orden de Ingreso' ,
            'tools' => [
                [
                        'encode' => false,
                        'items' => [],
                        'label' => new \rmrevin\yii\fontawesome\component\Icon('file') . "&nbsp;" . $model->numero_orden,
                    ],
            ],
        ]
    )
    ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-5">
                <?php
                Panel::begin(
                    [
                        'header' => 'Datos de la Orden',
                        'icon' => 'cog',
                        'options' => [
                            'class' => "x_panel data-orden"
                        ]
                    ]
                )
                ?>
                <?php
                $lista = ArrayHelper::map(\common\models\Seguro::find()->where(['estado' => 1])->all(), 'id', 'nombre');
                echo $form->field($model, 'seguro_id')->widget(Select2::classname(), [
                    'data' => $lista,
                    'options' => ['placeholder' => 'Seleccione ...'],
                    'pluginOptions' => ['allowClear' => true]
                ])
                ?>
                <?php
                $lista = ArrayHelper::map(\common\models\Broker::find()->where(['estado' => 1])->all(), 'id', 'nombre');
                echo $form->field($model, 'broker_id')->widget(Select2::classname(), [
                    'data' => $lista,
                    'options' => ['placeholder' => 'Seleccione ...'],
                    'pluginOptions' => ['allowClear' => true]
                ])
                ?>
                <?php
                echo $form->field($model, 'fecha_recibe')->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Seleccione la fecha ...'],
                    'disabled' => true,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'startDate' => date('Y-m-d'),
                    ],
                ]);
                ?>
                <?php
                echo $form->field($model, 'fecha_salida')->widget(\kartik\widgets\DatePicker::classname(), [
                    'options' => ['placeholder' => 'Seleccione la fecha ...'],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'startDate' => date('Y-m-d', strtotime($model->fecha_recibe)),
                    ],
                    'pluginEvents' => [
                        "changeDate" => "function(e) {  fechaFinProcesos(e.date); }",
                    ]
                ])
                ?>
                <?php
                //                if (!$model->isNewRecord) {
                //                    $lista = ArrayHelper::map(common\models\OrdenEstado::find()->all(), 'id', 'nombre');
                //                    echo $form->field($model, 'estado_id')->widget(Select2::classname(), [
                //                        'data' => $lista,
                //                        'options' => ['placeholder' => 'Seleccione ...'],
                //                        'pluginOptions' => ['allowClear' => true]
                //                    ]);
                //                }
                ?>
                <?php Panel::end() ?>
            </div>
            <div class="col-sm-7">
                <?php
                Pjax::begin(['id' => 'data-filtro']);
                Panel::begin(
                    [
                        'header' => 'Datos Cliente',
                        'icon' => 'cog',
                        'options' => [
                            'class' => "x_panel data-orden"
                        ]
                    ]
                )
                ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <table class="table table-condensed">
                                <tr>
                                    <td>
                                        <label>
                                            <?= $model->getAttributeLabel('cliente_id') ?>
                                        </label>
                                    </td>
                                    <td>
                                        <?php
                                        echo((Helper::checkRoute('/cliente/create')) ? Html::a('<i class="fa fa-user"></i>', ['/cliente/create', 'bandera' => 1], ['title' => 'Agregar Cliente', 'class' => 'btn btn-info btn-sm btn-round pull-right', 'role' => 'modal-remote']) : '');
                                        ?>
                                        <?php
                                        $lista = ArrayHelper::map(\common\models\Cliente::find()->where(['estado' => 1])->all(), 'id', 'numero_documento');
                                        echo $form->field($model, 'cliente_id')->widget(Select2::classname(), [
                                            'data' => $lista,
                                            'options' => ['placeholder' => 'Seleccione ...', 'id' => 'cliente_id'],
                                            'pluginOptions' => ['allowClear' => true, 'width' => 'auto;']
                                        ])->label(false)
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <label>
                                            <?= $model->getAttributeLabel('vehiculo_id') ?>
                                        </label>
                                    </th>
                                    <td>
                                        <div id="boton-vehiculos" class="pull-right">
                                            <button type="button" class="btn btn-success btn-sm btn-round"
                                                    disabled="true"><i
                                                        class="fa fa-car"></i></button>
                                        </div>
                                        <?php
                                        $lista = [];
                                        if($model->cliente_id){
                                            $lista = ArrayHelper::map(\common\models\Vehiculo::find()
                                                    ->where(['cliente_id' => $model->cliente_id])
                                                    ->all(), 'id', 'placa');
                                        }
                                        echo $form->field($model, 'vehiculo_id')->widget(Select2::classname(), [
                                            'data' => $lista,
                                            'options' => ['placeholder' => 'Seleccione ...', 'id' => 'vehiculo_id'],
                                            'pluginOptions' => ['allowClear' => true, 'width' => 'auto']
                                        ])->label(false)
                                        ?>
                                    </td>
                                </tr>
                                <?php
                                if (!$model->isNewRecord) {
                                    ?>
                                    <tr>
                                        <td><label>Galeria</label></td>
                                        <td>
                                            <?php
                                            echo((Helper::checkRoute('/global/upload-generico-doc-img')) ?
                                                Html::a('<i class="fa fa-archive"></i> Galeria de Ingreso', ['/global/upload-generico-doc-img',
                                                    'id' => $model->id, 'tipo' => '1', 'model' => 'common\models\Orden',
                                                    'path' => 'uploads/orden/' . $model->id . '/galery', 'img' => '', 'bandera' => 1],
                                                    ['title' => 'Galeria de Ingreso', 'class' => 'btn btn-success btn-sm btn-round', 'role' => 'modal-remote']) : '');
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <?php
                            Panel::begin();
                            ?>
                            Nombres: <label id="lbl_cliente">...</label><br>
                            Email: <label id="lbl_email">...</label><br>
                            # Doc: <label id="lbl_documento">...</label><br>
                            Telefono: <label id="lbl_tel">...</label><br>
                            Celular: <label id="lbl_cell">...</label><br>
                            <?php
                            Panel::end();
                            ?>
                        </div>                        
                    </div>
                    <div class="row">
                        <?php
                         echo $form->field($model, 'observacion_ingreso')->textarea(['id' => 'observacion']);
                        ?>
                    </div>
                </div>
                <script>
                    function bloquearCarga() {
                        $.blockUI({
                            message: 'Procesando por favor espere...',
                            baseZ: 2000
                        });
                    }

                    function desbloquearCarga() {
                        $.unblockUI();
                    }

                    function reloadContent(cliente) {
                        $.pjax.reload({container: '#data-filtro', async: false});
                        $('#cliente_id').val(cliente).trigger('change');
                        dataVehiculos();
                    }

                    $(function () {
                        $("#cliente_id").change(function () {
                            dataVehiculos();
                        });
                    });

                    function dataVehiculos() {
                        var cliente_id = $('#cliente_id').val();
                        $("#lbl_cliente").text('...');
                        $("#lbl_documento").text('...');
                        $("#lbl_email").text('...');
                        $("#lbl_tel").text('...');
                        $("#lbl_cell").text('...');
                        $("#boton-vehiculos").empty();
                        $("#vehiculo_id").empty();
                        $("#boton-vehiculos").append('<button id="btn_vehiculo" type="button" class="btn btn-success" disabled="true"><i class="fa fa-car"></i></button>');
                        if (cliente_id != '') {
                            bloquearCarga();
                            $.ajax({
                                url: '<?= Yii::getAlias("@web") ?>' + "/orden/consultar?cliente_id=" + cliente_id,
                                type: "POST",
                                async: false,
                                success: function (data, textStatus, jqXHR) {
                                    desbloquearCarga();
                                    $("#lbl_cliente").text(data.nombres);
                                    $("#lbl_documento").text(data.documento);
                                    $("#lbl_email").text(data.email);
                                    $("#lbl_tel").text(data.telefono);
                                    $("#lbl_cell").text(data.celular);
                                    $("#cliente_id").val(data.id);
                                    $("#boton-vehiculos").empty();
                                    $("#boton-vehiculos").append(data.boton);
                                    $("#vehiculo_id").empty();
                                    $("#vehiculo_id").append(data.vehiculo);
                                },
                                error: function (jqXHR, status, error) {
                                    desbloquearCarga();
                                    $.growlUI('Error', '<div class="alert alert-danger">' + error + '</div>');
                                }
                            });
                        }
                    }
                </script>
                <?php
                Panel::end();
                Pjax::end();
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <?php
            if (!$model->isNewRecord) {
                ?>
                <script>
                    $(function () {
                        var cliente = '<?= $model->cliente_id ?>';
                        var vehiculo = '<?= $model->vehiculo_id ?>';
                        reloadContent(cliente);
                        $('#vehiculo_id').val(vehiculo).trigger('change');
                    });
                </script>
                <?php
            }
            Panel::begin(
                [
                    'header' => 'Informacion de Procesos',
                    'icon' => 'cog',
                ]
            );
            echo $form->field($model, 'ordenProcesos', ['enableLabel' => false])->widget(MultipleInput::className(), [
                'max' => 10,
                'min' => 1,
                'cloneButton' => true,
                'allowEmptyList' => false,
                'enableGuessTitle' => true,
                'addButtonPosition' => MultipleInput::POS_HEADER,
                'columns' => [
                    [
                        'name' => 'id',
                        'title' => '',
                        'type' => 'hiddenInput',
                    ],
                    [
                        'name' => 'fase',
                        'title' => 'Fase',
                        'enableError' => false,
                        'options' => [
                            'class' => 'input-priority'
                        ]
                    ],
                    [
                        'enableError' => true,
                        'name' => 'proceso_id',
                        'type' => 'dropDownList',
                        'title' => 'Proceso',
                        'items' => ArrayHelper::map($procesos, 'id', 'nombre'),
                        'headerOptions' => [
                            'class' => 'col-lg-2'
                        ],
                        'options' => [
                            'onchange' => "consultarEmpleados($(this))",
                            'prompt' => 'Seleccione'
                        ]
                    ],
                    [
                        'name' => 'observacion',
                        'title' => 'Observacion Detectada',
                        'headerOptions' => [
                            'class' => 'col-lg-6'
                        ],
                    ],
                    [
                        'name' => 'fecha_inicio',
                        'type' => \kartik\date\DatePicker::className(),
                        'title' => 'Fecha Inicio',
                        'value' => function ($data) {
                            return ($data['fecha_inicio'] ? date('Y-m-d', strtotime($data['fecha_inicio'])) : '');
                        },
                        'options' => [
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true,
                                'autoclose' => true,
                                'startDate' => ($model->fecha_recibe ? date('Y-m-d', strtotime($model->fecha_recibe)) : ''),
                                'endDate' => ($model->fecha_entrega ? date('Y-m-d', strtotime($model->fecha_entrega)) : ''),
                            ],
                        ],
                        'headerOptions' => [
                            'class' => 'col-lg-2'
                        ],
                    ],
                    [
                        'name' => 'fecha_fin',
                        'type' => \kartik\date\DatePicker::className(),
                        'title' => 'Fecha Fin',
                        'value' => function ($data) {
                            return ($data['fecha_fin'] ? date('Y-m-d', strtotime($data['fecha_fin'])) : '');
                        },
                        'options' => [
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true,
                                'autoclose' => true,
                                'startDate' => ($model->fecha_recibe ? date('Y-m-d', strtotime($model->fecha_recibe)) : ''),
                                'endDate' => ($model->fecha_entrega ? date('Y-m-d', strtotime($model->fecha_entrega)) : ''),
                            ],
                        ],
                        'headerOptions' => [
                            'class' => 'col-lg-2'
                        ],
                    ],
//                    [
//                        'name' => 'responsable_id',
//                        'type' => 'dropDownList',
//                        'title' => 'Responsable',
//                        'items' => ArrayHelper::map($empleadosSucursal, 'empleado_id', 'empleado.nombres'),
//                        'headerOptions' => [
//                            'style' => 'width: 180px;',
//                        ]
//                    ],
                ]
            ]);
            Panel::end();
            ?>
        </div>
        <div class="row">
            <div class="col-lg-9 "> </div>
            <div class="form-group text-right" style="padding: 10px;">
                <?= Html::a('Regresar', ['orden/index'], ['class' => 'btn btn-warning', 'title' => 'Listado de Ordenes']) ?>
                <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php Panel::end() ?>
    <?php ActiveForm::end(); ?>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// Siempre lo necesitas para plugin jquery
    "options" => ['tabindex' => false],
    "size" => Modal::SIZE_LARGE
]) ?>
<?php Modal::end(); ?>
<style>
    .help-block {
        margin-bottom: 5px;
    }
    .table > tbody > tr > th {
        vertical-align: middle;
    }
    .form-group {
        margin-bottom: 5px;
    }
    .form-horizontal .form-group {
        margin-right: -5px;
        margin-left: -5px;
    }
    .data-orden {
        min-height: 320px;
        max-height: 320px;
    }
</style>
<script>
    
    $(function () {
        var tabla = $('.multiple-input-list > tbody > tr > .list-cell__proceso_id').length;
        if (tabla > 0) {
            var i;
            for (i = 0; i < tabla; i++) {
                var procesoId = "orden-ordenprocesos-" + i + "-proceso_id";
                var value = $("#" + procesoId).val();
                var responsableDepdrop = "#orden-ordenprocesos-" + i + "-responsable_id";
                var value2 = $(responsableDepdrop).val();
                enviarDataResponsable(value, responsableDepdrop,value2);
            }
        }

        $("#btn-observacion").click(function () {
            $("#text-observacion").toggle();
        });
        jQuery('#w5').on('afterAddRow', function (e, row, currentIndex) {
            var clonId = '#orden-ordenprocesos-' + currentIndex + '-id';
            $(clonId).val('');
            var fecha = $('#orden-fecha_entrega').val();
            fechaFinProcesos(fecha);
            var procesoId = "orden-ordenprocesos-" + currentIndex + "-proceso_id";
            var value = $("#" + procesoId).val();
            var responsableDepdrop = "#orden-ordenprocesos-" + currentIndex + "-responsable_id";
            var value2 = $(responsableDepdrop).val();
            enviarDataResponsable(value, responsableDepdrop,value2);
        }).on('beforeDeleteRow', function (e, row, currentIndex) {
            var clonId = '#orden-ordenprocesos-' + currentIndex + '-id';
            var id = $(clonId).val();
            var Pregunta = confirm('Estas seguro de borrar este proceso?');
            if (Pregunta == true) {
                if (id) {
                    console.log(id);
                }
            } else {
                return false;
            }
        });
    });

    function fechaFinProcesos(fecha) {
        var value = $('#orden-fecha_entrega').val();
        if (value != "") {
            var tabla = $('.multiple-input-list > tbody > tr > .list-cell__fecha_fin').length;
            if (tabla > 0) {
                var i;
                for (i = 0; i < tabla; i++) {
                    var finData = "#orden-ordenprocesos-" + i + "-fecha_fin";
                    $(finData).kvDatepicker({
                        format: 'yyyy-mm-dd',
                        clearDates: true,
                        autoclose: true,
                        endDate: fecha,
                    });
                }
            }
        } else {
            $.growlUI('Error', '<div class="alert alert-danger">La fecha Entrega no puede estar vacia</div>');
        }
    }

    function consultarEmpleados(data) {
        var id = data[0].id;
        var value = $("#" + id).val();
        var separarId = id.split("-");
        var index = parseInt(separarId[2]);
        var responsableDepdrop = '#' + separarId[0] + '-' + separarId[1] + '-' + index + '-responsable_id';
        var value2 = '';
        bloquearCarga();
        enviarDataResponsable(value, responsableDepdrop,value2);
        desbloquearCarga();
    }

    function enviarDataResponsable(value, responsableDepdrop,value2) {
        var sucursal_id = '<?=$sucursal_id?>';
        if (value != '') {
            $.ajax({
                url: '<?= Yii::getAlias("@web") ?>' + "/orden/consultar-responsable?proceso_id=" + value + "&sucursal_id=" + sucursal_id,
                type: "POST",
                async: false,
                success: function (data, textStatus, jqXHR) {
                    $(responsableDepdrop).empty();
                    $(responsableDepdrop).append(data);
                },
                error: function (jqXHR, status, error) {
                    desbloquearCarga();
                    $.growlUI('Error', '<div class="alert alert-danger">' + error + '</div>');
                }
            });
            $(responsableDepdrop).val(value2);
        }
    }
</script>
