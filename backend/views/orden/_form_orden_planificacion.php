<?php

use kartik\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;
use kartik\widgets\Select2;
use lcaiza\ajaxcrud\AurySolutionsWidget;
use mdm\admin\components\Helper;
use unclead\multipleinput\MultipleInput;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yiister\gentelella\widgets\Panel;

/* @var $this yii\web\View */
/* @var $model common\models\Orden */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Planificar Orden';
$this->params['breadcrumbs'][] = ['label' => 'Ordenes', 'url' => ['/orden/planificacion']];
$this->params['breadcrumbs'][] = $this->title;

//echo "<pre>";
$empleadosSucursal = json_encode($empleadosSucursal);
//print_r($empleadosSucursal);
//echo "</pre>";
//die();
?>
<?= AurySolutionsWidget::widget() ?>
<style>
    p{
        margin: 0;
        padding: 1px;
    }
</style>
<div class="orden-form">
    <?php
    $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_HORIZONTAL,
                'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_LARGE]
    ]);
    ?>
    <?php
    Panel::begin(['header' => 'Orden de Trabajo'])
    ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-5">
                <?php
                Panel::begin(
                        [
                            'header' => 'Datos de la Orden',
                            'icon' => 'cog',
                            'options' => [
                                'class' => "x_panel data-orden"
                            ]
                        ]
                )
                ?>
                <?php
                $lista = ArrayHelper::map(\common\models\Seguro::find()->where(['estado' => 1])->all(), 'id', 'nombre');
                echo $form->field($model, 'seguro_id')->widget(Select2::classname(), [
                    'data' => $lista,
                    'options' => ['placeholder' => 'Seleccione ...'],
                    'pluginOptions' => ['allowClear' => true]
                ])
                ?>
                <?php
                $lista = ArrayHelper::map(\common\models\Broker::find()->where(['estado' => 1])->all(), 'id', 'nombre');
                echo $form->field($model, 'broker_id')->widget(Select2::classname(), [
                    'data' => $lista,
                    'options' => ['placeholder' => 'Seleccione ...'],
                    'pluginOptions' => ['allowClear' => true]
                ])
                ?>
                <?php
                echo $form->field($model, 'fecha_recibe')->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Seleccione la fecha ...'],
                    'disabled' => true,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'startDate' => date('Y-m-d'),
                    ],
                ]);
                ?>
                <?php
                echo $form->field($model, 'fecha_salida')->widget(\kartik\widgets\DatePicker::classname(), [
                    'options' => ['placeholder' => 'Seleccione la fecha ...'],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'startDate' => date('Y-m-d', strtotime($model->fecha_recibe)),
                    ],
                    'pluginEvents' => [
                        "changeDate" => "function(e) {  fechaFinProcesos(e.date); }",
                    ]
                ])
                ?>
                <?php Panel::end() ?>
            </div>
            <div class="col-sm-7">
                <?php
                Panel::begin(
                        [
                            'header' => 'Datos Cliente',
                            'icon' => 'cog',
                            'options' => [
                                'class' => "x_panel data-orden"
                            ]
                        ]
                )
                ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <p>
                                <label>Cliente :</label> <span><?= $model->cliente->nombres ?></span>
                            </p>
                            <p>
                                <label>Direccion :</label> <span><?= ($model->cliente->direccion) ? $model->cliente->direccion : '' ?></span>
                            </p>
                            <p>
                                <label>Telefono :</label> <span><?= ($model->cliente->telefono) ? $model->cliente->telefono : '' ?></span>
                            </p>
                            <p>
                                <label>Celular :</label> <span><?= ($model->cliente->celular) ? $model->cliente->celular : '' ?></span>
                            </p>
                            <p>
                                <label>Correo :</label> <span><?= ($model->cliente->user) ? $model->cliente->user->email : '' ?></span>
                            </p>
                            <p>
                                <label>Marca :</label> <span><?= ($model->vehiculo->marca) ? $model->vehiculo->marca->nombre : '' ?></span>
                            </p>
                            <p>
                                <label>Modelo :</label> <span><?= ($model->vehiculo->modelo) ? $model->vehiculo->modelo->nombre : '' ?></span>
                            </p>
                            <p>
                                <label>Placa :</label> <span><?= ($model->vehiculo_id) ? $model->vehiculo->placa : '' ?></span>
                            </p>
                        </div>
                        <div class="col-sm-6">
                            <?php
                            $directory = Yii::getAlias("@backend") . "/web/uploads/orden/$model->id/galery/img/";
                            if (is_dir($directory)):
                                ?>
                                <div class="row">
                                    <?php
                                    $images = glob("$directory{*.gif,*.jpg,*.png,*.jpeg}", GLOB_BRACE);
                                    foreach ($images as $v) {
                                        $nombre = str_replace($directory, '', $v);
                                        $img = Yii::getAlias("@web") . "/uploads/orden/$model->id/galery/img/$nombre";
                                        echo '<img src="' . $img . '" border="0" style="width:100px;float:left;margin:2px;" />';
                                    }
                                    ?>
                                </div>
                                <?php
                            endif;
                            ?>
                            <br />
                            <?php
                            Panel::begin();
                            ?>
                            <h6 style="margin: 0; padding: 0;" class="text-center text-uppercase">Calificar Orden</h6>
                            <?php
                            $lista = ArrayHelper::map(common\models\OrdenEstado::find()->where("id IN (1,2)")->all(), 'id', 'nombre');
                            echo $form->field($model, 'estado_id')->widget(Select2::classname(), [
                                'data' => $lista,
                                'options' => ['placeholder' => 'Seleccione ...'],
                                'pluginOptions' => ['allowClear' => true]
                            ]);
                            ?>
                            <?php
                            Panel::end();
                            ?>
                        </div>
                    </div>
                </div>
                <script>
//                    function bloquearCarga() {
//                        $.blockUI({
//                            message: 'Procesando por favor espere...',
//                            baseZ: 2000
//                        });
//                    }
//
//                    function desbloquearCarga() {
//                        $.unblockUI();
//                    }

//                    function reloadContent(cliente) {
//                        $.pjax.reload({container: '#data-filtro', async: false});
//                        $('#cliente_id').val(cliente).trigger('change');
//                        dataVehiculos();
//                    }

                    $(function () {
//                        $("#cliente_id").change(function () {
//                            dataVehiculos();
//                        });
                    });

//                    function dataVehiculos() {
//                        var cliente_id = $('#cliente_id').val();
//                        $("#lbl_cliente").text('...');
//                        $("#lbl_documento").text('...');
//                        $("#lbl_email").text('...');
//                        $("#lbl_tel").text('...');
//                        $("#lbl_cell").text('...');
//                        $("#boton-vehiculos").empty();
//                        $("#vehiculo_id").empty();
//                        $("#boton-vehiculos").append('<button id="btn_vehiculo" type="button" class="btn btn-success" disabled="true"><i class="fa fa-car"></i></button>');
//                        if (cliente_id != '') {
//                            bloquearCarga();
//                            $.ajax({
//                                url: '<?//= Yii::getAlias("@web") ?>' + "/orden/consultar?cliente_id=" + cliente_id,
//                                type: "POST",
//                                async: false,
//                                success: function (data, textStatus, jqXHR) {
//                                    desbloquearCarga();
//                                    $("#lbl_cliente").text(data.nombres);
//                                    $("#lbl_documento").text(data.documento);
//                                    $("#lbl_email").text(data.email);
//                                    $("#lbl_tel").text(data.telefono);
//                                    $("#lbl_cell").text(data.celular);
//                                    $("#cliente_id").val(data.id);
//                                    $("#boton-vehiculos").empty();
//                                    $("#boton-vehiculos").append(data.boton);
//                                    $("#vehiculo_id").empty();
//                                    $("#vehiculo_id").append(data.vehiculo);
//                                },
//                                error: function (jqXHR, status, error) {
//                                    desbloquearCarga();
//                                    $.growlUI('Error', '<div class="alert alert-danger">' + error + '</div>');
//                                }
//                            });
//                        }
//                    }
                </script>
                <?php
                Panel::end();
                ?>
            </div>
            <div class="row container-fluid">
                <div class="col-lg-12 form-group highlight-addon field-observacion">
                    <textarea name="Orden[observacion_ingreso]" id="observacion" class="form-control"><?= $model->observacion_ingreso ?></textarea>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <?php
            Panel::begin(
                    [
                        'header' => 'Informacion de Procesos',
                        'icon' => 'cog',
                    ]
            );
            echo $form->field($model, 'ordenProcesos', ['enableLabel' => false])->widget(MultipleInput::className(), [
                'max' => 10,
                'cloneButton' => true,
                'allowEmptyList' => true,
                'enableGuessTitle' => true,
                'addButtonPosition' => MultipleInput::POS_HEADER,
                'columns' => [
                    [
                        'name' => 'id',
                        'title' => '',
                        'type' => 'hiddenInput',
                    ],
                    [
                        'name' => 'fase',
                        'title' => 'Fase',
                    ],
                    [
                        'name' => 'proceso_id',
                        'type' => 'dropDownList',
                        'title' => 'Area',
                        'items' => ArrayHelper::map($procesos, 'id', 'nombre'),
                        'headerOptions' => [
                            'class' => 'col-lg-2'
                        ],
                        'options' => [
                            'onchange' => "consultarEmpleados($(this))",
                            'prompt' => 'Seleccione',
                        ]
                    ],
                    [
                        'name' => 'observacion',
                        'title' => 'Observacion Detectada',
                        'headerOptions' => [
                            'class' => 'col-lg-4'
                        ],
                    ],
                    [
                        'name' => 'fecha_inicio',
                        'type' => \kartik\date\DatePicker::className(),
                        'title' => 'Fecha Inicio',
                        'value' => function ($data) {
                            return ($data['fecha_inicio'] ? date('Y-m-d', strtotime($data['fecha_inicio'])) : '');
                        },
                        'options' => [
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true,
                                'autoclose' => true,
                                'startDate' => ($model->fecha_recibe ? date('Y-m-d', strtotime($model->fecha_recibe)) : ''),
                                'endDate' => ($model->fecha_entrega ? date('Y-m-d', strtotime($model->fecha_entrega)) : ''),
                            ],
                        ],
                        'headerOptions' => [
                            'class' => 'col-lg-2'
                        ],
                    ],
                    [
                        'name' => 'fecha_fin',
                        'type' => \kartik\date\DatePicker::className(),
                        'title' => 'Fecha Fin',
                        'value' => function ($data) {
                            return ($data['fecha_fin'] ? date('Y-m-d', strtotime($data['fecha_fin'])) : '');
                        },
                        'options' => [
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true,
                                'autoclose' => true,
                                'startDate' => ($model->fecha_recibe ? date('Y-m-d', strtotime($model->fecha_recibe)) : ''),
                                'endDate' => ($model->fecha_entrega ? date('Y-m-d', strtotime($model->fecha_entrega)) : ''),
                            ],
                        ],
                        'headerOptions' => [
                            'class' => 'col-lg-2'
                        ],
                    ],
                    [
                        'name' => 'responsable_id',
                        'type' => 'dropDownList',
                        'title' => 'Responsable',
                        'value' => function ($data) {
                            return ($data['responsable_id'] ? $data['responsable_id'] : '');
                        },
                        'items' => function($data) {
                            $listado = common\models\EmpleadoSucursal::find()->where(['encargado_proceso' => 1, 'proceso_id' => $data['proceso_id']])->all(); //falta por sucursal
                            $empresaSucursal = ['' => 'Seleccione'];
                            foreach ($listado as $lista) {
                                $empresaSucursal[$lista->empleado_id] = $lista->empleado->nombres;
                            }
                            return $empresaSucursal;
                        }, // ArrayHelper::map($empleadosSucursal, 'empleado_id', 'empleado.nombres'),
                        'headerOptions' => [
                            'class' => 'col-lg-2'
                        ],
                    ],
                ]
            ]);
            Panel::end();
            ?>
        </div>
        <div class="row">
            <div class="col-lg-9">
            </div>
            <div class="form-group text-right" style="padding: 10px;">
                <?= Html::a('Regresar', ['orden/planificacion'], ['class' => 'btn btn-warning', 'title' => 'Listado de Ordenes']) ?>
                <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php Panel::end() ?>
    <?php ActiveForm::end(); ?>
</div>
<?php
Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "", // Siempre lo necesitas para plugin jquery
    "options" => ['tabindex' => false],
    "size" => Modal::SIZE_LARGE
])
?>
<?php Modal::end(); ?>
<style>
    .data-orden {
        min-height: 280px;
        max-height: 280px;
    }
</style>
<script>
    $(function () {

        $("#btn-observacion").click(function () {
            $("#text-observacion").toggle();
        });

        jQuery('#w5').on('afterAddRow', function (e, row, currentIndex) {
            var clonId = '#orden-ordenprocesos-' + currentIndex + '-id';
            $(clonId).val('');
            var fecha = $('#orden-fecha_entrega').val();
            fechaFinProcesos(fecha);
        }).on('beforeDeleteRow', function (e, row, currentIndex) {
            var clonId = '#orden-ordenprocesos-' + currentIndex + '-id';
            var id = $(clonId).val();
            var Pregunta = confirm('Estas seguro de borrar este proceso?');
            if (Pregunta == true) {
                if (id) {
                    console.log(id);
                }
            } else {
                return false;
            }
        });
    });

    var empleados = <?= $empleadosSucursal ?>;

    function fechaFinProcesos(fecha) {
        var value = $('#orden-fecha_entrega').val();
        if (value != "") {
            var tabla = $('.multiple-input-list > tbody > tr > .list-cell__fecha_fin').length;
            if (tabla > 0) {
                var i;
                for (i = 0; i < tabla; i++) {
                    var finData = "#orden-ordenprocesos-" + i + "-fecha_fin";
                    $(finData).kvDatepicker({
                        format: 'yyyy-mm-dd',
                        clearDates: true,
                        autoclose: true,
                        endDate: fecha,
                    });
                }
            }
        } else {
            $.growlUI('Error', '<div class="alert alert-danger">La fecha Entrega no puede estar vacia</div>');
        }
    }

    function consultarEmpleados(data) {
        var object = data[0].id;
        var id = $("#" + object).val();
        var responsableDepdrop = "#" + object.replace("proceso_id", "responsable_id");

        bloquearCarga();
        enviarDataResponsable(id, responsableDepdrop, '');
        desbloquearCarga();
    }

    function enviarDataResponsable(proceso_id, responsableDepdrop, responsable_id) {
        $(responsableDepdrop).empty();
        $(responsableDepdrop).append("<option value=''>Seleccione</option>");
        jQuery.each(empleados, function (i, val) {
            if (val.proceso_id == proceso_id) {
                var select = '';
                if (val.empleado_id == responsable_id) {
                    select = 'selected';
                }
                $(responsableDepdrop).append("<option value='" + val.empleado_id + "'>" + val.nombres + "</option>");
            }
        });
    }

    function bloquearCarga() {
        $.blockUI({
            message: 'Procesando por favor espere...',
            baseZ: 2000
        });
    }

    function desbloquearCarga() {
        $.unblockUI();
    }
</script>