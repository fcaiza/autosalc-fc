<?php

use yii\widgets\DetailView;
use yiister\gentelella\widgets\Panel;

/* @var $this yii\web\View */
/* @var $model common\models\Orden */
?>
<style>
    p{
        margin: 0;
        padding: 0;
    }
</style>
<div class="orden-view">

    <?php
    Panel::begin(
            [
                'header' => 'Datos de la Orden',
                'icon' => 'cog',
                'options' => [
                    'class' => "x_panel data-orden"
                ]
            ]
    )
    ?>
    <div class="row">
        <div class="col-lg-6">
            <p>
                <label>Aseguradora :</label> <span><?= ($model->seguro_id) ? $model->seguro->nombre : '' ?></span>
            </p>
            <p>
                <label>Broker :</label> <span><?= ($model->broker_id) ? $model->broker->nombre : '' ?></span>
            </p>
            <p>
                <label>Empleado Recibe :</label> <span><?= ($model->empleado_recibe_id) ? $model->empleadoRecibe->nombres : '' ?></span>
            </p>
            <p>
                <label>Fecha Recibe :</label> <span><?= ($model->fecha_recibe) ? $model->fecha_recibe : '' ?></span>
            </p>
            <p>
                <label>Fecha Salida :</label> <span><?= ($model->fecha_salida) ? $model->fecha_salida : '' ?></span>
            </p>
            <p>
                <label>Observacion :</label> <?= ($model->observacion_ingreso) ? $model->observacion_ingreso : '' ?>
            </p>

        </div>
        <div class="col-lg-6">
            <p>
                <label>Cliente :</label> <span><?= $model->cliente->nombres ?></span>
            </p>
            <p>
                <label>Direccion :</label> <span><?= ($model->cliente->direccion) ? $model->cliente->direccion : '' ?></span>
            </p>
            <p>
                <label>Telefono :</label> <span><?= ($model->cliente->telefono) ? $model->cliente->telefono : '' ?></span>
            </p>
            <p>
                <label>Celular :</label> <span><?= ($model->cliente->celular) ? $model->cliente->celular : '' ?></span>
            </p>
            <p>
                <label>Correo :</label> <span><?= ($model->cliente->user) ? $model->cliente->user->email : '' ?></span>
            </p>
            <p>
                <label>Vehiculo :</label> <span><?= ($model->vehiculo->marca) ? $model->vehiculo->marca->nombre : '' ?></span>
            </p>
            <p>
                <label>Placa :</label> <span><?= ($model->vehiculo_id) ? $model->vehiculo->placa : '' ?></span>
            </p>
        </div>
    </div>
    <hr />
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th class="col-lg-1">Item</th>
                <th class="col-lg-2">Area</th>
                <th>Observacion</th>
                <th class="col-lg-2">Fecha Inicio</th>
                <th class="col-lg-2">Fecha Fin</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($model->ordenProcesos):
                $ordenProceso = common\models\OrdenProceso::find()
                    ->where(['orden_id' => $model->id])
                    ->orderBy(['fase' => SORT_ASC])
                    ->all();
                foreach ($ordenProceso as $proceso):
                    ?>
                    <tr>
                        <td><?= $proceso->fase ?></td>
                        <td><?= $proceso->proceso->nombre ?></td>
                        <td><?= $proceso->observacion ?></td>
                        <td><?= $proceso->fecha_inicio ?></td>
                        <td><?= $proceso->fecha_fin ?></td>
                    </tr>
                    <?php
                endforeach;
            else:
                echo "<tr><td colspan='5' class='text-danger text-center'>No existen datos para mostrar</td></tr>";
            endif;
            ?>
        </tbody>
    </table>
    <?php
    $directory = Yii::getAlias("@backend") . "/web/uploads/orden/$model->id/galery/img/";
    if (is_dir($directory)):
        ?>
        <div class="row">
            <h4 class="text-center">Fotos del vehiculo <small>ingreso a los talleres</small></h4>
            <?php
            $images = glob("$directory{*.gif,*.jpg,*.png,*.jpeg}", GLOB_BRACE);
            foreach ($images as $v)
            {
                $nombre = str_replace($directory, '', $v);
                $img = Yii::getAlias("@web") . "/uploads/orden/$model->id/galery/img/$nombre";
                echo '<img src="' . $img . '" border="0" style="width:150px;float:left;margin:5px;" />';
            }
            ?>
        </div>
        <?php
    endif;
    ?>
    <?php Panel::end() ?>

    <?php
//    echo "<pre>";
//    print_r($model);
//    echo "</pre>";
    ?>

</div>
