<?php

use kartik\grid\GridView;
use mdm\admin\components\Helper;
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => 'kartik\grid\ExpandRowColumn',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detailUrl' => Url::to(['detalle']),
        'enableRowClick' => false,
        'detailRowCssClass' => GridView::TYPE_SUCCESS,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'nombre',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'apellido',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Email',
        'value' => 'user.email',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'tipo_documento_id',
        'value' => 'tipoDocumento.nombre',
        'filter' => false,
        'width' => '100px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'numero_documento',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'telefono',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'celular',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'Vehiculos',
        'format' => 'raw',
        'value' => function ($model) {
            $count = (count($model->vehiculos));
            return "<button class='btn btn-info btn-round btn-xs'> $count <i class='glyphicon glyphicon-book'></i> </button>";
        }
    ],
    [
        'class' => '\kartik\grid\BooleanColumn',
        'attribute' => 'estado',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'width' => '120px',
        'vAlign' => 'middle',
        'template' => Helper::filterActionColumn('{/global/reset-email} {/global/reset} {view} {update} {delete} '),
        'buttons' => [
            '/global/reset-email' => function ($url, $data) {
                return Html::a('<span class="glyphicon glyphicon-send"></span> ', $url, [
                    'title' => 'Reset Clave Email', 'data-toggle' => 'tooltip',
                    'role' => 'modal-remote',
                ]);
            },
            '/global/reset' => function ($url, $data) {
                return Html::a('<span class="glyphicon glyphicon-floppy-save"></span> ', $url, [
                    'title' => 'Cambiar Clave', 'data-toggle' => 'tooltip',
                    'role' => 'modal-remote',
                ]);
            },
        ],
        'urlCreator' => function ($action, $model, $key, $index) {
            if ($action == '/global/reset') {
                return Url::to([$action, 'id' => $model->user_id]);
            }
            if ($action == '/global/reset-email') {
                return Url::to([$action, 'id' => $model->user_id]);
            }
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Ver', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Editar', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Borrar',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Esta usted seguro?',
            'data-confirm-message' => 'Seguro que quiere borrar este item'],
    ],

];   