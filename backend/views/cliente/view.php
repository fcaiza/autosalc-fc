<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Cliente */
?>
<div class="cliente-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'apellido',
            [
                'attribute' => 'tipo_documento_id',
                'value' => ($model->tipo_documento_id) ? $model->tipoDocumento->nombre : ' ',
            ],
            'numero_documento',
            [
                'label' => 'Username',
                'value' => $model->user->username,
            ],
            [
                'label' => 'Email',
                'value' => $model->user->email,
            ],
            'direccion',
            'telefono',
            'celular',
            [
                'label' => 'Estado',
                'value' => ($model->estado == 1) ? 'Activo' : 'Inactivo',
            ],
            [
                'label' => 'Creacion',
                'value' => $model->created_at,
                'format' => ['DateTime', 'php:Y-m-d H:i:s']
            ],
            [
                'label' => 'Actualizacion',
                'value' => $model->updated_at,
                'format' => ['DateTime', 'php:Y-m-d H:i:s']
            ],
        ],
    ]) ?>

</div>
