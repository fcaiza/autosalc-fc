<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Cliente */

?>
<div class="cliente-create">
    <?= $this->render('_form', [
        'model' => $model,
        'user'=>$user
    ]) ?>
</div>
