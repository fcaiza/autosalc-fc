<?php

use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

//use backend\modules\transferencia\controllers\AdminSeleccionController;

/* @var $this yii\web\View */
/* @var $model common\models\Cliente */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cliente-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_LARGE]
    ]); ?>
    <div class="panel panel-default">
        <div class="container-fluid">
            <div class="panel-body col-sm-6">
                <legend>Datos Cliente</legend>
                <?php echo $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

                <?php echo $form->field($model, 'apellido')->textInput(['maxlength' => true]) ?>

                <?php $lista = ArrayHelper::map(\common\models\TipoDocumento::find()->where(['estado' => 1])->all(), 'id', 'nombre');
                echo $form->field($model, 'tipo_documento_id')->widget(Select2::classname(), [
                    'data' => $lista,
                    'options' => ['placeholder' => 'Seleccione ...'],
                    'pluginOptions' => ['allowClear' => true]
                ])
                ?>
                <?php echo $form->field($model, 'numero_documento')->textInput(['maxlength' => true, 'placeholder' => "Numero documento"]) ?>

                <?php echo $form->field($model, 'telefono')->textInput(['maxlength' => true, 'placeholder' => "Telefono"]) ?>

                <?php echo $form->field($model, 'celular')->textInput(['maxlength' => true, 'placeholder' => "Celular"]) ?>

                <?php echo $form->field($model, 'direccion')->textInput(['maxlength' => true, 'placeholder' => "Dirección"]) ?>

            </div>
            <div class="panel-body col-sm-6">
                <legend>Información Usuario</legend>
                <?php echo $form->field($user, 'username')->textInput(['maxlength' => true]) ?>

                <?php echo $form->field($user, 'email')->textInput(['maxlength' => true]) ?>

                <?php echo $form->field($model, 'estado')->dropDownList(ArrayHelper::map(\common\models\CatalogoGeneral::listarCatalogo(1), 'value', 'nombre'), ['prompt' => 'Seleccione']) ?>

            </div>
        </div>
    </div>
    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>

<script>
    $(function () {
        $("#cliente-nombre").change(function () {
            var nombre = ucwords($(this).val());
            $(this).val(nombre);
        });
        $("#cliente-apellido").change(function () {
            var nombre = ucwords($(this).val());
            $(this).val(nombre);
        });
        $("#cliente-nombre").blur(function () {
            var random = guid();
            var nombre = $(this).val().substring(0, 4).toUpperCase();
            $("#user-username").val(nombre + '-' + random);
        });
    });

    function ucwords(str) {
        return (str + '')
            .replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
                return $1.toUpperCase();
            });
    }

    function guid() {
        var sGuid = "";
        for (var i = 0; i < 6; i++) {
            sGuid += Math.floor(Math.random() * 0xF).toString(0xF).toUpperCase();
        }
        return sGuid;
    }

</script>
