<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Broker */

?>
<div class="broker-create">
    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user,
    ]) ?>
</div>
