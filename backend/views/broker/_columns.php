<?php

use mdm\admin\components\Helper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'imagen',
        'format' => 'raw',
        'filter' => false,
        'enableSorting' => false,
        'value' => function ($model) {
            $imagen = '';
            if ($model->imagen) {
                $imagen = Html::img(Yii::getAlias('@web') . '/img/broker/' . $model->id . '/' . $model->imagen, ['width' => '100']);
            } else {

                $imagen = Html::img(Yii::getAlias('@web') . '/img/sin-imagen.jpg', ['width' => '100']);
            }
            return $imagen;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'nombre',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'ruc',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Username',
        'value' => 'user.username',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Email',
        'value' => 'user.email',
    ],
    [
        'class' => '\kartik\grid\BooleanColumn',
        'attribute' => 'estado',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // 'format' => ['DateTime', 'php: Y-m-d H:i:s'],
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'updated_at',
    // 'format' => ['DateTime', 'php: Y-m-d H:i:s'],
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'width' => '100px',
        'vAlign' => 'middle',
        'template' => Helper::filterActionColumn('{/global/reset-email} {/global/reset} {view} {update} {delete} '),
        'buttons' => [
            '/global/reset-email' => function ($url, $data) {
                return Html::a('<span class="glyphicon glyphicon-send"></span> ', $url, [
                    'title' => 'Reset Clave Email', 'data-toggle' => 'tooltip',
                    'role' => 'modal-remote',
                ]);
            },
            '/global/reset' => function ($url, $data) {
                return Html::a('<span class="glyphicon glyphicon-floppy-save"></span> ', $url, [
                    'title' => 'Cambiar Clave', 'data-toggle' => 'tooltip',
                    'role' => 'modal-remote',
                ]);
            },
        ],
        'urlCreator' => function ($action, $model, $key, $index) {
            if ($action == '/global/reset') {
                return Url::to([$action, 'id' => $model->user_id]);
            }
            if ($action == '/global/reset-email') {
                return Url::to([$action, 'id' => $model->user_id]);
            }
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Ver', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Editar', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Borrar',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Esta usted seguro?',
            'data-confirm-message' => 'Seguro que quiere borrar este item'],
    ],

];   