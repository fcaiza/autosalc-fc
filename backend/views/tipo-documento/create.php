<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TipoDocumento */

?>
<div class="tipo-documento-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
