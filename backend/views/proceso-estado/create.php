<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProcesoEstado */

?>
<div class="proceso-estado-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
