<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ProcesoEstado */
?>
<div class="proceso-estado-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'codigo',
			[
                'label' => 'Estado',
                'value' => ($model->estado == 1) ? 'Activo' : 'Inactivo',
            ],
			[
                'label' => 'Creacion',
                'value' => $model->created_at,
                'format' => ['DateTime', 'php:Y-m-d H:i:s']
            ],
			[
                'label' => 'Actualizacion',
                'value' => $model->updated_at,
                'format' => ['DateTime', 'php:Y-m-d H:i:s']
            ],
        ],
    ]) ?>

</div>
