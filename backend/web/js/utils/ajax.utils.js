/***
 * Contiene utilidades para acciones tipo ajax
 * @author Luis Malquin
 * @requires (jQuery, jquery.blockUI, krajeeDialog)
 * @type {{loadPartialView: $.utilsAjax.loadPartialView, ajaxSubmit: $.utilsAjax.ajaxSubmit, ajaxDelete: $.utilsAjax.ajaxDelete, ajaxSend: $.utilsAjax.ajaxSend}}
 */

$.utilsAjax={
    loadPartialView: function(url,container,callback){
        if(!$('.growlUI').is(':visible')){
            $.blockUI({
                message: 'Procesando por favor espere...',
            });
        }

        var contentObject='';
        if(typeof container === 'object' ){
            contentObject=container;
        }else{
            contentObject=$("#"+container);
        }
        contentObject.load(url,function(response, status, xhr ){
            if ( status == "error" ){
                $.unblockUI();
                $.growlUI('Error',response);
            }else if(callback!=undefined){
                callback();
            }else{
                $.unblockUI();
            }
        });
    },
    ajaxSubmit: function(formSubmit,onSuccess,onError){
        var data =formSubmit.serializeArray();
        var url = formSubmit.attr('action');

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            async: false,
            data: data,
            beforeSend: function(){
                $.blockUI({
                    message: 'Procesando por favor espere...',
                });
            }
        })
            .done(function(response) {
                $.unblockUI();
                if (response.success) {
                    if(onSuccess!=undefined){
                        onSuccess(response);
                    }
                }else{
                    if(response.error!=undefined) {
                        $.each(response.error, function (key, val) {
                            formSubmit.yiiActiveForm('updateAttribute', key, [val]);
                        });
                    }
                    if(onError!=undefined){
                        onError(response);
                    }else{
                        $.growlUI('Error','<div class="alert alert-danger">'+response.message+'</div>');
                    }
                }

            })
            .fail(function() {
                $.unblockUI();
                $.growlUI('Error','<div class="alert alert-danger">Se produjo un error</div>');

            });
    },
    ajaxDelete: function(url,onSuccess){
        krajeeDialog.confirm('¿Esta seguro de eliminar el registro?',function(result){
            if(result){
                $.utilsAjax.ajaxSend(url,onSuccess);
            }
        });
    },
    ajaxSend: function(url,onSuccess,onError){
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            async: false,
            beforeSend: function(){
                $.blockUI({
                    message: 'Procesando por favor espere...',
                });
            }
        })
            .done(function(response) {
                $.unblockUI();
                if (response.success) {
                    if(onSuccess!=undefined){
                        onSuccess(response);
                    }
                }else{
                    $.growlUI('Error','<div class="alert alert-danger">'+response.message+'</div>');
                    if(onError!=undefined){
                        onError();
                    }
                }
            })
            .fail(function() {
                $.unblockUI();
                $.growlUI('Error','<div class="alert alert-danger">Se produjo un error</div>');
                if(onError!=undefined){
                    onError();
                }
            });
    }
}

$(function(){

    if($.blockUI !=undefined){
        $.blockUI.defaults.baseZ=100000;
    }
})