<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/card.css'
//        'css/site.css',
    ];
    public $js = [
        'js/bootstrap3-typeahead.min.js',
        'js/utils/jquery.blockUI.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
//        'gentelella\assets\Asset'
    ];
     public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );
}
