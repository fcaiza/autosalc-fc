<?php

namespace backend\controllers;

use common\models\Cliente;
use common\models\Empleado;
use common\models\EmpleadoSucursal;
use common\models\Orden;
use common\models\OrdenProceso;
use common\models\OrdenTrabajo;
use common\models\Proceso;
use common\models\search\OrdenSearch;
use common\models\UserGerarquia;
use common\models\Vehiculo;
use kartik\mpdf\Pdf;
use mdm\admin\components\Helper;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * OrdenController implements the CRUD actions for Orden model.
 */
class OrdenController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Orden models.
     * @return mixed
     */
    public function actionIndex() {
        $usuarios = UserGerarquia::obtenerEmpleadoUserIdGerarquia();
        if (Yii::$app->user->id > 1)
            array_push($usuarios, ['empleado_recibe_id' => Yii::$app->user->identity->empleado->id]);
        $searchModel = new OrdenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, "1", $usuarios);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Orden models.
     * @return mixed
     */
    public function actionPlanificacion() {
        $usuarios = UserGerarquia::obtenerEmpleadoUserIdGerarquia();
        if (Yii::$app->user->id > 1)
            array_push($usuarios, ['empleado_recibe_id' => Yii::$app->user->identity->empleado->id]);
        $searchModel = new OrdenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, "1,2", $usuarios);

        return $this->render('planificacion', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAsignacion() {
        $usuarios = UserGerarquia::obtenerEmpleadoUserIdGerarquia();
        if (Yii::$app->user->id > 1)
            array_push($usuarios, ['responsable_id' => Yii::$app->user->identity->empleado->id]);
        $searchModel = new OrdenSearch();
        $dataProvider = $searchModel->searchAsignacion(Yii::$app->request->queryParams, $usuarios);

        return $this->render('asignacion', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Orden models.
     * @return mixed
     */
    public function actionEjecucion() {
        $searchModel = new OrdenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, "4");

        return $this->render('ejecucion', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Orden models.
     * @return mixed
     */
    public function actionNotificacion() {
        $searchModel = new OrdenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, "4,5");

        return $this->render('notificacion', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Orden model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $pdf = FALSE) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if ($pdf) {
            return self::returnPdf($model);
        } else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Resumen del registro #: " . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                Html::a('Tranformar a Pdf', ['view', 'id' => $id, 'pdf' => TRUE], ['class' => 'btn btn-info', 'target' => '_blank'])
            ];
        }
    }

    public function returnPdf($model) {

        $content = $this->renderPartial('view', ['model' => $model,]);
        $footer = '<table class="document-header" align="center" cellpadding="15" cellspacing="0" border="0" width="100%">';
        $footer .= '<tr>';
        $footer .= '<td valign="middle" height="10" width="50%">AutosALC <a style="color: white; text-decoration: none;" href="#">AutosALC</a></td>';
        $footer .= '<td valign="middle" align="right" height="10" width="50%">Pagina - {PAGENO}<br/>Creado {DATE j-m-Y}</td>';
        $footer .= '</tr>';
        $footer .= '</table>';

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'filename' => "orden.pdf",
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => ['title' => 'Orden de ingreso'],
            'methods' => [
                'SetHeader' => ['Orden de ingreso (AutosALC)'],
                'SetFooter' => $footer,
            ]
        ]);
        return $pdf->render();
    }

    /**
     * Creates a new Orden model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($action) {
        $request = Yii::$app->request;
        $posts = Yii::$app->request->post();

        $model = new Orden();
        $model->numero_orden = $model->generarNumero();
        $empleado = Empleado::findOne(['user_id' => (\Yii::$app->user->id == 1) ? 2 : \Yii::$app->user->id]);
        $sucursal_id = $model->obtenerSucursal();

        $model->fecha_recibe = date('Y-m-d H:i:s');
        $model->empleado_recibe_id = $empleado->id;
        $model->estado_id = 1;
        $model->sucursal_id = $sucursal_id;

        if ($model->load($request->post()) && $model->validate()) {
            $postOrdenProcesos = isset($posts['Orden']['ordenProcesos']) ? $posts['Orden']['ordenProcesos'] : '';
            if ($postOrdenProcesos) {
                $transaction = Orden::getDb()->beginTransaction();
                try {
                    $model->numero_orden = $model->generarNumero(1);
                    $model->save();
                    $oldIds = [];
                    $num_procesos = 0;
                    foreach ($postOrdenProcesos as $i => $ordenPro) {
                        $ordenProceso = OrdenProceso::findOne($ordenPro['id']);
                        if (!$ordenProceso) {
                            $ordenProceso = new OrdenProceso();
                        }
                        $ordenProceso->proceso_id = $ordenPro['proceso_id'];
                        $ordenProceso->orden_id = $model->id;
                        $ordenProceso->observacion = $ordenPro['observacion'];
                        $ordenProceso->fecha_inicio = $ordenPro['fecha_inicio'];
                        $ordenProceso->fecha_fin = $ordenPro['fecha_fin'];
                        $ordenProceso->fase = $ordenPro['fase'];
                        $ordenProceso->estado_id = 1;
                        if (!$ordenProceso->save()) {
                            $transaction->rollBack();
                            $html = "<ul>";
                            foreach ($ordenProceso->errors as $error => $value) {
                                $html .= "<li>$error : {$value[0]}</li>";
                            }
                            $html .= "</ul>";
                            Yii::$app->session->setFlash('danger', "Existen los siguientes errores " . $html);
                            return self::returnError($model, $sucursal_id, $ordenProceso->errors, $action);
                        }
                        array_push($oldIds, $ordenProceso->id);
                        $num_procesos++;
                    }
                    $model->numero_servicio = $num_procesos;
                    $model->save();
                    $transaction->commit();
                    return $this->redirect(['update', 'id' => $model->id, 'action' => 'ingreso']);
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            } else {
                Yii::$app->session->setFlash('danger', "No existen items de la orden para generarla");
                OrdenProceso::deleteAll(['orden_id' => $model->id]);
            }
            return self::returnError($model, $sucursal_id, '', $action);
        } else {
            return self::returnError($model, $sucursal_id, $model->getErrors(), $action);
        }
    }

    /**
     * Updates an existing Orden model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $action) {
        $request = Yii::$app->request;
        $model = Orden::findOne($id);
        $posts = Yii::$app->request->post();

        if ($model->load($request->post()) && $model->validate()) {
            $postOrdenProcesos = isset($posts['Orden']['ordenProcesos']) ? $posts['Orden']['ordenProcesos'] : '';
            if ($postOrdenProcesos) {
                $transaction = Orden::getDb()->beginTransaction();
                try {
                    $num_procesos = 0;
                    $oldIds = '';
                    foreach ($postOrdenProcesos as $i => $ordenPro) {
                        $ordenProceso = OrdenProceso::findOne($ordenPro['id']);
                        if (!$ordenProceso) {
                            $ordenProceso = new OrdenProceso();
                        }
                        $ordenProceso->proceso_id = $ordenPro['proceso_id'];
                        $ordenProceso->orden_id = $id;
                        $ordenProceso->observacion = $ordenPro['observacion'];
                        $ordenProceso->fecha_inicio = $ordenPro['fecha_inicio'];
                        $ordenProceso->fecha_fin = $ordenPro['fecha_fin'];
                        $ordenProceso->responsable_id = isset($ordenPro['responsable_id']) ? $ordenPro['responsable_id'] : NULL;
                        $ordenProceso->fase = $ordenPro['fase'];
                        $ordenProceso->estado_id = ($ordenProceso->estado_id) ? $ordenProceso->estado_id : 1;
                        if (!$ordenProceso->save()) {
                            $transaction->rollBack();
                            $html = "<ul>";
                            foreach ($ordenProceso->errors as $error => $value) {
                                $html .= "<li>$error : {$value[0]}</li>";
                            }
                            $html .= "</ul>";
                            Yii::$app->session->setFlash('danger', "Existen los siguientes errores " . $html);
                            return self::returnError($model, $sucursal_id, $ordenProceso->errors, $action);
                        }
                        $oldIds .= $ordenProceso->id . ',';
                        $num_procesos++;
                    }
                    $model->numero_servicio = $num_procesos;
                    $model->fecha_revisa = date('Y-m-d  H:i:s');

                    if ($model->estado_id == 2) {
                        $result = self::crearOrdenTrabajo($model);
                        if ($result->bandera) {
                            $model->estado_id = $result->estado;
                            $model->empleado_revisa_id = \Yii::$app->user->identity->empleado->id;
                            $model->save();
                            $transaction->commit();
                            return $this->redirect("../planificacion");
                        } else {
                            Yii::$app->session->setFlash('danger', "La Planificacion tiene errores " . $result->mensaje);
                            $model->estado_id = $result->estado;
                        }
                    }
                    $model->save();
                    self::deleteVacios($oldIds, $model->id);
                    $transaction->commit();
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            } else {
                Yii::$app->session->setFlash('danger', "Existen problemas para guardar la orden ");
                OrdenProceso::deleteAll(['orden_id' => $model->id]);
            }

            return self::returnError($model, $model->sucursal_id, '', $action);
        } else {
            return self::returnError($model, $model->sucursal_id, '', $action);
        }
    }

    private static function crearOrdenTrabajo($model) {
        $bandera = $mensaje = false;
        $estado = 1; // 1=>error , 3=>asignar, 4=>enproceso
        $contadorTareas = $contadorAux = 0;
        $estadoProceso = 0;

        $mensaje = '<ul>';
        foreach ($model->ordenProcesos as $proceso) {

            if (!$proceso->fecha_inicio || !$proceso->fecha_fin) {
                $mensaje .= "<li>La Fase $proceso->fase no cuneta con " .
                        ((!$proceso->fecha_inicio) ? ' Fecha inicio ' : '') .
                        ((!$proceso->fecha_fin) ? ' Fecha fin ' : '') .
                        (($proceso->validarResponsable) ? ((!$proceso->responsable_id) ? ' Revise el responsable' : '') : '') .
                        "</li>";
            } else {

                $contadorTareas++;
                $estadoProceso = 3;
                if ($proceso->validarResponsable) {
                    $contadorAux++;
                    $estadoProceso = 2;
                }

                $tarea = OrdenTrabajo::findOne(['orden_proceso_id' => $proceso->id]);
                if (!$tarea) {
                    $tarea = new OrdenTrabajo();
                    $tarea->orden_proceso_id = $proceso->id;
                    $tarea->estado_id = 1;
                    $tarea->fecha_inicio = $proceso->fecha_inicio;
                    $tarea->fecha_fin = $proceso->fecha_fin;
                    if (!$tarea->save()) {
                        $bandera = false;
                        $mensaje .= "<li>Error al guardar la orden de trabajo</li>";
                        break;
                    }
                }

                $proceso->estado_id = $estadoProceso;
                $proceso->save();
                $bandera = true;
            }
        }
        $mensaje .= '</ul>';
        (count($model->ordenProcesos) == $contadorTareas) ? $estado = 4 : '';
        ($contadorAux) ? $estado = 3 : '';

        return (object) [
                    'bandera' => $bandera,
                    'mensaje' => $mensaje,
                    'estado' => $estado
        ];
    }

    public function deleteVacios($oldIds, $id) {
        if ($oldIds) {
            $oldIds = substr($oldIds, 0, -1);
            OrdenProceso::deleteAll("orden_id = $id AND id NOT IN ($oldIds)");
        }
    }

    public function returnError($model, $sucursal_id, $error = "", $action = "ingreso") {
        $vista = "_form_orden";
        if ($action == 'planificacion') {
            $vista = "_form_orden_planificacion";
        }

        $procesos = Proceso::find()
                ->where(['estado' => 1])
                ->orderBy(['orden' => SORT_ASC])
                ->all();
        if (!$sucursal_id) {
            return $this->redirect(Yii::$app->request->referrer);
        }
        return $this->render($vista, [
                    'model' => $model,
                    'procesos' => $procesos,
                    'sucursal_id' => $sucursal_id,
                    'error' => $error,
                    'empleadosSucursal' => self::EmpleadosSucursal($sucursal_id),
        ]);
    }

    /**
     * Delete an existing Orden model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        OrdenProceso::deleteAll(['orden_id' => $model->id]);
        $model->delete();

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Orden model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orden the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Orden::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionConsultar($cliente_id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = Cliente::findOne($cliente_id);
        $vehiculos = "<option value=''>Seleccione</option>";
        if ($model->vehiculos) {
            foreach ($model->vehiculos as $vehiculo) {
//                $out[] = ['group' => $vehiculo->marca->nombre, 'id' => $vehiculo->id, 'placa' => $vehiculo->modelo->nombre . '(' . $vehiculo->placa . ')'];
                $vehiculos .= "<option value='$vehiculo->id'>$vehiculo->placa</option>";
            }
//            $campo = GlobalController::groupArray($out, "group");
//            foreach ($campo as $name => $grupo):
//                $vehiculos .= "<optgroup label='$name'>";
//                foreach ($grupo as $data2):
//                    $id = $data2['id'];
//                    $placa = $data2['placa'];
//                    $vehiculos .= "<option value='$id'>$placa]</option>";
//                endforeach;
//                $vehiculos .= "</optgroup>";
//            endforeach;
        }
        $boton = ((Helper::checkRoute('/vehiculo/create')) ? Html::a('<i class="fa fa-car"></i>', ['/vehiculo/create', 'cliente_id' => $model->id, 'bandera' => 1], ['title' => 'Agregar Vehiculo', 'class' => 'btn btn-success btn-sm btn-round', 'role' => 'modal-remote']) : '');
        return [
            'id' => $model->id,
            'nombres' => $model->getNombres(),
            'documento' => $model->numero_documento,
            'email' => $model->user->email,
            'telefono' => $model->telefono,
            'celular' => $model->celular,
            'boton' => $boton,
            'vehiculo' => $vehiculos,
        ];
    }

    public function actionVehiculosCliente() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        $campo = [];
        if (isset($_POST['depdrop_parents'])) {
            $cliente_id = $_POST['depdrop_parents'];
            $list = Vehiculo::find()->andWhere(['cliente_id' => $cliente_id])->all();
            foreach ($list as $account) {
                $out[] = ['group' => $account->marca->nombre, 'id' => $account->id, 'name' => $account->modelo->nombre . '(' . $account->placa . ')'];
            }
            $campo = GlobalController::groupArray($out, "group");
        }
        return ['output' => $campo, 'selected' => ''];
    }

    public function EmpleadosSucursal($sucursal_id) {
        $listado = EmpleadoSucursal::find()->where(['sucursal_id' => $sucursal_id, 'encargado_proceso' => 1])->all();
        $empresaSucursal = [];
        foreach ($listado as $data) {
            $empresaSucursal[] = [
                'proceso_id' => $data->proceso_id,
                'empleado_id' => $data->empleado_id,
                'nombres' => $data->empleado->nombres
            ];
        }
        return $empresaSucursal;
    }

    public function actionConsultarResponsable($proceso_id, $sucursal_id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $select = "<option value=''>Seleccione</option>";
        $empresaSucursal = EmpleadoSucursal::find()->where(['sucursal_id' => $sucursal_id, 'proceso_id' => $proceso_id])->all();
        if ($empresaSucursal) {
            foreach ($empresaSucursal as $empleados):
                $id = $empleados->empleado->id;
                $empleadoName = $empleados->empleado->getNombres();
                $select .= "<option value='$id'>$empleadoName</option>";
            endforeach;
        }
        return $select;
    }

}
