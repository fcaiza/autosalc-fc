<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'reset-password'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $cookies = Yii::$app->request->cookies;

        if (!$cookies->has('sucursal')) {
            $empleado_id = (Yii::$app->user->identity->empleado) ? Yii::$app->user->identity->empleado->id : 1;
            $sucursal = \common\models\Sucursal::find()
                    ->innerJoinWith('empleadoSucursals')
                    ->where(['empleado_id' => $empleado_id])
                    ->orderBy(['por_defecto' => SORT_DESC])
                    ->one();

            if ($sucursal) {
                $cookies = Yii::$app->response->cookies;
                $cookies->add(new \yii\web\Cookie([
                            'name' => 'sucursal',
                            'value' => $sucursal->id
                ]));
            } else {
                if (Yii::$app->user->id > 1) {
                    $this->layout = 'main-error';
                    return $this->render('error', [
                                'name' => 'Estimado ' . Yii::$app->user->identity->username,
                                'message' => 'Usted no tiene permisos para acceder al sistema',
                                'class' => 'container-fluid'
                    ]);
                }
            }
        }

        $procesos = \common\models\Proceso::find()
                ->where(['estado' => 1, 'publicar' => 1])
                ->all();
        $estados = \common\models\OrdenEstado::find()
                ->where(['estado' => 1])
                ->orderBy(['orden' => SORT_ASC])
                ->all();
        $count = \common\models\OrdenEstado::find()
                ->where(['estado' => 1])
                ->count();


        return $this->render('index', [
                    'procesos' => $procesos,
                    'estados' => $estados,
                    'count' => $count,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin() {
        $this->layout = "main-login";
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            $cookies = Yii::$app->response->cookies;

            $sucursal = \common\models\Sucursal::find()
                    ->innerJoinWith('empleadoSucursals')
                    ->innerJoinWith("empleadoSucursals.empleado")
                    ->where(['user_id' => Yii::$app->user->id, 'por_defecto' => 1])
                    ->one();

            $cookies->add(new \yii\web\Cookie([
                        'name' => 'sucursal',
                        'value' => $sucursal->id
            ]));

            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout() {
        Yii::$app->user->logout();
        $cookies = Yii::$app->response->cookies;
        $cookies->remove('sucursal');

        return $this->goHome();
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {
        try {
            $model = new \frontend\models\ResetPasswordForm($token);
        } catch (\Exception $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Se ha guardado la nueva clave.');

            return $this->redirect('index');
        }

        $this->layout = 'main-login';
        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

}
