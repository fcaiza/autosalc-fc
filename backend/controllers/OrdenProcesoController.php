<?php

namespace backend\controllers;

use common\models\Empleado;
use common\models\EmpleadoSucursal;
use common\models\Orden;
use common\models\OrdenTrabajo;
use Yii;
use common\models\OrdenProceso;
use common\models\search\OrdenProcesoSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\filters\AccessControl;

/**
 * OrdenProcesoController implements the CRUD actions for OrdenProceso model.
 */
class OrdenProcesoController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all OrdenProceso models.
     * @return mixed
     */
    public function actionIndex() {
        if (isset($_POST['expandRowKey'])) {
            $orden_id = $_POST['expandRowKey'];
            $searchModel = new OrdenProcesoSearch();
            $searchModel->orden_id = $orden_id;
            $searchModel->estado_id = 2;
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->renderPartial('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single OrdenProceso model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Resumen de la Orden #: " . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('view', [
                        'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Updates an existing OrdenProceso model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $lista = ArrayHelper::map(Empleado::empleadosXProcesoId($model->proceso_id), 'id', function ($data) {
                    return $data->getNombres();
                });

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Asignar Proceso",
                    'content' => $this->renderAjax('_asignacion', [
                        'model' => $model,
                        'lista' => $lista,
                        'error' => ''
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->validate() && $model->asignado_id) {
                $model->estado_id = 2;
                $model->save();
                $ordenTrabajo = new OrdenTrabajo();
                $ordenTrabajo->empleado_id = $model->asignado_id;
                $ordenTrabajo->orden_proceso_id = $model->id;
                $ordenTrabajo->estado_id = $model->estado_id;
                $ordenTrabajo->fecha_inicio = ($model->fecha_inicio) ? $model->fecha_inicio : date('Y-m-d');
                $ordenTrabajo->fecha_fin = $model->fecha_fin;
                if (!$ordenTrabajo->save()) {
                    print_r($ordenTrabajo->errors);
                    die();
                }
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'forceClose' => true
                ];
            } else {
                $error = (!$model->asignado_id) ? 'Escoja un empleado' : '';
                return [
                    'title' => "Asignar Proceso",
                    'content' => $this->renderAjax('_asignacion', [
                        'model' => $model,
                        'lista' => $lista,
                        'error' => $error
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_asignacion', [
                            'model' => $model,
                            'lista' => $lista,
                            'error' => ''
                ]);
            }
        }
    }

    /**
     * Finds the OrdenProceso model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrdenProceso the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = OrdenProceso::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
