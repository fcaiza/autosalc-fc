<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TipoDato */
?>
<div class="tipo-dato-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
