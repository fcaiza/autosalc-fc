<?php

namespace backend\controllers;

use Yii;
use common\models\Dato;
use common\models\User;
use common\models\search\DatoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\filters\AccessControl;
use frontend\models\SignupForm;

/**
 * DatoController implements the CRUD actions for Dato model.
 */
class DatoController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Dato models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        if (!file_exists(\Yii::getAlias('@backend') . '/web/img/users/'))
        {
            mkdir(\Yii::getAlias('@backend') . '/web/img/users/', 0777, true);
        }

        $searchModel = new DatoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'id' => $id
        ]);
    }

    /**
     * Displays a single Dato model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Resumen del registro #: " . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                Html::a('Editar', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        }
        else
        {
            return $this->render('view', [
                        'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Dato model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $request = Yii::$app->request;
        if ($id == 1)
        {
            $model = new Dato(['scenario' => 'cliente']);
        }
        elseif ($id == 2)
        {
            $model = new Dato(['scenario' => 'empleado']);
        }
        else
        {
            $model = new Dato(['scenario' => 'proveedor']);
        }
        $user = new SignupForm();
        $model->tipo_dato_id = $id;

        if ($request->isAjax)
        {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet)
            {
                return [
                    'title' => "Crear Nuevo",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'user' => $user,
                        'tipo_dato_id' => $id,
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
            else if ($model->load($request->post()) && $user->load($request->post()) && $new = $user->save())
            {

                $upload_image = \yii\web\UploadedFile::getInstance($model, 'upload_image');
                if (!empty($upload_image))
                {
                    $image_name = $new->id . '.jpg';
                    $model->foto = $image_name;
                }
                $model->user_id = $new->id;
                if ($model->save())
                {
                    if (!empty($upload_image))
                    {
                        $upload_image->saveAs('img/users/' . $image_name);
                    }
                }
                else
                {
                    return [
                        'title' => "Crear Nuevo",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'user' => $user,
                            'tipo_dato_id' => $id,
                        ]),
                        'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                        Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Crear Nuevo",
                    'content' => '<span class="text-success">Registro creado correctamente</span>',
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::a('Crear Otro', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            }
            else
            {
                return [
                    'title' => "Crear Nuevo",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'user' => $user,
                        'tipo_dato_id' => $id,
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        }
    }

    /**
     * Updates an existing Dato model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $user = $model->user;
        $tipo_dato_id = $model->tipo_dato_id;

        if ($request->isAjax)
        {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet)
            {
                return [
                    'title' => "Editar registro #: " . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'user' => $user,
                        'tipo_dato_id' => $tipo_dato_id,
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
            //else if ($model->load($request->post()) && $model->save())
            else if ($model->load($request->post()) && $user->load($request->post()) && $user->save())
            {
                $upload_image = \yii\web\UploadedFile::getInstance($model, 'upload_image');
                if (!empty($upload_image))
                {
                    $image_name = $user->id . '.jpg';
                    $model->foto = $image_name;
                }

                if ($model->save())
                {
                    if (!empty($upload_image))
                    {
                        $upload_image->saveAs('img/users/' . $image_name);
                    }
                }
                else
                {
                    return [
                        'title' => "Crear Nuevo",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'user' => $user,
                            'tipo_dato_id' => $tipo_dato_id,
                        ]),
                        'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                        Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Registro editado #: " . $id,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::a('Editar', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            }
            else
            {
                return [
                    'title' => "Editar registro #: " . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'user' => $user,
                        'tipo_dato_id' => $tipo_dato_id,
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        }
    }

    /**
     * Delete an existing Dato model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax)
        {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }
        else
        {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Dato model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dato the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dato::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionConsultar($id)
    {
        $nombres = "";

        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = Dato::findOne(['num_documento' => $id]);
        if ($model)
        {
            $vehiculos = "<option value=''>Seleccione</option>";
            if ($model->vehiculos)
            {
                foreach ($model->vehiculos as $vehiculo)
                {
                    $vehiculos .= "<option value='$vehiculo->id'>$vehiculo->placa</option>";
                }
            }
            return [
                'bandera' => true,
                'id' => $model->id,
                'nombres' => $model->fullName,
                'documento' => $model->num_documento,
                'email' => $model->user->email,
                'vehiculo' => $vehiculos
            ];
        }

        return [
            'bandera' => false,
            'id' => $id
        ];
    }

    public function actionNuevo()
    {
        $request = Yii::$app->request;

        $user = new SignupForm();
        $model = new Dato();
        $model->tipo_dato_id = 1;
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($request->isAjax)
        {
            if ($model->load($request->post()) && $user->load($request->post()) && $new = $user->save())
            {
                $upload_image = \yii\web\UploadedFile::getInstance($model, 'upload_image');
                if (!empty($upload_image))
                {
                    $image_name = $new->id . '.jpg';
                    $model->foto = $image_name;
                }

                $model->user_id = $new->id;
                $model->nombre = strtoupper($model->nombre);
                $model->apellido = strtoupper($model->apellido);

                if ($model->save())
                {
                    if (!empty($upload_image))
                    {
                        $upload_image->saveAs('img/users/' . $image_name);
                    }
                }
                return ['id' => $model->id, 'nombre' => $model->getfullName()];
            }
        }
    }

}
