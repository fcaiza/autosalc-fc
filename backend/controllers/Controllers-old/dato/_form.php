<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
//use backend\modules\transferencia\controllers\AdminSeleccionController;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Dato */
/* @var $form yii\widgets\ActiveForm */
$label = $model->getAttributeLabel('nombre');
if ($tipo_dato_id == 3)
{
    $label = "Razon Social";
}
?>

<div class="dato-form">

    <?php
    $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_HORIZONTAL,
        'id'=>'contact_form',
                'formConfig' => ['labelSpan' => 4, 'deviceSize' => ActiveForm::SIZE_LARGE]
    ]);
    ?>
    <div class="col-sm-6">


        <?php echo $form->field($model, 'nombre')->textInput(['maxlength' => true])->label($label) ?>

        <?php echo ($tipo_dato_id < 3) ? $form->field($model, 'apellido')->textInput(['maxlength' => true]) : ''; ?>


        <?php
        $lista = ArrayHelper::map(common\models\TipoDocumento::find()->all(), 'id', 'nombre');
        echo $form->field($model, 'tipo_documento_id')->widget(Select2::classname(), [
            'data' => $lista,
            'options' => ['placeholder' => 'Seleccione ...'],
            'pluginOptions' => ['allowClear' => true]
        ])
        ?>

        <?php echo $form->field($model, 'num_documento')->textInput(['maxlength' => true]) ?>

        <?php echo $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

        <?php echo $form->field($model, 'celular')->textInput(['maxlength' => true]) ?>

        <?php echo $form->field($model, 'direccion')->textarea(['row' => 3]) ?>

    </div>
    <div class="col-sm-6">
        <?php echo $form->field($user, 'username')->textInput(['maxlength' => true]) ?>

        <?php echo $form->field($user, 'email')->textInput(['maxlength' => true]) ?>

        <?php echo $form->field($user, 'password')->passwordInput(['maxlength' => true]) ?>

        <?php
        $imagenen = '';
        if (file_exists(Yii::getAlias('@web') . '/img/users/' . $model->foto))
        {
            $imagenen = Yii::getAlias('@web') . '/img/users/' . $model->foto;
        }
        echo $form->field($model, 'upload_image')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
            'pluginOptions' => [
                'showPreview' => true,
                'showCaption' => true,
                'showRemove' => true,
                'showUpload' => false,
                'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                'browseLabel' => 'Select Photo',
                'initialPreview' => $imagenen,
                'initialPreviewConfig' => $imagenen,
            ]
        ])->label('Logo');
        ?>

    </div>


    <?php ActiveForm::end(); ?>

</div>
