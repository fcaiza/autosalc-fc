<?php

use yii\helpers\Url;
use yii\helpers\Html;
use mdm\admin\components\Helper;
use kartik\grid\GridView;

$campos = [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'nombre',
        'label' => ($id == 3) ? 'Razon Social' : 'Nombres',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'apellido',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'user_id',
        'value' => 'user.username',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'tipo_documento_id',
        'value' => 'tipoDocumento.nombre',
        'filter' => yii\helpers\ArrayHelper::map(common\models\TipoDocumento::find()->all(), 'id', 'nombre'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'num_documento',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'telefono',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'celular',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'direccion',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'foto',
        'format' => 'raw',
        'hAlign' => 'center',
        'filter' => '',
        'value' => function($model) {
            if ($model->foto)
            {
                return Html::img(Yii::getAlias('@web') . '/img/users/' . $model->foto, ['width' => '50']);
            }
            return '';
        }
    ],
];

if ($id == 1)
{
    $campos[] = [
//        'label' => 'Vehiculos',
        'class' => 'kartik\grid\ExpandRowColumn',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detailUrl' => Url::to(['vehiculo/index']),
        'enableRowClick' => false,
        'detailRowCssClass' => GridView::TYPE_SUCCESS,
    ];
}
$campos[] = [
    'class' => 'kartik\grid\ActionColumn',
    'dropdown' => false,
    'width' => '100px',
    'vAlign' => 'middle',
    'template' => Helper::filterActionColumn('{view} {update} {delete} '),
    'buttons' => [
        'ejemplo' => function ($url, $data) {
            return Html::a('<span class="fa fa-asterisk"></span> ', $url, [
                        'title' => 'Titulo', 'data-toggle' => 'tooltip',
                        'role' => 'modal-remote',
            ]);
        },
    ],
    'urlCreator' => function($action, $model, $key, $index) {
        return Url::to([$action, 'id' => $key]);
    },
    'viewOptions' => ['role' => 'modal-remote', 'title' => 'Ver', 'data-toggle' => 'tooltip'],
    'updateOptions' => ['role' => 'modal-remote', 'title' => 'Editar', 'data-toggle' => 'tooltip'],
    'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Borrar',
        'data-confirm' => false, 'data-method' => false, // for overide yii data api
        'data-request-method' => 'post',
        'data-toggle' => 'tooltip',
        'data-confirm-title' => 'Esta usted seguro?',
        'data-confirm-message' => 'Seguro que quiere borrar este item'],
];



return $campos;
