<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Dato */
?>
<div class="dato-view">
    <div class="col-sm-6">
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'tipoDato.nombre',
                'tipoDocumento.nombre',
                'num_documento',
                'nombre',
                'apellido',
                'telefono',
                'celular',
                'direccion'
            ],
        ])
        ?>
    </div>
    <div class="col-sm-6">
        <?=
        DetailView::widget([
            'model' => $model->user,
            'attributes' => [
                'username',
                'email',
                'status',
                'created_at',
            ],
        ])
        ?>
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <a href="#" class="thumbnail">
                    <?php
                    echo ($model->foto) ? Html::img(Yii::getAlias('@web') . '/img/users/' . $model->foto, ['width' => '200']) : '';
                    ?>
                </a>
            </div>
        </div>
    </div>
</div>
