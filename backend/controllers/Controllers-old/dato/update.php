<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Dato */
?>
<div class="dato-update">

    <?=
    $this->render('_form', [
        'model' => $model,
        'user' => $user,
        'tipo_dato_id' => $tipo_dato_id,
    ])
    ?>

</div>
