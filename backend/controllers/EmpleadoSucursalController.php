<?php

namespace backend\controllers;

use common\models\EmpleadoSucursal;
use common\models\Orden;
use common\models\search\EmpleadoSucursalSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * EmpleadoSucursalController implements the CRUD actions for EmpleadoSucursal model.
 */
class EmpleadoSucursalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all EmpleadoSucursal models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (isset($_POST['expandRowKey'])) {
            $empleado_id = $_POST['expandRowKey'];
            $searchModel = new EmpleadoSucursalSearch();
            $searchModel->empleado_id = $empleado_id;
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->renderPartial('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'empleado_id' => $empleado_id
            ]);
        }
    }


    /**
     * Displays a single EmpleadoSucursal model.
     * @param integer $sucursal_id
     * @param integer $empleado_id
     * @param integer $proceso_id
     * @return mixed
     */
    public function actionView($sucursal_id, $empleado_id, $proceso_id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Resumen del registro #: " . $sucursal_id, $empleado_id, $proceso_id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($sucursal_id, $empleado_id, $proceso_id),
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::a('Editar', ['update', 'sucursal_id, $empleado_id, $proceso_id' => $sucursal_id, $empleado_id, $proceso_id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($sucursal_id, $empleado_id, $proceso_id),
            ]);
        }
    }

    /**
     * Creates a new EmpleadoSucursal model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($empleado_id)
    {
        $request = Yii::$app->request;
        $model = new EmpleadoSucursal();
        $model->empleado_id = $empleado_id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Crear Nuevo",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                        Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->validate()) {
                if ($model->por_defecto) {
                    self::updateEmpleadoDefaul($model->sucursal_id, $model->empleado_id);
                }
                $model->save();
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Crear Nuevo",
                    'content' => '<span class="text-success">Registro creado correctamente</span>',
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                        Html::a('Crear Otro', ['create', 'empleado_id' => $empleado_id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Crear Nuevo",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                        Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'sucursal_id' => $model->sucursal_id, 'empleado_id' => $model->empleado_id, 'proceso_id' => $model->proceso_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing EmpleadoSucursal model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $sucursal_id
     * @param integer $empleado_id
     * @param integer $proceso_id
     * @return mixed
     */
    public function actionUpdate($sucursal_id, $empleado_id, $proceso_id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($sucursal_id, $empleado_id, $proceso_id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Editar registro #: " . $sucursal_id, $empleado_id, $proceso_id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                        Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->validate()) {
                if ($model->por_defecto) {
                    self::updateEmpleadoDefaul($model->sucursal_id, $model->empleado_id);
                }
                $model->save();
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Registro editado #: " . $sucursal_id, $empleado_id, $proceso_id,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                        Html::a('Editar', ['update', 'sucursal_id, $empleado_id, $proceso_id' => $sucursal_id, $empleado_id, $proceso_id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Editar registro #: " . $sucursal_id, $empleado_id, $proceso_id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                        Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'sucursal_id' => $model->sucursal_id, 'empleado_id' => $model->empleado_id, 'proceso_id' => $model->proceso_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing EmpleadoSucursal model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $sucursal_id
     * @param integer $empleado_id
     * @param integer $proceso_id
     * @return mixed
     */
    public function actionDelete($sucursal_id, $empleado_id, $proceso_id)
    {
        $request = Yii::$app->request;
        $ordenes = [];
        $model = $this->findModel($sucursal_id, $empleado_id, $proceso_id);
        if ($model->por_defecto) {
            $ordenes = Orden::find()
                ->where(['empleado_recibe_id' => $empleado_id,])
                ->orWhere(['empleado_revisa_id' => $empleado_id])
                ->orWhere(['empleado_entrega_id' => $empleado_id])
                ->all();
        }
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($ordenes) {
                return [
                    'title' => "Error",
                    'content' => 'Error no se pudo borrar esta asignado en una orden',
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"])
                ];
            } else {
                $model->delete();
            }
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }
    }


    public function updateEmpleadoDefaul($sucursal_id, $empleado_id)
    {
        EmpleadoSucursal::updateAll(['por_defecto' => 0], ['sucursal_id' => $sucursal_id, 'empleado_id' => $empleado_id]);
    }

    /**
     * Finds the EmpleadoSucursal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $sucursal_id
     * @param integer $empleado_id
     * @param integer $proceso_id
     * @return EmpleadoSucursal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($sucursal_id, $empleado_id, $proceso_id)
    {
        if (($model = EmpleadoSucursal::findOne(['sucursal_id' => $sucursal_id, 'empleado_id' => $empleado_id, 'proceso_id' => $proceso_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
