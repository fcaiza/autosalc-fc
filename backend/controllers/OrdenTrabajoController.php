<?php

namespace backend\controllers;

use common\models\OrdenTrabajo;
use common\models\OrdenTrabajoImagen;
use common\models\search\OrdenTrabajoSearch;
use common\models\UploadImagen;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * OrdenTrabajoController implements the CRUD actions for OrdenTrabajo model.
 */
class OrdenTrabajoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all OrdenTrabajo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdenTrabajoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single OrdenTrabajo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Resumen del registro #: " . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::a('Editar', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new OrdenTrabajo model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new OrdenTrabajo();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Crear Nuevo",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                        Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Crear Nuevo",
                    'content' => '<span class="text-success">Registro creado correctamente</span>',
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                        Html::a('Crear Otro', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Crear Nuevo",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                        Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing OrdenTrabajo model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Editar registro #: " . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                        Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Registro editado #: " . $id,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                        Html::a('Editar', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Editar registro #: " . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                        Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing OrdenTrabajo model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Finds the OrdenTrabajo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrdenTrabajo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrdenTrabajo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

//    public function actionImagenOrden($id)
//    {
//        $request = Yii::$app->request;
//        Yii::$app->response->format = Response::FORMAT_JSON;
//        $path = 'uploads/orden-trabajo/'.$id;
//        $path_frotend = Yii::getAlias('@backend');
//        \yii\helpers\FileHelper::createDirectory($aliasDir = $path_frotend . "/web/" . $path, $mode = 0755, $recursive = true);
//        return [
//            'title' => 'Agregar Adjuntos',
//            'content' => $this->renderAjax('/upload/UploadOrdenTrabajo', [
//                'id' => $id,
//                'path' => $path,
//            ])
//        ];
//    }

    public function actionImagenOrden($id)
    {
        $request = Yii::$app->request;
        $path = 'uploads/orden-trabajo/' . $id;
        $path_frotend = Yii::getAlias('@backend');
        \yii\helpers\FileHelper::createDirectory($aliasDir = $path_frotend . "/web/" . $path, $mode = 0755, $recursive = true);
        $name = 'orden-' . $id . '-' . date('YmdHmss');
        $model = new OrdenTrabajoImagen();
        $model->orden_trabajo_id = $id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => 'Agregar Imagen de Avance',
                    'content' => $this->renderAjax('/upload/UploadOrdenTrabajo2', [
                        'id' => $id,
                        'path' => $path,
                        'model' => $model,
                        'name' => $name,
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                        Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) /*&& $model->validate()*/) {
                $imagen = $_POST['imagen'];
                if ($imagen == '') {
                    $model->imagen = NULL;
                } else {
                    $model->imagen = $imagen;
                }
                $web_path = $path_frotend . '/' . $path . '/' . $imagen;
                if ($model->validate()) {
                    $model->save();
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'forceClose' => true
                    ];
                } else {
                    return [
                        'title' => 'Agregar Imagen de Avance',
                        'content' => $this->renderAjax('/upload/UploadOrdenTrabajo2', [
                            'id' => $id,
                            'path' => $path,
                            'model' => $model,
                            'name' => $name,
                        ]),
                        'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                            Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index']);
            } else {
                return $this->render('/upload/UploadOrdenTrabajo2', [
                    'id' => $id,
                    'path' => $path,
                    'model' => $model,
                    'name' => $name,
                ]);
            }
        }
    }

    public function actionGuardarImg($id, $path)
    {
        $path_frotend = Yii::getAlias('@backend');
        $path = urldecode($path);
        $modelData = new UploadImagen();
        if (Yii::$app->request->isPost) {
            $fileTmpPath = $_FILES['UploadImagen']['tmp_name']['file'][0];
//            $fileName = $_FILES['UploadImagen']['name']['file'][0];
            $fileName = 'orden-' . $id . '-' . date('YmdHmss') . '.jpg';
            $fileSize = $_FILES['UploadImagen']['size']['file'][0];
            $fileType = $_FILES['UploadImagen']['type']['file'][0];
            $fileTemp = $_FILES['UploadImagen']['tmp_name']['file'][0];
            $fileNameCmps = explode(".", $fileName);
            $fileExtension = strtolower(end($fileNameCmps));
            $name = str_replace(' ', '_', $fileName);
            $dir = '';
            /*imagenes*/
            $allowedfileExtensionsImagen = array('png', 'jpg', 'jpeg');
            if (in_array($fileExtension, $allowedfileExtensionsImagen)) {
                $dir = $path_frotend . '/web/' . $path . '/' . $name;
            }
            if (move_uploaded_file($fileTemp, $dir)) {

            } else {
                Yii::$app->response->format = Yii::$app->response->format = Response::FORMAT_JSON;
                return ["error" => 'No se pudo subir el archivo'];
            }
            $path = $path_frotend . '/web/' . $path . '/' . $name;
        }
        $res = [
            'link' => ($path) ? $path : ''
        ];
        Yii::$app->response->format = Yii::$app->response->format = Response::FORMAT_JSON;
        return $res;
    }
}
