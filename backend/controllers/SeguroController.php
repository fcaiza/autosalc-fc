<?php

namespace backend\controllers;

use common\models\search\SeguroSearch;
use common\models\Seguro;
use common\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * SeguroController implements the CRUD actions for Seguro model.
 */
class SeguroController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Seguro models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SeguroSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Seguro model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $user = $model->user;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Resumen del registro #: " . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                    'user' => $user,
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::a('Editar', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $model,
                'user' => $user,
            ]);
        }
    }

    /**
     * Creates a new Seguro model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Seguro();
        $user = new User();
        $user->setPassword("123456789");
        $user->generateAuthKey();
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($request->isGet) {
            return [
                'title' => "Crear Nuevo",
                'content' => $this->renderAjax('create', [
                    'model' => $model,
                    'user' => $user
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])

            ];
        } else if ($model->load($request->post()) && $user->load($request->post()) && $user->validate()) {
            $transaction = Seguro::getDb()->beginTransaction();
            try {
                $user->setPassword("123456789");
                $user->generateAuthKey();
                $user->status = $model->estado;
                if (!$user->save()) {
                    return self::returnValidate($model, $user);
                }
                $model->user_id = $user->id;
                if (!$model->save()) {
                    return self::returnValidate($model, $user);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
            return [
                'forceReload' => '#crud-datatable-pjax',
                'title' => "Crear Nuevo",
                'content' => '<span class="text-success">Registro creado correctamente</span>',
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::a('Crear Otro', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

            ];
        } else {
            return self::returnValidate($model, $user);
        }

    }

    public function returnValidate($model, $user)
    {
        return [
            'title' => "Error en la Información",
            'content' => $this->renderAjax('create', [
                'model' => $model,
                'user' => $user,
            ]),
            'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])

        ];
    }

    /**
     * Updates an existing Seguro model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $user = $model->user;

        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($request->isGet) {
            return [
                'title' => "Editar registro #: " . $id,
                'content' => $this->renderAjax('update', [
                    'model' => $model,
                    'user' => $user
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
            ];
        } else if ($model->load($request->post()) && $user->load($request->post()) && $user->validate()) {
            $transaction = Seguro::getDb()->beginTransaction();
            try {
                $user->status = $model->estado;
                if (!$user->save()) {
                    return self::returnValidate($model, $user);
                }
                $imagen = $_POST['imagen'];
                if ($imagen == '') {
                    $model->imagen = NULL;
                } else {
                    $model->imagen = $imagen;
                }
                if (!$model->save()) {
                    return self::returnValidate($model, $user);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
            return [
                'forceReload' => '#crud-datatable-pjax',
                'title' => "Registro editado #: " . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                    'user' => $user
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::a('Editar', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return self::returnValidate($model, $user);
        }
    }

    /**
     * Delete an existing Seguro model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Finds the Seguro model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Seguro the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Seguro::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
