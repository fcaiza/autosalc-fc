<?php

namespace backend\controllers;

use common\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Response;


class GlobalController extends Controller
{

    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    //controla la accion
    public function beforeAction($action)
    {
        if ($action->id == 'upload' || $action->id == 'upload-file' || $action->id == 'upload-file1') {
            Yii::$app->request->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /* NUEVO UPLOAD GENERICO */

    public function actionUploadGenericoDocImg($id, $path, $tipo, $model, $name = "", $bandera = "", $atribbute = "")
    {
        $request = Yii::$app->request;
        /* complementa para referirse al patch (TIPO '' = frontend , 1 = backend) */
        Yii::$app->response->format = Response::FORMAT_JSON;
        $path_frotend = ($tipo ? Yii::getAlias('@backend') : Yii::getAlias('@frontend'));
        $model2 = urldecode($model);
        $className = new $model2;
        $objeto = $className::findOne($id);
        if ($bandera) {
            $path = $path . '/';
        } else {
            $path = $path . '/' . $objeto->servicio_id . '-' . $objeto->id . '/';
        }
        /* Se genera los directorios */
        \yii\helpers\FileHelper::createDirectory($aliasDir = $path_frotend . "/web/" . $path . '/img', $mode = 0755, $recursive = true);
        \yii\helpers\FileHelper::createDirectory($aliasDir = $path_frotend . "/web/" . $path . '/doc', $mode = 0755, $recursive = true);
        return [
            'title' => 'Agregar Adjuntos',
            'content' => $this->renderAjax('/upload/UploadGenericoDocImg.php', [
                'id' => $id,
                'tipo' => $tipo,
                'path' => $path,
                'model' => $model,
                'objeto' => $objeto
            ])
        ];
    }

    public function actionGuardarGenericoDocImg($id, $path, $tipo, $model, $img = 0, $name = "", $bandera = "", $atribbute = "")
    {
        $path_frotend = ($tipo ? Yii::getAlias('@backend') : Yii::getAlias('@frontend'));
        $path = urldecode($path);
        $modelData = new \common\models\UploadImagen();
        if (Yii::$app->request->isPost) {
            $modelData->file = \yii\web\UploadedFile::getInstance($modelData, 'file');
            if ($img) {
                $model2 = urldecode($model);
                $className = new $model2;
                $modelData->file = \yii\web\UploadedFile::getInstance($className, 'imagen');
                if ($name) {
                    $name = trim($name);
                    $name = str_replace(' ', '_', $name);
                } else {
                    $name = $modelData->file->baseName;
                    $name = str_replace(' ', '_', $name);
                }
                \yii\helpers\FileHelper::createDirectory($aliasDir = $path_frotend . "/web/" . $path, $mode = 0755, $recursive = true);
                $file = $path_frotend . '/web/' . $path . '/' . strtolower($name) . '.' . strtolower($modelData->file->extension);
                $modelData->file->saveAs($file);
                $path = $path_frotend . '/web/' . $path . '/img/' . $name . '.' . $modelData->file->extension;
            } else {
                $fileTmpPath = $_FILES['UploadImagen']['tmp_name']['file'][0];
                $fileName = $_FILES['UploadImagen']['name']['file'][0];
                $fileSize = $_FILES['UploadImagen']['size']['file'][0];
                $fileType = $_FILES['UploadImagen']['type']['file'][0];
                $fileTemp = $_FILES['UploadImagen']['tmp_name']['file'][0];
                $fileNameCmps = explode(".", $fileName);
                $fileExtension = strtolower(end($fileNameCmps));
                $name = str_replace(' ', '_', $fileName);
                $dir = '';
                /*imagenes*/
                $allowedfileExtensionsImagen = array('png', 'jpg', 'jpeg');
                if (in_array($fileExtension, $allowedfileExtensionsImagen)) {
                    $dir = $path_frotend . '/web/' . $path . 'img/' . $name;
                }
                /*Documentos*/
                $allowedfileExtensionsDocumentos = array('csv', 'xlsx', 'pdf', 'docx');
                if (in_array($fileExtension, $allowedfileExtensionsDocumentos)) {
                    $dir = $path_frotend . '/web/' . $path . 'doc/' . $name;
                }
                if (move_uploaded_file($fileTemp, $dir)) {

                } else {
                    Yii::$app->response->format = Yii::$app->response->format = Response::FORMAT_JSON;
                    return ["error" => 'No se pudo subir el archivo'];
                }
                $path = $path_frotend . '/web/' . $path . '/img/' . $name;
            }
        }
        $res = [
            'link' => ($path) ? $path : ''
        ];
        Yii::$app->response->format = Yii::$app->response->format = Response::FORMAT_JSON;
        return $res;
    }

    public static function ObtenerVersion($file, $path, $path_frotend)
    {
        $url = Yii::getAlias('@backend');
        $absoluteURL = Yii::$app->urlManager->createAbsoluteUrl($path);
        $directory = $url . '/web/' . $path . '/';
        $dirint = dir($directory);
        while (($archivo = $dirint->read()) !== false) {
            if ($archivo == "." || $archivo == ".." || $archivo == "img" || $archivo == "doc") {

            } else {
                $fileDelete = Yii::getAlias('@backend/web') . '/' . $path . '/' . $archivo;
                if (file_exists($fileDelete)) {
                    \yii\helpers\FileHelper::unlink($fileDelete);
                }
            }
        }
        $dirint->close();
        return true;
    }

    public static function ObtenerImagenModelo($path, $name)
    {
        $dataImage = [];
        $detalle = [];
        $imagenen = '';
        $url = Yii::getAlias('@backend');
        \yii\helpers\FileHelper::createDirectory($aliasDir = $url . "/web/" . $path, $mode = 0755, $recursive = true);
        $absoluteURL = Yii::$app->urlManager->createAbsoluteUrl($path);
        $directory = $url . '/web' . $path . '/';
        $dirint = dir($directory);
        while (($archivo = $dirint->read()) !== false) {
            if ($archivo == "." || $archivo == ".." || $archivo == "img" || $archivo == "doc") {

            } else {
                if ($name == $archivo) {
                    $imagenen = $archivo;
                }
            }
        }
        $dirint->close();
        $detalle = [
            "caption" => $imagenen,
            "downloadUrl" => ($imagenen ? $absoluteURL . '/' . $imagenen : ''),
            "key" => $imagenen,
            "type" => 'image',
        ];
        $dataImage = [($imagenen ? $absoluteURL . '/' . $imagenen : '')];

        $paso = [$dataImage, $detalle];
        return $paso;
    }

    public static function ObtenerGenericoDocImg($id, $path, $tipo, $model, $name = "")
    {
        $url = Yii::getAlias('@backend');
        $absoluteURL = Yii::$app->urlManager->createAbsoluteUrl($path);
        $directory = $url . '/web/' . $path . '/img/';
        $directory2 = $url . '/web/' . $path . '/doc/';
        $imagenes = [];
        $documentos = [];
        $dirint = dir($directory);
        while (($archivo = $dirint->read()) !== false) {
            if ($archivo == "." || $archivo == "..") {

            } else {
                $imagenes[] = [$archivo];
            }
        }
        $dirint->close();

        $dirint2 = dir($directory2);
        while (($archivo2 = $dirint2->read()) !== false) {
            if ($archivo2 == "." || $archivo2 == "..") {

            } else {
                $documentos[] = [$archivo2];
            }
        }
        $dirint2->close();
        $dataImage = [];
        $detalle = [];
        foreach ($imagenes as $item) {
            $detalle[] = [
                "caption" => $item[0],
                "downloadUrl" => $absoluteURL . '/img/' . $item[0],
                "key" => $item[0],
                "type" => 'image',
            ];
            $dataImage[] = [$absoluteURL . '/img/' . $item[0]];
        }
        foreach ($documentos as $item) {
            $tipos = explode(".", $item[0]);
            $type = '';
            if ($tipos[1] == 'xlsx' || $tipos[1] == 'docx') {
                $type = 'pdf';
            } else {
                $type = 'pdf';
            }
            $detalle[] = [
                "caption" => $item[0],
                "downloadUrl" => $absoluteURL . '/doc/' . $item[0],
                "key" => $item[0],
                "type" => $type,
            ];
            $dataImage[] = [$absoluteURL . '/doc/' . $item[0]];
        }
        $paso = [$dataImage, $detalle];
        return $paso;
    }

    public static function groupArray($arr, $group, $preserveGroupKey = false, $preserveSubArrays = false)
    {
        /*
          --- $campo = \backend\controllers\GestDocController::groupArray($out, "group");---
         */
        $temp = array();
        foreach ($arr as $key => $value) {
            $groupValue = $value[$group];
            if (!$preserveGroupKey) {
                unset($arr[$key][$group]);
            }
            if (!array_key_exists($groupValue, $temp)) {
                $temp[$groupValue] = array();
            }

            if (!$preserveSubArrays) {
                $data = count($arr[$key]) == 1 ? array_pop($arr[$key]) : $arr[$key];
            } else {
                $data = $arr[$key];
            }
            $temp[$groupValue][] = $data;
        }
        return $temp;
    }

    public function actionDeleteGenericoDocImg($id, $path, $tipo, $model, $img = 0, $name = "")
    {
        Yii::$app->response->format = Yii::$app->response->format = Response::FORMAT_JSON;
        $key = $_POST['key'];
        $url = ($tipo ? Yii::getAlias('@backend/web') : Yii::getAlias('@frontend/web'));
        $path = urldecode($path);
        $web_path = $url . '/' . $path . '/doc/' . $key;
        $web_path2 = $url . '/' . $path . '/img/' . $key;
        if (file_exists($web_path)) {
            \yii\helpers\FileHelper::unlink($web_path);
            return true;
        } else if (file_exists($web_path2)) {
            \yii\helpers\FileHelper::unlink($web_path2);
            return true;
        } else {
            return ["error" => 'no se pudo borrar el archivo ' . $key . ', no existe'];
        }
        return true;
    }

    public function actionDeleteImg($id, $path, $tipo, $model, $bandera = "", $atribbute = "")
    {
        Yii::$app->response->format = Yii::$app->response->format = Response::FORMAT_JSON;
        $key = $_POST['key'];
        $url = ($tipo ? Yii::getAlias('@backend/web') : Yii::getAlias('@frontend/web'));
        $model2 = urldecode($model);
        $className = new $model2;
        $objeto = $className::findOne($id);
        if ($atribbute) {
            $imgg = $objeto->$atribbute;
            $objeto->$atribbute = NUll;
        } else {
            $imgg = $objeto->imagen;
            $objeto->imagen = '';
        }
        if (!$objeto->save()) {
            return ["error" => 'revise la información'];
        } else {
            $path = urldecode($path);
            $path = $path . '/' . $objeto->id;
            $web_path = $url . '/' . $path . '/' . $imgg;
            if (file_exists($web_path)) {
                \yii\helpers\FileHelper::unlink($web_path);
                return true;
            } else {
                return ["error" => 'no se pudo borrar el archivo ' . $key . ', no existe'];
            }
        }
    }

    public static function ObtenerAdjuntosEmail()
    {
        \yii\helpers\FileHelper::createDirectory($aliasDir = Yii::getAlias('@backend') . "/web/uploads/adjuntos/doc", $mode = 0755, $recursive = true);
        $as = [];
        $directory = "uploads/adjuntos/doc";
        $dirint = dir($directory);
        while (($archivo = $dirint->read()) !== false) {
            if ($archivo == "." || $archivo == "..") {

            } else {
                $as[] = [$archivo];
            }
        }
        $dirint->close();
        $camposNuevos = [];
        foreach ($as as $item) {
            $peso = 'uploads/adjuntos/doc/' . $item[0];
            $bytes = filesize($peso);
            if ($bytes >= 1073741824) {
                $peso = number_format($bytes / 1073741824, 2) . ' GB';
            } elseif ($bytes >= 1048576) {
                $peso = number_format($bytes / 1048576, 2) . ' MB';
            } elseif ($bytes >= 1024) {
                $peso = number_format($bytes / 1024, 2) . ' KB';
            } elseif ($bytes > 1) {
                $peso = $bytes . ' bytes';
            } elseif ($bytes == 1) {
                $peso = '1 byte';
            } else {
                $peso = '0 bytes';
            }
            $camposNuevos[] = [
                "nombre" => $item[0],
                "link" => Yii::$app->urlManager->createAbsoluteUrl('uploads/adjuntos/doc/') . '/' . $item[0],
                "peso" => $peso
            ];
        }
        return $camposNuevos;
    }

    public static function download_file($archivo, $downloadfilename = null)
    {

        if (file_exists($archivo)) {
            $downloadfilename = $downloadfilename !== null ? $downloadfilename : basename($archivo);
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . $downloadfilename);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($archivo));

            ob_clean();
            flush();
            readfile($archivo);
            exit;
        }

    }

    public function actionFileFroala()
    {
        $web_path = Yii::getAlias('@web');
        \yii\helpers\FileHelper::createDirectory($aliasDir = Yii::getAlias('@backend') . "/web/uploads/adjuntos", $mode = 0755, $recursive = true);
        $as = [];
        $directory = "uploads/adjuntos";
        $dirint = dir($directory);
        while (($archivo = $dirint->read()) !== false) {
            if ($archivo == "." || $archivo == "..") {

            } else {
                $as[] = [$archivo];
            }
        }
        $dirint->close();
        $dataImage = [];
        foreach ($as as $item) {
            $dataDetail = [];
            $dataDetail['url'] = $path = Yii::$app->urlManager->createAbsoluteUrl('uploads/adjuntos/') . '/' . $item[0];
            $dataImage [] = $dataDetail;
        }
        $res = $dataImage;
        Yii::$app->response->format = Yii::$app->response->format = Response::FORMAT_JSON;

        return $res;
    }

    public function actionUpload()
    {
        $base_path = Yii::getAlias('@app');
        $web_path = Yii::getAlias('@web');
        \yii\helpers\FileHelper::createDirectory($aliasDir = Yii::getAlias('@backend') . "/web/uploads/adjuntos", $mode = 0755, $recursive = true);
        $model = new \common\models\UploadImagen();

        if (Yii::$app->request->isPost) {
            $model->file = \yii\web\UploadedFile::getInstanceByName('file');
            if ($model->validate()) {
                $model->file->saveAs($base_path . '/web/uploads/adjuntos/' . $model->file->baseName . '.' . $model->file->extension);
            }
        }
        $path = Yii::$app->urlManager->createAbsoluteUrl('uploads/adjuntos/') . '/' . $model->file->baseName . '.' . $model->file->extension;
        $res = [
            'link' => $path
        ];
        Yii::$app->response->format = Yii::$app->response->format = Response::FORMAT_JSON;
        return $res;
    }

    public function actionUploadFile1()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $base_path = Yii::getAlias('@app');
        $web_path = Yii::getAlias('@web');
        \yii\helpers\FileHelper::createDirectory($aliasDir = Yii::getAlias('@backend') . "/web/uploads/adjuntos/doc", $mode = 0755, $recursive = true);
        $model = new \common\models\UploadImagen();

        if (Yii::$app->request->isPost) {
            $model->file = \yii\web\UploadedFile::getInstanceByName('file');
            if ($model->validate()) {
                $nombreArchivo = strtolower($model->file->baseName);
                $nombreArchivo = $this->EliminarAcentos($nombreArchivo);
                $model->file->saveAs($base_path . '/web/uploads/adjuntos/doc/' . $nombreArchivo . '.' . $model->file->extension);
            }
        }
        $path = Yii::$app->urlManager->createAbsoluteUrl('uploads/adjuntos/doc') . '/' . $nombreArchivo . '.' . $model->file->extension;
        $res = [
            'link' => $path
        ];
        Yii::$app->response->format = Yii::$app->response->format = Response::FORMAT_JSON;
        return $res;
    }

    public static function ordenModel($modelName, $id, $atrib)
    {
        $model2 = urldecode($modelName);
        $className = new $model2;
        $model = $className::findOne($id);
        if ($model->$atrib) {
            $order = $model->$atrib + 1;
            $menus = $className::find()
                ->where(['<>', 'id', $model->id])
                ->andWhere(['>=', "$atrib", $model->$atrib])
                ->all();
            if ($menus) {
                foreach ($menus as $item) {
                    $item->$atrib = $order;
                    $item->save();
                    $order++;
                }
            }
        } else {
            $order = $className::find()->count();
            $model->$atrib = $order;
            $model->save();

        }
    }

    public function actionResetEmail($id)
    {
        $request = Yii::$app->request;

        $model = new \frontend\models\PasswordResetRequestForm();
        $user = User::findOne($id);

        $model->email = $user->email;

        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($model->validate()) {

            if ($model->sendEmail()) {
                return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
            } else {
                return [
                    'title' => "Editar registro #: " . $id,
                    'content' => 'No se pudo resetear el token para la clave',
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }
        }

    }

    public function actionReset($id)
    {
        $request = Yii::$app->request;
        $model = User::findOne($id);
        $model->password_hash = '';
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($request->isGet) {
            return [
                'title' => "Registrar Nueva Clave",
                'content' => $this->renderAjax('/site/_clave_update', ['model' => $model]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
            ];
        } else if ($model->load($request->post())) {
            $model->setPassword($model->password_hash);
            $model->save();
            return [
                'title' => "Registrar Nueva Clave",
                'content' => 'Se Actualizo correctamente',
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return [
                'title' => "Registrar Nueva Clave",
                'content' => $this->renderAjax('/site/_clave_update', ['model' => $model]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
            ];
        }

    }


}
