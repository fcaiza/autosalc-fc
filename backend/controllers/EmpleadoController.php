<?php

namespace backend\controllers;

use common\models\Broker;
use common\models\Cliente;
use common\models\Empleado;
use common\models\search\EmpleadoSearch;
use common\models\Seguro;
use common\models\Sucursal;
use common\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * EmpleadoController implements the CRUD actions for Empleado model.
 */
class EmpleadoController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Empleado models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmpleadoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Empleado model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Resumen del registro #: " . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                Html::a('Editar', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        }
        else
        {
            return $this->render('view', [
                        'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Empleado model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Empleado();
        $user = new User();
        $user->setPassword("123456789");
        $user->generateAuthKey();
        /*
         *   Process for ajax request
         */
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($request->isGet)
        {
            return [
                'title' => "Crear Nuevo",
                'content' => $this->renderAjax('create', [
                    'model' => $model,
                    'user' => $user,
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
            ];
        }
        else if ($model->load($request->post()) && $user->load($request->post()) && $user->validate())
        {
            $transaction = Empleado::getDb()->beginTransaction();
            try
            {

                $user->status = $model->estado;
                if (!$user->save())
                {
                    return self::returnValidate($model, $user);
                }
                $model->user_id = $user->id;
                if (!$model->save())
                {
                    return self::returnValidate($model, $user);
                }
                $transaction->commit();
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Crear Nuevo",
                    'content' => '<span class="text-success">Registro creado correctamente</span>',
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::a('Crear Otro', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } catch (\Exception $e)
            {
                $transaction->rollBack();
                throw $e;
            }
        }
        else
        {
            return self::returnValidate($model, $user);
        }
    }

    public function returnValidate($model, $user)
    {
        return [
            'title' => "Error en la Información",
            'content' => $this->renderAjax('create', [
                'model' => $model,
                'user' => $user,
            ]),
            'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
            Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
        ];
    }

    /**
     * Updates an existing Empleado model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $user = $model->user;
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($request->isGet)
        {
            return [
                'title' => "Editar registro #: " . $id,
                'content' => $this->renderAjax('update', [
                    'model' => $model,
                    'user' => $user
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
            ];
        }
        else if ($model->load($request->post()) && $user->load($request->post()) && $user->validate())
        {
            $transaction = Empleado::getDb()->beginTransaction();
            try
            {
                $user->status = $model->estado;
                if (!$user->save())
                {
                    return self::returnValidate($model, $user);
                }
                if (!$model->save())
                {
                    return self::returnValidate($model, $user);
                }
                $transaction->commit();
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Registro editado #: " . $id,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                        'user' => $user
                    ]),
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::a('Editar', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } catch (\Exception $e)
            {
                $transaction->rollBack();
                throw $e;
            }
        }
        else
        {
            return [
                'title' => "Editar registro #: " . $id,
                'content' => $this->renderAjax('update', [
                    'model' => $model,
                    'user' => $user
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
            ];
        }
    }

    /**
     * Delete an existing Empleado model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
    }

    public function usuarioExistentes()
    {
        $usuarios = [];
        $broker = Broker::find()->all();
        $seguro = Seguro::find()->all();
        $empleado = Empleado::find()->all();
        $cliente = Cliente::find()->all();
        foreach ($broker as $userBroker):
            array_push($usuarios, $userBroker->user_id);
        endforeach;
        foreach ($seguro as $userSeguro):
            array_push($usuarios, $userSeguro->user_id);
        endforeach;
        foreach ($empleado as $userEmpleado):
            array_push($usuarios, $userEmpleado->user_id);
        endforeach;
        foreach ($cliente as $userCliente):
            array_push($usuarios, $userCliente->user_id);
        endforeach;

        return $usuarios;
    }

    public function actionAsignaSucursal()
    {
        $request = Yii::$app->request;
        $model = new Empleado();
        $users = self::usuarioExistentes();

        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($request->isGet)
        {
            return [
                'title' => "Crear Nuevo",
                'content' => $this->renderAjax('_form_asigna', [
                    'model' => $model,
                    'users' => $users
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
            ];
        }
        else if ($model->load($request->post()))
        {
            $transaction = Empleado::getDb()->beginTransaction();
            try
            {
                if (!$model->save())
                {
                    return self::returnValidateAsigna($model, $users);
                }
                $transaction->commit();
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Crear Nuevo",
                    'content' => '<span class="text-success">Registro creado correctamente</span>',
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::a('Crear Otro', ['asigna-sucursal'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } catch (\Exception $e)
            {
                $transaction->rollBack();
                throw $e;
            }
        }
        else
        {
            return self::returnValidateAsigna($model, $users);
        }
    }

    public function returnValidateAsigna($model, $users)
    {
        return [
            'title' => "Error en la Información",
            'content' => $this->renderAjax('_form_asigna', [
                'model' => $model,
                'users' => $users
            ]),
            'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
            Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
        ];
    }

    /**
     * Finds the Empleado model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Empleado the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Empleado::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSucursales()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        $campo = [];
        if (isset($_POST['depdrop_parents']))
        {
            $id = $_POST['depdrop_parents'];
            $list = Sucursal::find()->andWhere(['NOT IN', 'id', $id])->all();
            foreach ($list as $account)
            {
                $out[] = ['group' => $account->empresa->nombre, 'id' => $account->id, 'name' => $account->nombre];
            }
            $campo = GlobalController::groupArray($out, "group");
        }
        return ['output' => $campo, 'selected' => ''];
    }

}
