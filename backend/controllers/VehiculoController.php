<?php

namespace backend\controllers;

use common\models\Modelo;
use common\models\search\VehiculoSearch;
use common\models\Vehiculo;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * VehiculoController implements the CRUD actions for Vehiculo model.
 */
class VehiculoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Vehiculo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VehiculoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Vehiculo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Resumen del registro #: " . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Vehiculo model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($cliente_id = '', $bandera = '')
    {
        $request = Yii::$app->request;
        $model = new Vehiculo();
        if ($cliente_id) {
            $model->cliente_id = $cliente_id;
        }

        /*
        *   Process for ajax request
        */
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($request->isGet) {
            return [
                'title' => "Crear Nuevo",
                'content' => $this->renderAjax('create', [
                    'model' => $model,
                    'cliente_id' => $cliente_id
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])

            ];
        } else if ($model->load($request->post()) && $model->save()) {
            if ($bandera) {
                return [
                    'content' => "<script> $(function () {  reloadContent($cliente_id); $('#ajaxCrudModal .close').click() }); </script>",
                ];
            } else {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Crear Nuevo",
                    'content' => '<span class="text-success">Registro creado correctamente</span>',
                    'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                        Html::a('Crear Otro', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            }
        } else {
            return [
                'title' => "Crear Nuevo",
                'content' => $this->renderAjax('create', [
                    'model' => $model,
                    'cliente_id' => $cliente_id
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])

            ];
        }

    }

    /**
     * Updates an existing Vehiculo model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $bandera = '')
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        /*
        *   Process for ajax request
        */
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($request->isGet) {
            return [
                'title' => "Editar registro #: " . $id,
                'content' => $this->renderAjax('update', [
                    'model' => $model,
                    'cliente_id' => $bandera
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
            ];
        } else if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload' => '#crud-datatable-pjax',
                'title' => "Registro editado #: " . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                    'cliente_id' => $bandera
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::a('Editar', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return [
                'title' => "Editar registro #: " . $id,
                'content' => $this->renderAjax('update', [
                    'model' => $model,
                    'cliente_id' => $bandera
                ]),
                'footer' => Html::button('Cerrar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                    Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
            ];
        }
    }

    /**
     * Delete an existing Vehiculo model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Finds the Vehiculo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vehiculo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vehiculo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionModelos()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        $campo = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = $_POST['depdrop_parents'];
            $list = Modelo::find()->where(['estado' => 1, 'marca_id' => $id])->all();
            foreach ($list as $account) {
                $out[] = ['group' => $account->marca->nombre, 'id' => $account->id, 'name' => $account->nombre];
            }
            $campo = GlobalController::groupArray($out, "group");
        }
        return ['output' => $campo, 'selected' => ''];
    }
}
