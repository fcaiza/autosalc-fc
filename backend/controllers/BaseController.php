<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;

/**
 * Description of DataBaseController
 *
 * @author Luis Caiza
 */
class BaseController extends Controller {

    public static function armarMenu()
    {
        $matriz = \mdm\admin\components\MenuHelper::getAssignedMenu(Yii::$app->user->id);
        $menu = self::recorro($matriz);
        return $matriz;
    }

    public static function recorro(&$matriz, $bandera = true)
    {
        foreach ($matriz as $key => $value)
        {
            $menuActivo = \common\models\Menu::findOne(['name' => $value['label'], 'estado' => 1]);
            if (!$menuActivo)
            {
                unset($matriz[$key]);
            }
            else
            {
                if (isset($value['label']))
                {
                    $menu = \common\models\Menu::findOne(['name' => $value['label']]);
                    if ($menu)
                    {
                        if ($bandera)
                        {
                            $matriz[$key]['icon'] = ($menu->icon) ? $menu->icon : '';
                        }
                        $matriz[$key]['color'] = ($menu->option) ? $menu->option : '';
                        if (isset($matriz[$key]['url']))
                        {
                            if ($menu->data)
                            {
                                $params = [$menu->route];
                                $dataAll = explode('&', $menu->data);

                                foreach ($dataAll as $data)
                                {
                                    $data = explode('=', $data);
                                    if ($data)
                                    {
                                        $params[$data[0]] = $data[1];
                                    }
                                }

                                $matriz[$key]['url'] = $params;
                            }
                        }
                    }
                    if (isset($value['items']))
                    {
                        $matriz[$key]['items'] = self::recorro($value['items'], false);
                    }
                }
            }
        }
        return $matriz;
    }

    public static function eliminaAcentos($text)
    {
        $text = htmlentities($text, ENT_QUOTES, 'UTF-8');
        $text = strtolower($text);
        $patron = array(
            // Espacios, puntos y comas por guion
            //'/[\., ]+/' => ' ',
            // Vocales
            '/\+/' => '',
            '/&agrave;/' => 'a',
            '/&egrave;/' => 'e',
            '/&igrave;/' => 'i',
            '/&ograve;/' => 'o',
            '/&ugrave;/' => 'u',
            '/&aacute;/' => 'a',
            '/&eacute;/' => 'e',
            '/&iacute;/' => 'i',
            '/&oacute;/' => 'o',
            '/&uacute;/' => 'u',
            '/&acirc;/' => 'a',
            '/&ecirc;/' => 'e',
            '/&icirc;/' => 'i',
            '/&ocirc;/' => 'o',
            '/&ucirc;/' => 'u',
            '/&atilde;/' => 'a',
            '/&etilde;/' => 'e',
            '/&itilde;/' => 'i',
            '/&otilde;/' => 'o',
            '/&utilde;/' => 'u',
            '/&auml;/' => 'a',
            '/&euml;/' => 'e',
            '/&iuml;/' => 'i',
            '/&ouml;/' => 'o',
            '/&uuml;/' => 'u',
            '/&auml;/' => 'a',
            '/&euml;/' => 'e',
            '/&iuml;/' => 'i',
            '/&ouml;/' => 'o',
            '/&uuml;/' => 'u',
            // Otras letras y caracteres especiales
            '/&aring;/' => 'a',
            '/&ntilde;/' => 'n',
                // Agregar aqui mas caracteres si es necesario
        );

        $text = preg_replace(array_keys($patron), array_values($patron), $text);
        return $text;
    }

}
