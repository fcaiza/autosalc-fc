<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=cdiabeji_autosalc',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.kleintours.com.ec',
                'username' => 'sistema18@kleintours.com.ec',
                'password' => 'S1st3m@5_KtG0',
                'port' => '587',
            ],
            'useFileTransport' => false,
        ],  
    ],
];
