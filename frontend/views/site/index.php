<?php
/* @var $this yii\web\View */

$this->title = 'Autos ALC';
?>

<section id="intro">

    <div class="intro-content">
        <h2><span>Autos ALC</span><br>tenemos la magia!</h2>
        <div>
            <a href="#about" class="btn-get-started scrollto">Iniciar</a> 
        </div>
    </div>
    <div id="intro-carousel" class="owl-carousel" >
        <div class="item" style="background-image: url('img/intro-carousel/car1.jpg');"></div>  
        <div class="item" style="background-image: url('img/intro-carousel/car2.jpg');"></div>  
        <div class="item" style="background-image: url('img/intro-carousel/2.jpg');"></div>  
    </div>
</section><

<main id="main"> 

    <section id="services">
        <div class="container">
            <div class="section-header">
                <h2>Nuestros Servicios</h2>
                <p>Para cumplir con todas las exigencias de nuestos clientes nos hemos permitido incorporar todos los servicios necesarios para brindar la mejora atencion en un solo lugar, es por eso que nostros <b>tenemos la magia!</b>.</p>
            </div>

            <div class="row">

                <?php
                if ($procesos):
                    foreach ($procesos as $proceso):
                        $icono = "fa-car-alt";
                        if ($proceso->icono):
                            $icono = "fa-" . $proceso->icono;
                        endif;
                        ?>
                        <div class="col-lg-4">
                            <div class="box wow fadeInLeft">
                                <div class="icon"><i class="fas <?= $icono ?>"></i></div>
                                <h4 class="title"><a href=""><?= $proceso->nombre ?></a></h4>
                                <p class="description"><?= str_replace('</p>', '', str_replace('<p>', '', $proceso->detalle)) ?></p>
                            </div>
                        </div>
                        <?php
                    endforeach;
                endif;
                ?>
            </div>

        </div>
    </section>


    <!--==========================
      About Section
    ============================-->
<!--    <section id="about" class="wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <h2>Nosotros</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident, doloribus omnis minus ovident, doloribus omnis minus temporibus perferendis nesciunt..</p>
            </div>
            <div class="row">
                <div class="col-lg-6 about-img">
                    <img src="img/about-img.png" alt="">
                </div>

                <div class="col-lg-6 content">
                    <h2>Lorem ipsum dolor sit amet, consectetur adipiscing</h2>
                    <h3>Dolores quae porro consequatur aliquam, incidunt eius magni provident, doloribus omnis minus ovident</h3>
                    <p>Consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt fugiat. Dolores quae porro consequatur aliquam, incidunt fugiat culpa esse aute nulla. malis nulla duis fugiat</p>
                    <ul>
                        <li><i class="icon ion-ios-checkmark-outline"></i> Dolores quae porro consequatur aliquam, incidunt fugiat culpa.</li>
                        <li><i class="icon ion-ios-checkmark-outline"></i> Dolores quae porro consequatur aliquam, culpa esse aute nulla.</li>
                        <li><i class="icon ion-ios-checkmark-outline"></i> Dolores quae porro esse aute nulla. malis nulla duis fugiat</li>
                    </ul> 
                </div>
            </div>

        </div>
    </section>-->

    <!--==========================
      Our Portfolio Section
    ============================-->
<!--    <section id="portfolio" class="wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <h2>Galleria</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt fugiat culpa esse aute nulla. nulla duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore</p>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row no-gutters">

                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="img/portfolio/1.jpg" class="portfolio-popup">
                            <img src="img/portfolio/1.jpg" alt="">
                            <div class="portfolio-overlay">
                                <div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Name</h2></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="img/portfolio/2.jpg" class="portfolio-popup">
                            <img src="img/portfolio/2.jpg" alt="">
                            <div class="portfolio-overlay">
                                <div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Name</h2></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="img/portfolio/3.jpg" class="portfolio-popup">
                            <img src="img/portfolio/3.jpg" alt="">
                            <div class="portfolio-overlay">
                                <div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Name</h2></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="img/portfolio/4.jpg" class="portfolio-popup">
                            <img src="img/portfolio/4.jpg" alt="">
                            <div class="portfolio-overlay">
                                <div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Name</h2></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="img/portfolio/5.jpg" class="portfolio-popup">
                            <img src="img/portfolio/5.jpg" alt="">
                            <div class="portfolio-overlay">
                                <div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Name</h2></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="img/portfolio/6.jpg" class="portfolio-popup">
                            <img src="img/portfolio/6.jpg" alt="">
                            <div class="portfolio-overlay">
                                <div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Name </h2></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="img/portfolio/7.jpg" class="portfolio-popup">
                            <img src="img/portfolio/7.jpg" alt="">
                            <div class="portfolio-overlay">
                                <div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Name </h2></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="img/portfolio/8.jpg" class="portfolio-popup">
                            <img src="img/portfolio/8.jpg" alt="">
                            <div class="portfolio-overlay">
                                <div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Name</h2></div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>

        </div>
    </section> -->

    <!--==========================
      Testimonials Section
    ============================-->
<!--    <section id="testimonials" class="wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <h2>Testimonios</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt fugiat culpa esse aute nulla. duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore</p>
            </div>
            <div class="owl-carousel testimonials-carousel">

                <div class="testimonial-item">
                    <p>Dolores quae porro consequatur aliquam, incidunt fugiat culpa esse aute nulla. duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore

                    </p> 
                    <h3>Mrio James</h3>
                    <h4>CEO &amp; Founder</h4>
                </div>

                <div class="testimonial-item">
                    <p>
                        Dolores quae porro consequatur aliquam, incidunt fugiat culpa esse aute nulla. duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore

                    </p> 
                    <h3>Finton Gofnes</h3>
                    <h4>CTO</h4>
                </div>

                <div class="testimonial-item">
                    <p>
                        Dolores quae porro consequatur aliquam, incidunt fugiat culpa esse aute nulla. duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore

                    </p> 
                    <h3>Marcus Kell</h3>
                    <h4>Marketing</h4>
                </div>

                <div class="testimonial-item">
                    <p>
                        Dolores quae porro consequatur aliquam, incidunt fugiat culpa esse aute nulla. duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore

                    </p> 
                    <h3>Williams Belly</h3>
                    <h4>Accounts</h4>
                </div>

                <div class="testimonial-item">
                    <p>
                        Dolores quae porro consequatur aliquam, incidunt fugiat culpa esse aute nulla. duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore

                    </p> 
                    <h3>Larry Hanson</h3>
                    <h4>Investor</h4>
                </div>

            </div>

        </div>
    </section> -->

    <!--==========================
      Call To Action Section
    ============================-->
<!--    <section id="call-to-action" class="wow fadeInUp">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 text-center text-lg-left">
                    <h3 class="cta-title">Contrata Nuestros Servicios</h3>
                    <p class="cta-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt fugiat culpa esse aute nulla cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="col-lg-3 cta-btn-container text-center">
                    <a class="cta-btn align-middle" href="#contact">Contact Us</a>
                </div>
            </div>

        </div>
    </section>-->
    <!--==========================
         Price Section
       ============================-->
<!--            <section id="price" class="wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <h2>Price</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt fugiat culpa esse aute nulla. duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore</p>
            </div>
            <div class="pricing card-deck flex-column flex-md-row mb-3">
                <div class="card card-pricing text-center px-3 mb-4">
                    <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Starter</span>
                    <div class="bg-transparent card-header pt-4 border-0">
                        <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="15">$<span class="price">3</span><span class="h6 text-muted ml-2">/ per month</span></h1>
                    </div>
                    <div class="card-body pt-0">
                        <ul class="list-unstyled mb-4">
                            <li>Up to 5 users</li>
                            <li>Basic support on Github</li>
                            <li>Monthly updates</li>
                            <li>Free cancelation</li>
                        </ul>
                        <button type="button" class="btn btn-outline-secondary mb-3">Order now</button>
                    </div>
                </div>
                <div class="card card-pricing popular shadow text-center px-3 mb-4">
                    <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Professional</span>
                    <div class="bg-transparent card-header pt-4 border-0">
                        <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="30">$<span class="price">6</span><span class="h6 text-muted ml-2">/ per month</span></h1>
                    </div>
                    <div class="card-body pt-0">
                        <ul class="list-unstyled mb-4">
                            <li>Up to 5 users</li>
                            <li>Basic support on Github</li>
                            <li>Monthly updates</li>
                            <li>Free cancelation</li>
                        </ul>
                        <a href="" target="_blank" class="btn btn-primary mb-3">Order Now</a>
                    </div>
                </div>
                <div class="card card-pricing text-center px-3 mb-4">
                    <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Business</span>
                    <div class="bg-transparent card-header pt-4 border-0">
                        <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="45">$<span class="price">9</span><span class="h6 text-muted ml-2">/ per month</span></h1>
                    </div>
                    <div class="card-body pt-0">
                        <ul class="list-unstyled mb-4">
                            <li>Up to 5 users</li>
                            <li>Basic support on Github</li>
                            <li>Monthly updates</li>
                            <li>Free cancelation</li>
                        </ul>
                        <button type="button" class="btn btn-outline-secondary mb-3">Order now</button>
                    </div>
                </div>
                <div class="card card-pricing text-center px-3 mb-4">
                    <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Enterprise</span>
                    <div class="bg-transparent card-header pt-4 border-0">
                        <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="60">$<span class="price">12</span><span class="h6 text-muted ml-2">/ per month</span></h1>
                    </div>
                    <div class="card-body pt-0">
                        <ul class="list-unstyled mb-4">
                            <li>Up to 5 users</li>
                            <li>Basic support on Github</li>
                            <li>Monthly updates</li>
                            <li>Free cancelation</li>
                        </ul>
                        <button type="button" class="btn btn-outline-secondary mb-3">Order now</button>
                    </div>
                </div>
            </div>

        </div>
    </section> -->
    <!--==========================
      Clients Section
    ============================-->
<!--            <section id="clients" class="wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <h2>Clients</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt fugiat culpa esse aute nulla. duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore</p>
            </div>

            <div class="owl-carousel clients-carousel">
                <img src="img/clients/client-1.png" alt="">
                <img src="img/clients/client-2.png" alt="">
                <img src="img/clients/client-3.png" alt="">
                <img src="img/clients/client-4.png" alt="">
                <img src="img/clients/client-5.png" alt="">
                <img src="img/clients/client-6.png" alt="">
            </div>

        </div>
    </section> -->
    <!--==========================
      Our Team Section
    ============================-->
<!--            <section id="team" class="wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <h2>Our Team</h2>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="member">
                        <div class="pic"><img src="img/team1.jpg" alt=""></div>
                        <div class="details">
                            <h4>James Smith</h4>
                            <span>Chief Executive Officer</span>
                            <div class="social">
                                <a href=""><i class="fa fa-twitter"></i></a>
                                <a href=""><i class="fa fa-facebook"></i></a>
                                <a href=""><i class="fa fa-google-plus"></i></a>
                                <a href=""><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="member">
                        <div class="pic"><img src="img/team2.jpg" alt=""></div>
                        <div class="details">
                            <h4>Michell Kellon</h4>
                            <span>Technical Manager</span>
                            <div class="social">
                                <a href=""><i class="fa fa-twitter"></i></a>
                                <a href=""><i class="fa fa-facebook"></i></a>
                                <a href=""><i class="fa fa-google-plus"></i></a>
                                <a href=""><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="member">
                        <div class="pic"><img src="img/team3.jpg" alt=""></div>
                        <div class="details">
                            <h4>French Lincon</h4>
                            <span>Financial Manager</span>
                            <div class="social">
                                <a href=""><i class="fa fa-twitter"></i></a>
                                <a href=""><i class="fa fa-facebook"></i></a>
                                <a href=""><i class="fa fa-google-plus"></i></a>
                                <a href=""><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="member">
                        <div class="pic"><img src="img/team4.jpg" alt=""></div>
                        <div class="details">
                            <h4>Amanda Jepson</h4>
                            <span>Accountant</span>
                            <div class="social">
                                <a href=""><i class="fa fa-twitter"></i></a>
                                <a href=""><i class="fa fa-facebook"></i></a>
                                <a href=""><i class="fa fa-google-plus"></i></a>
                                <a href=""><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section> -->

    <!--==========================
      Contact Section
    ============================-->
<!--    <section id="contact" class="wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <h2>Contact Us</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt fugiat culpa esse aute nulla. malis nulla duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore</p>
            </div>

            <div class="row contact-info">
                <div class="col-lg-5"> 
                    <div class="contact-address">
                        <i class="ion-ios-location-outline"></i>
                        <h3>Direccion</h3>
                        <address>Capitan Ramón Borja OE2-229 y, Quito 170138</address>
                    </div> 
                    <div class="contact-phone">
                        <i class="ion-ios-telephone-outline"></i>
                        <h3>Tel&eacute;fono</h3>
                        <p><a href="tel:+59322410787">(02) 241-0787</a></p>
                    </div> 
                    <div class="contact-email">
                        <i class="ion-ios-email-outline"></i>
                        <h3>Correo</h3>
                        <p><a href="mailto:info@example.com">name@website.com</a></p>
                    </div> 
                </div>
                <div class="col-lg-7">
                    <div class="container">
                        <div class="form"> 
                            <form name="sentMessage" class="well" id="contactForm"> 
                                <div class="control-group">
                                    <div class="form-group">
                                        <input type="text" class="form-control" 
                                               placeholder="Nombres" id="name" required />
                                    </div>
                                </div> 	
                                <div class="form-group">
                                    <div class="controls">
                                        <input type="email" class="form-control" placeholder="Correo" 
                                               id="email" required />
                                    </div>
                                </div> 	

                                <div class="form-group">
                                    <div class="controls">
                                        <textarea rows="5" cols="100" class="form-control" 
                                                  placeholder="Message" id="message" required
                                                  maxlength="999" style="resize:none"></textarea>
                                    </div>
                                </div> 		 
                                <div id="success"> </div> 
                                <button type="submit" class="btn btn-primary pull-right">Send</button><br />
                            </form>
                        </div>

                    </div>
                </div>


            </div>
        </div>

        <div class="container mb-4 map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d997.4515187492146!2d-78.485085!3d-0.1419537!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x91d5855221abe557%3A0x6a5bd138ed8bbe25!2sAUTOS+ALC+S.A.!5e0!3m2!1ses!2sec!4v1561082512433!5m2!1ses!2sec" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div
    </section> -->

</main>