<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>

<section id="contact" class="wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <h2>Contactanos</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt fugiat culpa esse aute nulla. malis nulla duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore</p>
            </div>

            <div class="row contact-info">
                <div class="col-lg-5"> 
                    <div class="contact-address">
                        <i class="ion-ios-location-outline"></i>
                        <h3>Direccion</h3>
                        <address>Capitan Ramón Borja OE2-229 y, Quito 170138</address>
                    </div> 
                    <div class="contact-phone">
                        <i class="ion-ios-telephone-outline"></i>
                        <h3>Tel&eacute;fono</h3>
                        <p><a href="tel:+59322410787">(02) 241-0787</a></p>
                    </div> 
                    <div class="contact-email">
                        <i class="ion-ios-email-outline"></i>
                        <h3>Correo</h3>
                        <p><a href="mailto:info@example.com">name@website.com</a></p>
                    </div> 
                </div>
                <div class="col-lg-7">
                    <div class="container">
                        <div class="form"> 
                            <form name="sentMessage" class="well" id="contactForm"> 
                                <div class="control-group">
                                    <div class="form-group">
                                        <input type="text" class="form-control" 
                                               placeholder="Nombres" id="name" required />
                                    </div>
                                </div> 	
                                <div class="form-group">
                                    <div class="controls">
                                        <input type="email" class="form-control" placeholder="Correo" 
                                               id="email" required />
                                    </div>
                                </div> 	

                                <div class="form-group">
                                    <div class="controls">
                                        <textarea rows="5" cols="100" class="form-control" 
                                                  placeholder="Message" id="message" required
                                                  maxlength="999" style="resize:none"></textarea>
                                    </div>
                                </div> 		 
                                <div id="success"> </div> 
                                <button type="submit" class="btn btn-primary pull-right">Send</button><br />
                            </form>
                        </div>

                    </div>
                </div>


            </div>
        </div>

        <div class="container mb-4 map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d997.4515187492146!2d-78.485085!3d-0.1419537!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x91d5855221abe557%3A0x6a5bd138ed8bbe25!2sAUTOS+ALC+S.A.!5e0!3m2!1ses!2sec!4v1561082512433!5m2!1ses!2sec" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div
    </section>


