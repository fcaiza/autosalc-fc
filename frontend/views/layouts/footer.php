
<footer id="footer">
    <div class="container">
        <div class="copyright">
            &copy; Copyright <strong>autos ALC</strong>. All Rights Reserved
        </div>
        <div class="credits"> 
            Dise&ntilde;ado por <a href="https://aurysolutions.com/">AurySolutions</a>
        </div>
    </div>
</footer>