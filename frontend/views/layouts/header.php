<section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
        <div class="contact-info float-left">
            <i class="fa fa-envelope-o"></i> <a href="mailto:name@website.com">name@website.com</a>
            <i class="fa fa-phone"></i> (593) 2 2410-788 / (593) 2 2410-787 / (593) 2 2402-992
        </div>
        <div class="social-links float-right">
            <!--<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>-->
            <a href="https://www.facebook.com/AUTOS-ALC-SA-933323183402352/" class="facebook" target="_blank"><i class="fa fa-facebook"></i></a>
            <!--<a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>-->
            <!--<a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>-->
            <!--<a href="#" class="instagram"><i class="fa fa-instagram"></i></a>-->
        </div>
    </div>
</section>


<header id="header">
    <div class="container">

        <div id="logo" class="pull-left">
            <h1>
                <i class="fas fa-car-alt" style="margin-top: 10px"></i>
                <a href="#body"><img src="<?= Yii::getAlias('@web')?>/img/autosalc/logo-blanco.png" alt="" title="" width="170px" class="img-responsive" /></a>
            </h1> 
        </div>

        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li class="menu-active"><a href="#body">Inicio</a></li>
                <li><a href="#about">Nosotros</a></li>
                <li><a href="#services">Servicios</a></li>
                <li><a href="#portfolio">Galeria</a></li>
                <!--<li><a href="#price">Precios</a></li>--> 
                <!--<li><a href="#team">Nuestro Equipo</a></li>--> 
                <li><a href="#contact">Contactenos</a></li>
            </ul>
        </nav>
    </div>
</header>