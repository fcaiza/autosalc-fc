<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
//use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;

//use common\widgets\Alert;
//
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <!-- Favicons -->
        <link href="img/favicon.png" rel="icon">
        <!--<link href="img/apple-touch-icon.png" rel="apple-touch-icon">-->
        <link rel="shortcut icon" type="image/ico" href="<?= Yii::getAlias('@web') ?>/img/autosalc/logo-final-b.png">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

        <?php $this->head() ?>
    </head>
    <body id="body">
        <?php $this->beginBody() ?>
        
        <?php echo $this->render('header.php'); ?>

        <?= $content ?>

        <?php echo $this->render('footer.php'); ?>

        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
