<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
//use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;

//use common\widgets\Alert;
//
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        <!-- Favicons -->
        <link href="img/favicon.png" rel="icon">
        <!--<link href="img/apple-touch-icon.png" rel="apple-touch-icon">-->
        <link rel="shortcut icon" type="image/ico" href="<?= Yii::getAlias('@web') ?>/img/autosalc/logo-final-b.png">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

        <?php $this->head() ?>
    </head>
    <body id="body">
        <?php $this->beginBody() ?>
        <?php echo $this->render('header.php'); ?>


        <?php
        $procesos = \common\models\Proceso::find()
                ->where(['estado' => 1])
                ->orderBy(['orden' => SORT_ASC])
                ->all();
        echo $this->render('/site/index', ['procesos' => $procesos]);
        ?>

        <?php
        echo $this->render('/site/about');
        ?>

        <?php
        $model = new frontend\models\ContactForm();
        echo $this->render('/site/contact', ['model' => $model]);
        ?>

        <!--<div class="wrap">-->
        <?php
//    NavBar::begin([
//        'brandLabel' => Yii::$app->name,
//        'brandUrl' => Yii::$app->homeUrl,
//        'options' => [
//            'class' => 'navbar-inverse navbar-fixed-top',
//        ],
//    ]);
//    $menuItems = [
//        ['label' => 'Home', 'url' => ['/site/index']],
//        ['label' => 'About', 'url' => ['/site/about']],
//        ['label' => 'Contact', 'url' => ['/site/contact']],
//    ];
//    if (Yii::$app->user->isGuest) {
////        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
//        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
//    } else {
//        $menuItems[] = '<li>'
//            . Html::beginForm(['/site/logout'], 'post')
//            . Html::submitButton(
//                'Logout (' . Yii::$app->user->identity->username . ')',
//                ['class' => 'btn btn-link logout']
//            )
//            . Html::endForm()
//            . '</li>';
//    }
//    echo Nav::widget([
//        'options' => ['class' => 'navbar-nav navbar-right'],
//        'items' => $menuItems,
//    ]);
//    NavBar::end();
        ?>

        <!--<div class="container">-->
        <?php
//        echo Breadcrumbs::widget([
//            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
//        ]); 
        ?>
        <?php //= Alert::widget() ?>
        <?php //= $content ?>
        <!--</div>-->
        <!--</div>-->

        <!--<footer class="footer">-->
        <!--    <div class="container">
                <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>
        
                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>-->
        <!--</footer>-->
        <?php echo $this->render('footer.php'); ?>

        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
