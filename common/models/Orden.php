<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "orden".
 *
 * @property int $id
 * @property int $broker_id broker
 * @property int $seguro_id aseguradora
 * @property int $cliente_id cliente
 * @property int $vehiculo_id vehiculo cliente
 * @property int $empleado_recibe_id usuario recibe
 * @property string $fecha_recibe fecha de ingreso
 * @property string $fecha_salida fecha tentativa de salida
 * @property int $empleado_revisa_id
 * @property string $fecha_revisa_id
 * @property int $empleado_entrega_id usuario entrega
 * @property string $fecha_entrega fecha de entrega real
 * @property string $observacion_ingreso observacion al recibir
 * @property string $observacion_entrega observacion al entregar
 * @property int $avance
 * @property int $estado_id estado de la orden
 * @property int $numero_servicio total de numeros de servicios
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Broker $broker
 * @property Vehiculo $vehiculo
 * @property Cliente $cliente
 * @property Empleado $empleadoEntrega
 * @property Empleado $empleadoRecibe
 * @property Empleado $empleadoRevisa
 * @property OrdenEstado $estado
 * @property Seguro $seguro
 * @property OrdenProceso[] $ordenProcesos
 */
class Orden extends \yii\db\ActiveRecord {

    public function behaviors() {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'orden';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['broker_id', 'seguro_id', 'cliente_id', 'vehiculo_id', 'empleado_recibe_id', 'empleado_revisa_id', 'empleado_entrega_id', 'avance', 'estado_id', 'numero_servicio', 'created_at', 'updated_at'], 'integer'],
            [['cliente_id', 'vehiculo_id', 'fecha_recibe', 'fecha_salida', 'estado_id'], 'required'],
            [['fecha_recibe', 'fecha_entrega', 'fecha_revisa_id', 'fecha_entrega'], 'safe'],
            [['observacion_ingreso', 'observacion_entrega'], 'string'],
            [['broker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Broker::className(), 'targetAttribute' => ['broker_id' => 'id']],
            [['cliente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['cliente_id' => 'id']],
            [['empleado_entrega_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empleado::className(), 'targetAttribute' => ['empleado_entrega_id' => 'id']],
            [['empleado_recibe_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empleado::className(), 'targetAttribute' => ['empleado_recibe_id' => 'id']],
            [['empleado_revisa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empleado::className(), 'targetAttribute' => ['empleado_revisa_id' => 'id']],
            [['estado_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenEstado::className(), 'targetAttribute' => ['estado_id' => 'id']],
            [['seguro_id'], 'exist', 'skipOnError' => true, 'targetClass' => Seguro::className(), 'targetAttribute' => ['seguro_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'broker_id' => 'Broker',
            'seguro_id' => 'Seguro',
            'cliente_id' => 'Cliente',
            'vehiculo_id' => 'Vehiculo',
            'empleado_recibe_id' => 'Empleado Recibe',
            'fecha_recibe' => 'Fecha Recibe',
            'fecha_salida' => 'Fecha Salida',
            'empleado_revisa_id' => 'Empleado Revisa',
            'fecha_revisa_id' => 'Fecha Revisa',
            'empleado_entrega_id' => 'Empleado Entrega',
            'fecha_entrega' => 'Fecha Entrega',
            'observacion_ingreso' => 'Observacion Ingreso',
            'observacion_entrega' => 'Observacion Entrega',
            'avance' => 'Avance',
            'estado_id' => 'Estado',
            'numero_servicio' => 'Numero Servicio',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBroker() {
        return $this->hasOne(Broker::className(), ['id' => 'broker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculo() {
        return $this->hasOne(Vehiculo::className(), ['id' => 'vehiculo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCliente() {
        return $this->hasOne(Cliente::className(), ['id' => 'cliente_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleadoEntrega() {
        return $this->hasOne(Empleado::className(), ['id' => 'empleado_entrega_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleadoRecibe() {
        return $this->hasOne(Empleado::className(), ['id' => 'empleado_recibe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleadoRevisa() {
        return $this->hasOne(Empleado::className(), ['id' => 'empleado_revisa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstado() {
        return $this->hasOne(OrdenEstado::className(), ['id' => 'estado_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeguro() {
        return $this->hasOne(Seguro::className(), ['id' => 'seguro_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenProcesos() {
        return $this->hasMany(OrdenProceso::className(), ['orden_id' => 'id']);
    }

    public static function generarNumero($bandera = 0) {
        $sucursal = self::obtenerSucursal();

        $generador = Sucursal::findOne($sucursal);
        $indice = $generador->secuencial + 1;
        if ($bandera) {
            $generador->secuencial = $indice;
            $generador->save();
        }

        $secuencial = str_pad($indice, 10, "0", STR_PAD_LEFT);

        return $secuencial;
    }

    public static function obtenerSucursal() {
        $sucursal = 1;
        $cookies = \Yii::$app->request->cookies;
        if ($cookies->has('sucursal')) {
            if (($cookie = $cookies->get('sucursal')) !== null) {
                $sucursal = $cookie->value;
            }
        }
        return $sucursal;
    }

    public static function contarOrdenes($estadoIn = NULL) {
        return Orden::find()
                ->filterWhere(['IN', 'estado_id', $estadoIn])
                        ->count();
    }

}
