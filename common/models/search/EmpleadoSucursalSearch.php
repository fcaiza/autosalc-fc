<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\EmpleadoSucursal;

/**
 * EmpleadoSucursalSearch represents the model behind the search form about `common\models\EmpleadoSucursal`.
 */
class EmpleadoSucursalSearch extends EmpleadoSucursal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sucursal_id', 'empleado_id', 'proceso_id'], 'integer'],
            [['por_defecto', 'encargado_proceso'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmpleadoSucursal::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'sucursal_id' => $this->sucursal_id,
            'empleado_id' => $this->empleado_id,
            'por_defecto' => $this->por_defecto,
            'proceso_id' => $this->proceso_id,
            'encargado_proceso' => $this->encargado_proceso,
        ]);

        return $dataProvider;
    }
}
