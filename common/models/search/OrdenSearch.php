<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orden;

/**
 * OrdenSearch represents the model behind the search form about `common\models\Orden`.
 */
class OrdenSearch extends Orden
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'broker_id', 'seguro_id',  'empleado_recibe_id', 'empleado_revisa_id', 'empleado_entrega_id', 'avance', 'estado_id', 'numero_servicio', 'created_at', 'updated_at', 'numero_orden'], 'integer'],
            [['cliente_id', 'vehiculo_id'], 'string'],
            [['fecha_recibe', 'fecha_salida', 'fecha_revisa', 'fecha_entrega', 'observacion_ingreso', 'observacion_entrega'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $estado = 0, $usuarios = "", $proceso = "")
    {
        $query = Orden::find()
                ->innerJoinWith("cliente")                
                ->innerJoinWith("vehiculo")
                ->where("estado_id IN ($estado)");
        
        if($usuarios){
            $query->andWhere(['IN', 'empleado_recibe_id', $usuarios]);
        }
        
        if($proceso){
            echo "Filtrar proceso";
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'broker_id' => $this->broker_id,
            'seguro_id' => $this->seguro_id,
//            'cliente_id' => $this->cliente_id,
//            'vehiculo_id' => $this->vehiculo_id,
            'empleado_recibe_id' => $this->empleado_recibe_id,
            'fecha_recibe' => $this->fecha_recibe,
            'fecha_salida' => $this->fecha_salida,
            'empleado_revisa_id' => $this->empleado_revisa_id,
            'fecha_revisa' => $this->fecha_revisa,
            'empleado_entrega_id' => $this->empleado_entrega_id,
            'fecha_entrega' => $this->fecha_entrega,
            'avance' => $this->avance,
            'estado_id' => $this->estado_id,
            'numero_servicio' => $this->numero_servicio,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'observacion_ingreso', $this->observacion_ingreso])
            ->andFilterWhere(['like', 'vehiculo.placa', $this->vehiculo_id])
            ->andFilterWhere(['like', 'numero_orden', $this->numero_orden])
            ->andFilterWhere(['like', 'cliente.numero_documento', $this->cliente_id])
            ->andFilterWhere(['like', 'observacion_entrega', $this->observacion_entrega]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchAsignacion($params, $usuarios = "")
    {
        $query = Orden::find()
                ->innerJoinWith("ordenProcesos")
                ->innerJoinWith("cliente")                
                ->innerJoinWith("vehiculo")
                ->where("orden_proceso.estado_id IN (2)");
        
        if($usuarios){
            $query->andWhere(['IN', 'responsable_id', $usuarios]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'broker_id' => $this->broker_id,
            'seguro_id' => $this->seguro_id,
            'empleado_revisa_id' => $this->empleado_revisa_id,
            'fecha_revisa' => $this->fecha_revisa,
            'empleado_entrega_id' => $this->empleado_entrega_id,
            'estado_id' => $this->estado_id,
        ]);

        $query->andFilterWhere(['like', 'observacion_ingreso', $this->observacion_ingreso])
            ->andFilterWhere(['like', 'vehiculo.placa', $this->vehiculo_id])
            ->andFilterWhere(['like', 'numero_orden', $this->numero_orden])
            ->andFilterWhere(['like', 'cliente.numero_documento', $this->cliente_id])
            ->andFilterWhere(['like', 'observacion_entrega', $this->observacion_entrega]);

        return $dataProvider;
    }
}
