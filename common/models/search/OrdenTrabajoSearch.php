<?php

namespace common\models\search;

use common\models\Empleado;
use common\models\EmpleadoSucursal;
use common\models\Orden;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrdenTrabajo;

/**
 * OrdenTrabajoSearch represents the model behind the search form about `common\models\OrdenTrabajo`.
 */
class OrdenTrabajoSearch extends OrdenTrabajo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'orden_proceso_id', 'empleado_id', 'estado_id', 'created_at', 'updated_at'], 'integer'],
            [['fecha_inicio', 'fecha_fin', 'trabajo', 'resultado'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
//        $empleado = Empleado::obtenerEmpleadoSession();
//        $sucursal = Orden::obtenerSucursal();
//        $empleadoSucursal = EmpleadoSucursal::find()
//            ->filterWhere(['empleado_sucursal.empleado_id' => $empleado->id, 'empleado_sucursal.sucursal_id' => $sucursal, 'empleado_sucursal.encargado_proceso' => 0])
//            ->one();

        $query = OrdenTrabajo::find();
//            ->innerJoinWith('ordenProceso')
//            ->where(['orden_trabajo.estado_id' => 2, 'orden_trabajo.empleado_id' => $empleado->id])
//            ->orWhere(['orden_trabajo.estado_id' => 2, 'orden_proceso.proceso_id' => $empleadoSucursal->proceso_id, 'orden_trabajo.empleado_id' => NULL]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'orden_proceso_id' => $this->orden_proceso_id,
            'empleado_id' => $this->empleado_id,
            'estado_id' => $this->estado_id,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_fin' => $this->fecha_fin,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'trabajo', $this->trabajo])
            ->andFilterWhere(['like', 'resultado', $this->resultado]);

        return $dataProvider;
    }
}
