<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CatalogoGeneral;

/**
 * CatalogoGeneralSearch represents the model behind the search form about `common\models\CatalogoGeneral`.
 */
class CatalogoGeneralSearch extends CatalogoGeneral
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'orden', 'value', 'catalogo_id'], 'integer'],
            [['nombre', 'valor_1', 'valor_2', 'valor_3'], 'safe'],
            [['estado'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CatalogoGeneral::find()
                ->orderBy(['id' => SORT_ASC,'catalogo_id' => SORT_DESC, 'orden' => SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'orden' => $this->orden,
            'value' => $this->value,
            'catalogo_id' => $this->catalogo_id,
            'estado' => $this->estado,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'valor_1', $this->valor_1])
            ->andFilterWhere(['like', 'valor_2', $this->valor_2])
            ->andFilterWhere(['like', 'valor_3', $this->valor_3]);

        return $dataProvider;
    }
}
