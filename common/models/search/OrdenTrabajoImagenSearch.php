<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrdenTrabajoImagen;

/**
 * OrdenTrabajoImagenSearch represents the model behind the search form about `common\models\OrdenTrabajoImagen`.
 */
class OrdenTrabajoImagenSearch extends OrdenTrabajoImagen
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'orden_trabajo_id'], 'integer'],
            [['imagen'], 'safe'],
            [['envio_email', 'estado'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrdenTrabajoImagen::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'orden_trabajo_id' => $this->orden_trabajo_id,
            'envio_email' => $this->envio_email,
            'estado' => $this->estado,
        ]);

        $query->andFilterWhere(['like', 'imagen', $this->imagen]);

        return $dataProvider;
    }
}
