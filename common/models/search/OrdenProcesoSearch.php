<?php

namespace common\models\search;

use common\models\Empleado;
use common\models\EmpleadoSucursal;
use common\models\Orden;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrdenProceso;

/**
 * OrdenProcesoSearch represents the model behind the search form about `common\models\OrdenProceso`.
 */
class OrdenProcesoSearch extends OrdenProceso
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'orden_id', 'proceso_id', 'estado_id', 'fase', 'responsable_id', 'asignado_id', 'notificacion', 'created_at', 'updated_at'], 'integer'],
            [['fecha_inicio', 'observacion', 'fecha_fin', 'resultado'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
//        $sucursal = Orden::obtenerSucursal();
//        $empleado = Empleado::obtenerEmpleadoSession();
//        $usuarioSucursal = EmpleadoSucursal::findOne(['sucursal_id'=>$sucursal,'empleado_id'=>(($empleado) ? $empleado->id : NULL),'encargado_proceso'=>1]);
//        $proceso_id = ($usuarioSucursal) ? $usuarioSucursal->proceso_id : '';
        $query = OrdenProceso::find();
//            ->innerJoinWith('orden')
//            ->where(['orden.sucursal_id'=>$sucursal,'orden_proceso.proceso_id'=>$proceso_id])->andWhere(['IN','orden_proceso.estado_id','(1,2)']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'orden_id' => $this->orden_id,
            'proceso_id' => $this->proceso_id,
            'estado_id' => $this->estado_id,
            'fase' => $this->fase,
            'responsable_id' => $this->responsable_id,
            'asignado_id' => $this->asignado_id,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_fin' => $this->fecha_fin,
            'notificacion' => $this->notificacion,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'observacion', $this->observacion])
            ->andFilterWhere(['like', 'resultado', $this->resultado]);

        return $dataProvider;
    }
}
