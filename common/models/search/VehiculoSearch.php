<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Vehiculo;

/**
 * VehiculoSearch represents the model behind the search form about `common\models\Vehiculo`.
 */
class VehiculoSearch extends Vehiculo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cliente_id', 'marca_id', 'modelo_id', 'pais_id', 'clase_id', 'servicio_id', 'color_1_id', 'color_2_id', 'anio', 'cilindraje', 'created_at', 'updated_at'], 'integer'],
            [['placa', 'ramv_cpn', 'desde', 'hasta', 'informacion'], 'safe'],
            [['matriculado'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id='')
    {
        $query = Vehiculo::find();

        if($id){
            $query->where(['cliente_id'=>$id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'cliente_id' => $this->cliente_id,
            'marca_id' => $this->marca_id,
            'modelo_id' => $this->modelo_id,
            'pais_id' => $this->pais_id,
            'clase_id' => $this->clase_id,
            'servicio_id' => $this->servicio_id,
            'color_1_id' => $this->color_1_id,
            'color_2_id' => $this->color_2_id,
            'anio' => $this->anio,
            'cilindraje' => $this->cilindraje,
            'matriculado' => $this->matriculado,
            'desde' => $this->desde,
            'hasta' => $this->hasta,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'placa', $this->placa])
            ->andFilterWhere(['like', 'ramv_cpn', $this->ramv_cpn])
            ->andFilterWhere(['like', 'informacion', $this->informacion]);

        return $dataProvider;
    }
}
