<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrdenNotificacion;

/**
 * OrdenNotificacionSearch represents the model behind the search form about `common\models\OrdenNotificacion`.
 */
class OrdenNotificacionSearch extends OrdenNotificacion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'orden_proceso_id'], 'integer'],
            [['fecha_envio', 'titulo', 'mensaje', 'observacion', 'adjunto'], 'safe'],
            [['cliente', 'entidad'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrdenNotificacion::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'orden_proceso_id' => $this->orden_proceso_id,
            'fecha_envio' => $this->fecha_envio,
            'cliente' => $this->cliente,
            'entidad' => $this->entidad,
        ]);

        $query->andFilterWhere(['like', 'titulo', $this->titulo])
            ->andFilterWhere(['like', 'mensaje', $this->mensaje])
            ->andFilterWhere(['like', 'observacion', $this->observacion])
            ->andFilterWhere(['like', 'adjunto', $this->adjunto]);

        return $dataProvider;
    }
}
