<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "cliente".
 *
 * @property int $id
 * @property int $user_id
 * @property string $nombre
 * @property string $apellido
 * @property int $tipo_documento_id
 * @property string $numero_documento
 * @property string $direccion
 * @property string $telefono
 * @property string $celular
 * @property int $estado
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TipoDocumento $tipoDocumento
 * @property User $user
 * @property Orden[] $ordens
 * @property Vehiculo[] $vehiculos
 */
class Cliente extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cliente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'nombre', 'apellido', 'estado','tipo_documento_id','numero_documento'], 'required'],
            [['user_id', 'tipo_documento_id', 'created_at', 'updated_at'], 'integer'],
            [['nombre', 'apellido', 'direccion'], 'string', 'max' => 250],
            [['numero_documento', 'telefono', 'celular'], 'string', 'max' => 20],
            [['tipo_documento_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoDocumento::className(), 'targetAttribute' => ['tipo_documento_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'tipo_documento_id' => 'Tipo Doc',
            'numero_documento' => '# Documento',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'celular' => 'Celular',
            'estado' => 'Estado',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoDocumento()
    {
        return $this->hasOne(TipoDocumento::className(), ['id' => 'tipo_documento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdens()
    {
        return $this->hasMany(Orden::className(), ['cliente_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculos()
    {
        return $this->hasMany(Vehiculo::className(), ['cliente_id' => 'id']);
    }

    public function getNombres(){
        return $this->nombre . ' ' . $this->apellido;
    }
}
