<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_gerarquia".
 *
 * @property int $user_jefe_id
 * @property int $user_encargado_id
 *
 * @property User $userJefe
 * @property User $userEncargado
 */
class UserGerarquia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_gerarquia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_jefe_id', 'user_encargado_id'], 'required'],
            [['user_jefe_id', 'user_encargado_id'], 'integer'],
            [['user_jefe_id', 'user_encargado_id'], 'unique', 'targetAttribute' => ['user_jefe_id', 'user_encargado_id']],
            [['user_jefe_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_jefe_id' => 'id']],
            [['user_encargado_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_encargado_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_jefe_id' => 'User Jefe ID',
            'user_encargado_id' => 'User Encargado ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserJefe()
    {
        return $this->hasOne(User::className(), ['id' => 'user_jefe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserEncargado()
    {
        return $this->hasOne(User::className(), ['id' => 'user_encargado_id']);
    }
    
    public static function obtenerEmpleadoUserIdGerarquia(){
        return UserGerarquia::find()
                ->alias('ug')
                ->select('e.id as empleado_recibe_id')
                ->innerJoin('user u', 'ug.user_encargado_id=u.id')
                ->innerJoin('empleado e', 'e.user_id=u.id')
                ->where(['user_jefe_id' => Yii::$app->user->id])
                ->asArray()
                ->all();
    }
}
