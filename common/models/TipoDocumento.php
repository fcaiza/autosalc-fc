<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "tipo_documento".
 *
 * @property int $id
 * @property string $nombre
 * @property string $codigo
 * @property int $estado
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Cliente[] $clientes
 */
class TipoDocumento extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_documento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'codigo','estado'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['nombre'], 'string', 'max' => 250],
            [['codigo'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'codigo' => 'Codigo',
            'estado' => 'Estado',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientes()
    {
        return $this->hasMany(Cliente::className(), ['tipo_documento_id' => 'id']);
    }
}
