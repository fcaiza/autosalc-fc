<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "empleado_sucursal".
 *
 * @property int $sucursal_id
 * @property int $empleado_id
 * @property int $por_defecto
 * @property int $proceso_id
 * @property int $encargado_proceso
 *
 * @property Sucursal $sucursal
 * @property Empleado $empleado
 * @property Proceso $proceso
 */
class EmpleadoSucursal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleado_sucursal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sucursal_id', 'empleado_id', 'proceso_id'], 'required'],
            [['sucursal_id', 'empleado_id', 'proceso_id'], 'integer'],
            [['por_defecto', 'encargado_proceso'], 'integer'],
            [['sucursal_id', 'empleado_id', 'proceso_id'], 'unique', 'targetAttribute' => ['sucursal_id', 'empleado_id', 'proceso_id']],
            [['sucursal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sucursal::className(), 'targetAttribute' => ['sucursal_id' => 'id']],
            [['empleado_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empleado::className(), 'targetAttribute' => ['empleado_id' => 'id']],
            [['proceso_id'], 'exist', 'skipOnError' => true, 'targetClass' => Proceso::className(), 'targetAttribute' => ['proceso_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sucursal_id' => 'Sucursal',
            'empleado_id' => 'Empleado',
            'por_defecto' => 'Por Defecto',
            'proceso_id' => 'Proceso',
            'encargado_proceso' => 'Encargado Proceso',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSucursal()
    {
        return $this->hasOne(Sucursal::className(), ['id' => 'sucursal_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleado()
    {
        return $this->hasOne(Empleado::className(), ['id' => 'empleado_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProceso()
    {
        return $this->hasOne(Proceso::className(), ['id' => 'proceso_id']);
    }
}
