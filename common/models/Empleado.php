<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "empleado".
 *
 * @property int $id
 * @property int $user_id
 * @property string $nombre
 * @property string $apellido
 * @property string $telefono
 * @property string $celular
 * @property int $estado
 *
 * @property User $user
 * @property EmpleadoSucursal[] $empleadoSucursals
 * @property Sucursal[] $sucursals
 * @property Orden[] $ordens
 * @property Orden[] $ordens0
 */
class Empleado extends \yii\db\ActiveRecord
{
//    public $sucursal_defecto;
//    public $sucursal_encargadas;
//    public $proceso_id;
//    public $encargado_proceso;

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'apellido', 'nombre', 'estado'], 'required'],
            [['user_id'], 'integer'],
//            [['sucursal_encargadas', 'proceso_id', 'encargado_proceso'], 'safe'],
            [['nombre', 'apellido'], 'string', 'max' => 250],
            [['telefono', 'celular'], 'string', 'max' => 20],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User',
//            'sucursal_defecto' => 'Sucursal Defecto',
//            'proceso_id' => 'Proceso',
//            'sucursal_encargadas' => 'Sucursales Encargadas',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'telefono' => 'Telefono',
            'celular' => 'Celular',
            'estado' => 'Estado',
//            'encargado_proceso' => 'Encargado'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleadoSucursals()
    {
        return $this->hasMany(EmpleadoSucursal::className(), ['empleado_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSucursals()
    {
        return $this->hasMany(Sucursal::className(), ['id' => 'sucursal_id'])->viaTable('empleado_sucursal', ['empleado_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdens()
    {
        return $this->hasMany(Orden::className(), ['entrega_entrega_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdens0()
    {
        return $this->hasMany(Orden::className(), ['empleado_recibe_id' => 'id']);
    }


    public function getNombres()
    {
        return $this->nombre . ' ' . $this->apellido;
    }

    public static function empleadosXProcesoId($proceso_id)
    {
        $sucursal = Orden::obtenerSucursal();
        $empleados = Empleado::find()
            ->innerJoinWith('empleadoSucursals')
            ->where(['empleado_sucursal.proceso_id' => $proceso_id, 'empleado_sucursal.sucursal_id' => $sucursal, 'empleado_sucursal.encargado_proceso' => 0])
            ->all();
        return $empleados;
    }

    public static function empleadoXProcesoEncargado($proceso_id, $empleado_id)
    {
        $sucursal = Orden::obtenerSucursal();
        $empleados = Empleado::find()
            ->innerJoinWith('empleadoSucursals')
            ->where(['empleado_sucursal.empleado_id' => $empleado_id, 'empleado_sucursal.proceso_id' => $proceso_id, 'empleado_sucursal.sucursal_id' => $sucursal, 'empleado_sucursal.encargado_proceso' => 1])
            ->one();
        return $empleados;
    }

    public static function obtenerEmpleadoSession($user_id = NULL)
    {
        return Empleado::findOne(['user_id' => (($user_id) ? $user_id : (\Yii::$app->user->id == 1) ? 2 : \Yii::$app->user->id)]);
    }
}
