<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "trabajo_estado".
 *
 * @property int $id
 * @property string $nombre
 * @property string $codigo
 * @property int $estado
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property OrdenTrabajo[] $ordenTrabajos
 */
class TrabajoEstado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trabajo_estado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'codigo'], 'required'],
            [['estado', 'created_at', 'updated_at'], 'integer'],
            [['nombre'], 'string', 'max' => 250],
            [['codigo'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'codigo' => 'Codigo',
            'estado' => 'Estado',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenTrabajos()
    {
        return $this->hasMany(OrdenTrabajo::className(), ['estado_id' => 'id']);
    }
}
