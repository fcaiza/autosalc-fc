<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "vehiculo".
 *
 * @property int $id
 * @property int $cliente_id
 * @property int $marca_id
 * @property int $modelo_id
 * @property int $pais_id
 * @property int $clase_id
 * @property int $servicio_id
 * @property int $color_1_id
 * @property int $color_2_id
 * @property int $anio
 * @property string $placa
 * @property int $cilindraje
 * @property string $ramv_cpn
 * @property int $matriculado
 * @property string $desde
 * @property string $hasta
 * @property string $informacion
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Clase $clase
 * @property Cliente $cliente
 * @property Color $color1
 * @property Color $color2
 * @property Marca $marca
 * @property Modelo $modelo
 * @property Pais $pais
 * @property Servicio $servicio
 */
class Vehiculo extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vehiculo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cliente_id', 'placa', 'marca_id', 'modelo_id'], 'required'],
            [['placa'], 'validatePlaca'],
            [['cliente_id', 'marca_id', 'modelo_id', 'pais_id', 'clase_id', 'servicio_id', 'color_1_id', 'color_2_id', 'anio', 'cilindraje', 'created_at', 'updated_at'], 'integer'],
            [['matriculado'], 'integer'],
            [['desde', 'hasta'], 'safe'],
            [['informacion'], 'string'],
            [['placa'], 'string', 'max' => 20],
            [['ramv_cpn'], 'string', 'max' => 50],
            [['clase_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clase::className(), 'targetAttribute' => ['clase_id' => 'id']],
            [['cliente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['cliente_id' => 'id']],
            [['color_1_id'], 'exist', 'skipOnError' => true, 'targetClass' => Color::className(), 'targetAttribute' => ['color_1_id' => 'id']],
            [['color_2_id'], 'exist', 'skipOnError' => true, 'targetClass' => Color::className(), 'targetAttribute' => ['color_2_id' => 'id']],
            [['marca_id'], 'exist', 'skipOnError' => true, 'targetClass' => Marca::className(), 'targetAttribute' => ['marca_id' => 'id']],
            [['modelo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Modelo::className(), 'targetAttribute' => ['modelo_id' => 'id']],
            [['pais_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pais::className(), 'targetAttribute' => ['pais_id' => 'id']],
            [['servicio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Servicio::className(), 'targetAttribute' => ['servicio_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cliente_id' => 'Cliente',
            'marca_id' => 'Marca',
            'modelo_id' => 'Modelo',
            'pais_id' => 'Pais',
            'clase_id' => 'Clase',
            'servicio_id' => 'Servicio',
            'color_1_id' => 'Color 1',
            'color_2_id' => 'Color 2',
            'anio' => 'Año',
            'placa' => 'Placa',
            'cilindraje' => 'Cilindraje',
            'ramv_cpn' => 'Ramv Cpn',
            'matriculado' => 'Matriculado',
            'desde' => 'Desde',
            'hasta' => 'Hasta',
            'informacion' => 'Informacion',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClase()
    {
        return $this->hasOne(Clase::className(), ['id' => 'clase_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Cliente::className(), ['id' => 'cliente_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColor1()
    {
        return $this->hasOne(Color::className(), ['id' => 'color_1_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColor2()
    {
        return $this->hasOne(Color::className(), ['id' => 'color_2_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarca()
    {
        return $this->hasOne(Marca::className(), ['id' => 'marca_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModelo()
    {
        return $this->hasOne(Modelo::className(), ['id' => 'modelo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPais()
    {
        return $this->hasOne(Pais::className(), ['id' => 'pais_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicio()
    {
        return $this->hasOne(Servicio::className(), ['id' => 'servicio_id']);
    }

    public function validatePlaca()
    {
        $valor = static::find()->all();
        foreach ($valor as $va) {
            if ($this->placa == $va->placa && $this->id != $va->id) {
                $this->addError('placa', 'La placa ya esta registrada');
            }
        }
    }
}
