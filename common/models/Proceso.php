<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "proceso".
 *
 * @property int $id
 * @property string $nombre
 * @property string $detalle
 * @property int $orden
 * @property string $imagen
 * @property string $icono
 * @property int $estado
 * @property int $created_at
 * @property int $updated_at
 *
 * @property OrdenProceso[] $ordenProcesos
 */
class Proceso extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proceso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre','estado'], 'required'],
            [['detalle'], 'string'],
            [['orden', 'created_at', 'updated_at'], 'integer'],
            [['nombre'], 'string', 'max' => 250],
            [['imagen', 'icono'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'detalle' => 'Detalle',
            'orden' => 'Orden',
            'imagen' => 'Imagen',
            'icono' => 'Icono',
            'estado' => 'Estado',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenProcesos()
    {
        return $this->hasMany(OrdenProceso::className(), ['proceso_id' => 'id']);
    }
}
