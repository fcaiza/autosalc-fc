<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "empresa".
 *
 * @property int $id
 * @property string $nombre
 * @property string $siglas
 * @property string $direccion
 * @property string $telefono
 * @property string $celular
 * @property string $wathshap
 * @property string $web
 * @property string $email
 * @property string $logo
 * @property int $estado
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Sucursal[] $sucursals
 */
class Empresa extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empresa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'estado'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['nombre'], 'string', 'max' => 250],
            [['siglas'], 'string', 'max' => 50],
            [['direccion'], 'string', 'max' => 500],
            [['telefono', 'celular', 'wathshap'], 'string', 'max' => 20],
            [['web', 'email', 'logo'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'siglas' => 'Siglas',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'celular' => 'Celular',
            'wathshap' => 'Whatsapp',
            'web' => 'Web',
            'email' => 'Email',
            'logo' => 'Logo',
            'estado' => 'Estado',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSucursals()
    {
        return $this->hasMany(Sucursal::className(), ['empresa_id' => 'id']);
    }
}
