<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "orden_estado".
 *
 * @property int $id
 * @property string $nombre
 * @property string $codigo
 * @property int $orden
 * @property int $estado
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Orden[] $ordens
 */
class OrdenEstado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orden_estado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'estado'], 'required'],
            [['orden', 'estado', 'created_at', 'updated_at'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['codigo'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'codigo' => 'Codigo',
            'orden' => 'Orden',
            'estado' => 'Estado',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdens()
    {
        return $this->hasMany(Orden::className(), ['estado_id' => 'id']);
    }
}
