<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "proceso_estado".
 *
 * @property int $id
 * @property string $nombre
 * @property string $codigo
 * @property int $estado
 * @property int $created_at
 * @property int $updated_at
 *
 * @property OrdenProceso[] $ordenProcesos
 */
class ProcesoEstado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proceso_estado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'codigo','estado'], 'required'],
            [['estado'], 'integer'],
            [['created_at', 'updated_at'], 'integer'],
            [['nombre'], 'string', 'max' => 250],
            [['codigo'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'codigo' => 'Codigo',
            'estado' => 'Estado',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenProcesos()
    {
        return $this->hasMany(OrdenProceso::className(), ['estado_id' => 'id']);
    }
}
