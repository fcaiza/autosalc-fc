<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "orden_trabajo".
 *
 * @property int $id
 * @property int $orden_proceso_id
 * @property int|null $empleado_id
 * @property int $estado_id
 * @property string $fecha_inicio
 * @property string|null $fecha_fin
 * @property string|null $trabajo
 * @property string|null $resultado
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property TrabajoEstado $estado
 * @property OrdenProceso $ordenProceso
 * @property OrdenTrabajoImagen[] $ordenTrabajoImagens
 */
class OrdenTrabajo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orden_trabajo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['orden_proceso_id', 'estado_id', 'fecha_inicio'], 'required'],
            [['orden_proceso_id', 'empleado_id', 'estado_id', 'created_at', 'updated_at'], 'integer'],
            [['fecha_inicio', 'fecha_fin'], 'safe'],
            [['trabajo', 'resultado'], 'string'],
            [['estado_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrabajoEstado::className(), 'targetAttribute' => ['estado_id' => 'id']],
            [['orden_proceso_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenProceso::className(), 'targetAttribute' => ['orden_proceso_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'orden_proceso_id' => 'Orden Proceso ID',
            'empleado_id' => 'Empleado ID',
            'estado_id' => 'Estado ID',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_fin' => 'Fecha Fin',
            'trabajo' => 'Trabajo',
            'resultado' => 'Resultado',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstado()
    {
        return $this->hasOne(TrabajoEstado::className(), ['id' => 'estado_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenProceso()
    {
        return $this->hasOne(OrdenProceso::className(), ['id' => 'orden_proceso_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenTrabajoImagens()
    {
        return $this->hasMany(OrdenTrabajoImagen::className(), ['orden_trabajo_id' => 'id']);
    }
}
