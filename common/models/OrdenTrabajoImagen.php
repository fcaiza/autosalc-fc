<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "orden_trabajo_imagen".
 *
 * @property int $id
 * @property int $orden_trabajo_id
 * @property string|null $detalle
 * @property string $imagen
 * @property int $envio_email
 * @property int|null $estado
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property OrdenTrabajo $ordenTrabajo
 */
class OrdenTrabajoImagen extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orden_trabajo_imagen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['orden_trabajo_id', 'imagen'], 'required'],
            [['orden_trabajo_id', 'envio_email', 'estado', 'created_at', 'updated_at'], 'integer'],
            [['detalle'], 'string', 'max' => 4000],
            [['imagen'], 'string', 'max' => 250],
            [['orden_trabajo_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenTrabajo::className(), 'targetAttribute' => ['orden_trabajo_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'orden_trabajo_id' => 'Orden Trabajo ID',
            'detalle' => 'Detalle',
            'imagen' => 'Imagen',
            'envio_email' => 'Envio Email',
            'estado' => 'Estado',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenTrabajo()
    {
        return $this->hasOne(OrdenTrabajo::className(), ['id' => 'orden_trabajo_id']);
    }
}
