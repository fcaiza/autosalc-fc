<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "catalogo_general".
 *
 * @property int $id
 * @property string $nombre
 * @property int $orden orden de listar
 * @property int $value valor de select
 * @property string $valor_1 opciones adicionales
 * @property string $valor_2 opciones adicionales
 * @property string $valor_3 opciones adicionales
 * @property int $catalogo_id recursividad del select
 * @property int $estado
 *
 * @property CatalogoGeneral $catalogo
 * @property CatalogoGeneral[] $catalogoGenerals
 * @property OrdenProceso[] $ordenProcesos
 * @property Vehiculo[] $vehiculos
 * @property Vehiculo[] $vehiculos0
 * @property Vehiculo[] $vehiculos1
 * @property Vehiculo[] $vehiculos2
 * @property Vehiculo[] $vehiculos3
 * @property Vehiculo[] $vehiculos4
 * @property Vehiculo[] $vehiculos5
 */
class CatalogoGeneral extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'catalogo_general';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['orden', 'value', 'catalogo_id'], 'integer'],
            [['estado'], 'integer'],
            [['nombre', 'valor_1', 'valor_2', 'valor_3'], 'string', 'max' => 100],
            [['catalogo_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatalogoGeneral::className(), 'targetAttribute' => ['catalogo_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'orden' => 'Orden',
            'value' => 'Value',
            'valor_1' => 'Valor 1',
            'valor_2' => 'Valor 2',
            'valor_3' => 'Valor 3',
            'catalogo_id' => 'Catalogo ID',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogo()
    {
        return $this->hasOne(CatalogoGeneral::className(), ['id' => 'catalogo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogoGenerals()
    {
        return $this->hasMany(CatalogoGeneral::className(), ['catalogo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenProcesos()
    {
        return $this->hasMany(OrdenProceso::className(), ['estado_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculos()
    {
        return $this->hasMany(Vehiculo::className(), ['clase_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculos0()
    {
        return $this->hasMany(Vehiculo::className(), ['color_1_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculos1()
    {
        return $this->hasMany(Vehiculo::className(), ['color_2_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculos2()
    {
        return $this->hasMany(Vehiculo::className(), ['marca_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculos3()
    {
        return $this->hasMany(Vehiculo::className(), ['modelo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculos4()
    {
        return $this->hasMany(Vehiculo::className(), ['pais_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculos5()
    {
        return $this->hasMany(Vehiculo::className(), ['servicio_id' => 'id']);
    }

    public static function listarCatalogo($id) {
        return self::find()
                ->where(['catalogo_id' => $id])
                ->all();
    }
}
