<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "orden_notificacion".
 *
 * @property int $id
 * @property int $orden_proceso_id
 * @property string $fecha_envio
 * @property string $titulo
 * @property string $mensaje
 * @property string $observacion
 * @property string $para
 * @property string $copia
 * @property string $adjunto
 * @property int $created_at
 * @property int $updated_at
 *
 * @property OrdenProceso $ordenProceso
 */
class OrdenNotificacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orden_notificacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['orden_proceso_id', 'fecha_envio', 'titulo', 'mensaje'], 'required'],
            [['orden_proceso_id', 'created_at', 'updated_at'], 'integer'],
            [['fecha_envio'], 'safe'],
            [['mensaje'], 'string'],
            [['titulo'], 'string', 'max' => 150],
            [['observacion', 'para', 'copia'], 'string', 'max' => 500],
            [['adjunto'], 'string', 'max' => 100],
            [['orden_proceso_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenProceso::className(), 'targetAttribute' => ['orden_proceso_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'orden_proceso_id' => 'Orden Proceso ID',
            'fecha_envio' => 'Fecha Envio',
            'titulo' => 'Titulo',
            'mensaje' => 'Mensaje',
            'observacion' => 'Observacion',
            'para' => 'Para',
            'copia' => 'Copia',
            'adjunto' => 'Adjunto',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenProceso()
    {
        return $this->hasOne(OrdenProceso::className(), ['id' => 'orden_proceso_id']);
    }
}
