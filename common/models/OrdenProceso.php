<?php

namespace common\models;

use yii\db\ActiveRecord;
use Yii;
use common\models\EmpleadoSucursal;

/**
 * This is the model class for table "orden_proceso".
 *
 * @property int $id
 * @property int $orden_id
 * @property int $proceso_id
 * @property int $estado_id
 * @property int $fase
 * @property int $responsable_id
 * @property int $asignado_id
 * @property string $fecha_inicio
 * @property string $observacion
 * @property string $fecha_fin
 * @property string $resultado
 * @property int $notificacion
 * @property int $created_at
 * @property int $updated_at
 *
 * @property OrdenNotificacion[] $ordenNotificacions
 * @property Orden $orden
 * @property ProcesoEstado $estado
 * @property Proceso $proceso
 * @property OrdenTrabajo[] $ordenTrabajos
 */
class OrdenProceso extends \yii\db\ActiveRecord {

    public function behaviors() {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'orden_proceso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['orden_id', 'proceso_id', 'observacion', 'fase'], 'required'],
            [['orden_id', 'proceso_id', 'estado_id', 'fase', 'responsable_id', 'asignado_id', 'notificacion', 'created_at', 'updated_at'], 'integer'],
            [['fecha_inicio', 'fecha_fin'], 'safe'],
            [['observacion', 'resultado'], 'string'],
            [['orden_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orden::className(), 'targetAttribute' => ['orden_id' => 'id']],
            [['estado_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProcesoEstado::className(), 'targetAttribute' => ['estado_id' => 'id']],
            [['proceso_id'], 'exist', 'skipOnError' => true, 'targetClass' => Proceso::className(), 'targetAttribute' => ['proceso_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'orden_id' => 'Orden',
            'proceso_id' => 'Proceso',
            'estado_id' => 'Estado',
            'fase' => 'Fase',
            'responsable_id' => 'Responsable',
            'asignado_id' => 'Asignado',
            'fecha_inicio' => 'Fecha Inicio',
            'observacion' => 'Observacion',
            'fecha_fin' => 'Fecha Fin',
            'resultado' => 'Resultado',
            'notificacion' => 'Notificacion',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenNotificacions() {
        return $this->hasMany(OrdenNotificacion::className(), ['orden_proceso_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrden() {
        return $this->hasOne(Orden::className(), ['id' => 'orden_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstado() {
        return $this->hasOne(ProcesoEstado::className(), ['id' => 'estado_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProceso() {
        return $this->hasOne(Proceso::className(), ['id' => 'proceso_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenTrabajos() {
        return $this->hasMany(OrdenTrabajo::className(), ['orden_proceso_id' => 'id']);
    }

    public function getValidarResponsable() {
        $bandera = EmpleadoSucursal::findOne(['proceso_id' => $this->proceso_id, 'encargado_proceso' => 1]);
        return ($bandera) ? true : false;
    }

}
