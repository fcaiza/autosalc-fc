<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "sucursal".
 *
 * @property int $id
 * @property int $empresa_id
 * @property string $nombre
 * @property string $ruc
 * @property string $telefono
 * @property string $celular
 * @property int $estado
 * @property int $secuencial
 * @property int $created_at
 * @property int $updated_at
 *
 * @property EmpleadoSucursal[] $empleadoSucursals
 * @property Empleado[] $empleados
 * @property Empresa $empresa
 */
class Sucursal extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sucursal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['empresa_id', 'created_at', 'updated_at', 'estado', 'secuencial'], 'integer'],
            [['nombre', 'estado'], 'required'],
            [['nombre'], 'string', 'max' => 250],
            [['ruc', 'telefono', 'celular'], 'string', 'max' => 20],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'empresa_id' => 'Empresa ID',
            'nombre' => 'Nombre',
            'ruc' => 'Ruc',
            'telefono' => 'Telefono',
            'celular' => 'Celular',
            'estado' => 'Estado',
            'secuencial' => 'Secuencial',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleadoSucursals()
    {
        return $this->hasMany(EmpleadoSucursal::className(), ['sucursal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleados()
    {
        return $this->hasMany(Empleado::className(), ['id' => 'empleado_id'])->viaTable('empleado_sucursal', ['sucursal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

//    public static function generarNumero($bandera = 0)
//    {
//        $sucursal = $this->obtenerSucursal();
//        
//        $generador = self::findOne($sucursal);
//        $indice = $generador->secuencial + 1;
//        if ($bandera)
//        {
//            $generador->secuencial = $indice;
//            $generador->save();
//        }
//        
//        $secuencial = str_pad($indice, 10, "0", STR_PAD_LEFT);
//
//        return $secuencial;
//    }
//    
//    public static function obtenerSucursal()
//    {
//        $sucursal = 1;
//        $cookies = \Yii::$app->request->cookies;
//        if ($cookies->has('sucursal'))
//        {
//            if (($cookie = $cookies->get('sucursal')) !== null)
//            {
//                $sucursal = $cookie->value;
//            }
//        }
//        return $sucursal;
//    }

}
