<?php
return [
    'adminEmail' => 'lcaiza@aurysolutions.com',
    'supportEmail' => 'info@aurysolutions.com',
    'senderEmail' => 'info@aurysolutions.com',
    'senderName' => 'Aury Soluitons',
    'user.passwordResetTokenExpire' => 3600,
];
