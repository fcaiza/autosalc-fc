/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.19 : Database - cdiabeji_autosalc
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cdiabeji_autosalc` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `cdiabeji_autosalc`;

/*Table structure for table `auth_assignment` */

DROP TABLE IF EXISTS `auth_assignment`;

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `auth_assignment_user_id_idx` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_assignment` */

insert  into `auth_assignment`(`item_name`,`user_id`,`created_at`) values ('administracion','3',1562628661),('administrador','2',1559913151),('datos-personales','3',1562628656),('empleado','4',1559913118),('gerente','3',1559913134),('super-admin','1',1559913173),('super-admin','2',1566352868);

/*Table structure for table `auth_item` */

DROP TABLE IF EXISTS `auth_item`;

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_item` */

insert  into `auth_item`(`name`,`type`,`description`,`rule_name`,`data`,`created_at`,`updated_at`) values ('/*',2,NULL,NULL,NULL,1566705285,1566705285),('/admin/assignment/assign',2,NULL,NULL,NULL,1559912163,1559912163),('/admin/assignment/index',2,NULL,NULL,NULL,1559912162,1559912162),('/admin/assignment/revoke',2,NULL,NULL,NULL,1559912163,1559912163),('/admin/assignment/view',2,NULL,NULL,NULL,1559912163,1559912163),('/admin/default/index',2,NULL,NULL,NULL,1559912163,1559912163),('/admin/menu/index',2,NULL,NULL,NULL,1567128763,1567128763),('/admin/permission/assign',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/permission/create',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/permission/delete',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/permission/index',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/permission/remove',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/permission/update',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/permission/view',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/role/assign',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/role/create',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/role/delete',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/role/index',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/role/remove',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/role/update',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/role/view',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/route/assign',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/route/create',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/route/index',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/route/refresh',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/route/remove',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/rule/create',2,NULL,NULL,NULL,1559912166,1559912166),('/admin/rule/delete',2,NULL,NULL,NULL,1559912166,1559912166),('/admin/rule/index',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/rule/update',2,NULL,NULL,NULL,1559912166,1559912166),('/admin/rule/view',2,NULL,NULL,NULL,1559912166,1559912166),('/admin/user/activate',2,NULL,NULL,NULL,1559912167,1559912167),('/admin/user/change-password',2,NULL,NULL,NULL,1559912167,1559912167),('/admin/user/delete',2,NULL,NULL,NULL,1559912166,1559912166),('/admin/user/index',2,NULL,NULL,NULL,1559912166,1559912166),('/admin/user/login',2,NULL,NULL,NULL,1559912166,1559912166),('/admin/user/logout',2,NULL,NULL,NULL,1559912166,1559912166),('/admin/user/request-password-reset',2,NULL,NULL,NULL,1559912167,1559912167),('/admin/user/reset-password',2,NULL,NULL,NULL,1559912167,1559912167),('/admin/user/signup',2,NULL,NULL,NULL,1559912166,1559912166),('/admin/user/view',2,NULL,NULL,NULL,1559912166,1559912166),('/broker/index',2,NULL,NULL,NULL,1567128763,1567128763),('/catalogo-general/create',2,NULL,NULL,NULL,1559912167,1559912167),('/catalogo-general/delete',2,NULL,NULL,NULL,1559912168,1559912168),('/catalogo-general/index',2,NULL,NULL,NULL,1559912167,1559912167),('/catalogo-general/update',2,NULL,NULL,NULL,1559912167,1559912167),('/catalogo-general/view',2,NULL,NULL,NULL,1559912167,1559912167),('/clase/index',2,NULL,NULL,NULL,1567128763,1567128763),('/cliente/index',2,NULL,NULL,NULL,1567128763,1567128763),('/color/index',2,NULL,NULL,NULL,1567128763,1567128763),('/dato/consultar',2,NULL,NULL,NULL,1560886737,1560886737),('/dato/create',2,NULL,NULL,NULL,1559912168,1559912168),('/dato/delete',2,NULL,NULL,NULL,1559912168,1559912168),('/dato/index',2,NULL,NULL,NULL,1559912168,1559912168),('/dato/update',2,NULL,NULL,NULL,1559912168,1559912168),('/dato/view',2,NULL,NULL,NULL,1559912168,1559912168),('/debug/default/index',2,NULL,NULL,NULL,1567128763,1567128763),('/empleado/index',2,NULL,NULL,NULL,1567128763,1567128763),('/empresa/*',2,NULL,NULL,NULL,1559831635,1559831635),('/empresa/create',2,NULL,NULL,NULL,1559912168,1559912168),('/empresa/delete',2,NULL,NULL,NULL,1559912169,1559912169),('/empresa/index',2,NULL,NULL,NULL,1559912168,1559912168),('/empresa/update',2,NULL,NULL,NULL,1559912169,1559912169),('/empresa/view',2,NULL,NULL,NULL,1559912168,1559912168),('/gii/*',2,NULL,NULL,NULL,1559912167,1559912167),('/gii/default/index',2,NULL,NULL,NULL,1567128763,1567128763),('/gridview/*',2,NULL,NULL,NULL,1559912046,1559912046),('/gridview/export/download',2,NULL,NULL,NULL,1559912052,1559912052),('/marca/index',2,NULL,NULL,NULL,1567128763,1567128763),('/menu/create',2,NULL,NULL,NULL,1559912169,1559912169),('/menu/delete',2,NULL,NULL,NULL,1559912169,1559912169),('/menu/index',2,NULL,NULL,NULL,1559912169,1559912169),('/menu/update',2,NULL,NULL,NULL,1559912169,1559912169),('/menu/view',2,NULL,NULL,NULL,1559912169,1559912169),('/modelo/index',2,NULL,NULL,NULL,1567128763,1567128763),('/orden-estado/create',2,NULL,NULL,NULL,1559912170,1559912170),('/orden-estado/delete',2,NULL,NULL,NULL,1559912170,1559912170),('/orden-estado/index',2,NULL,NULL,NULL,1559912170,1559912170),('/orden-estado/update',2,NULL,NULL,NULL,1559912170,1559912170),('/orden-estado/view',2,NULL,NULL,NULL,1559912170,1559912170),('/orden-notificacion/index',2,NULL,NULL,NULL,1567128763,1567128763),('/orden-proceso/index',2,NULL,NULL,NULL,1567128763,1567128763),('/orden-trabajo-imagen/index',2,NULL,NULL,NULL,1567128763,1567128763),('/orden-trabajo/index',2,NULL,NULL,NULL,1567128763,1567128763),('/orden/create',2,NULL,NULL,NULL,1559912170,1559912170),('/orden/delete',2,NULL,NULL,NULL,1559912170,1559912170),('/orden/index',2,NULL,NULL,NULL,1559912169,1559912169),('/orden/update',2,NULL,NULL,NULL,1559912170,1559912170),('/orden/view',2,NULL,NULL,NULL,1559912169,1559912169),('/pais/index',2,NULL,NULL,NULL,1567128763,1567128763),('/proceso-estado/index',2,NULL,NULL,NULL,1567128763,1567128763),('/proceso/create',2,NULL,NULL,NULL,1559912171,1559912171),('/proceso/delete',2,NULL,NULL,NULL,1559912171,1559912171),('/proceso/index',2,NULL,NULL,NULL,1559912170,1559912170),('/proceso/update',2,NULL,NULL,NULL,1559912171,1559912171),('/proceso/view',2,NULL,NULL,NULL,1559912171,1559912171),('/seguro/index',2,NULL,NULL,NULL,1567128763,1567128763),('/servicio/index',2,NULL,NULL,NULL,1567128763,1567128763),('/site/error',2,NULL,NULL,NULL,1540252678,1540252678),('/site/index',2,NULL,NULL,NULL,1540252678,1540252678),('/site/login',2,NULL,NULL,NULL,1540252678,1540252678),('/site/logout',2,NULL,NULL,NULL,1540252678,1540252678),('/sucursal/index',2,NULL,NULL,NULL,1567128763,1567128763),('/tipo-dato/create',2,NULL,NULL,NULL,1559912171,1559912171),('/tipo-dato/delete',2,NULL,NULL,NULL,1559912171,1559912171),('/tipo-dato/index',2,NULL,NULL,NULL,1559912171,1559912171),('/tipo-dato/update',2,NULL,NULL,NULL,1559912171,1559912171),('/tipo-dato/view',2,NULL,NULL,NULL,1559912171,1559912171),('/tipo-documento/create',2,NULL,NULL,NULL,1559912172,1559912172),('/tipo-documento/delete',2,NULL,NULL,NULL,1559912172,1559912172),('/tipo-documento/index',2,NULL,NULL,NULL,1559912172,1559912172),('/tipo-documento/update',2,NULL,NULL,NULL,1559912172,1559912172),('/tipo-documento/view',2,NULL,NULL,NULL,1559912172,1559912172),('/vehiculo/index',2,NULL,NULL,NULL,1567128763,1567128763),('admin-configuracion',2,'para crear rutas, menus, roles y permisos',NULL,NULL,1559912232,1559912249),('administracion',2,'para administrar el sistema a nivel general',NULL,NULL,1559912534,1559912534),('administrador',1,'para la administracion general del sistema',NULL,NULL,1559912935,1559912980),('configuracion',2,'para la configuracion del sistema',NULL,NULL,1559912325,1559912325),('datos-personales',2,'para administracion de datos de clientes, empleados, brokers, aseguradoras y mas',NULL,NULL,1560002135,1560002135),('empleado',1,NULL,NULL,NULL,1559913091,1559913091),('gerente',1,'perfil de gerente de la empresa',NULL,NULL,1559913038,1559913038),('proceso-orden',2,'para la creacion de ordenes',NULL,NULL,1559912717,1559912717),('site-general',2,NULL,NULL,NULL,1540252696,1540252696),('super-admin',1,'para administracion del sistema global',NULL,NULL,1559912879,1559912879);

/*Table structure for table `auth_item_child` */

DROP TABLE IF EXISTS `auth_item_child`;

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_item_child` */

insert  into `auth_item_child`(`parent`,`child`) values ('admin-configuracion','/*'),('admin-configuracion','/admin/assignment/assign'),('admin-configuracion','/admin/assignment/index'),('admin-configuracion','/admin/assignment/revoke'),('admin-configuracion','/admin/assignment/view'),('admin-configuracion','/admin/default/index'),('admin-configuracion','/admin/menu/index'),('admin-configuracion','/admin/permission/assign'),('admin-configuracion','/admin/permission/create'),('admin-configuracion','/admin/permission/delete'),('admin-configuracion','/admin/permission/index'),('admin-configuracion','/admin/permission/remove'),('admin-configuracion','/admin/permission/update'),('admin-configuracion','/admin/permission/view'),('admin-configuracion','/admin/role/assign'),('admin-configuracion','/admin/role/create'),('admin-configuracion','/admin/role/delete'),('admin-configuracion','/admin/role/index'),('admin-configuracion','/admin/role/remove'),('admin-configuracion','/admin/role/update'),('admin-configuracion','/admin/role/view'),('admin-configuracion','/admin/route/assign'),('admin-configuracion','/admin/route/create'),('admin-configuracion','/admin/route/index'),('admin-configuracion','/admin/route/refresh'),('admin-configuracion','/admin/route/remove'),('admin-configuracion','/admin/rule/create'),('admin-configuracion','/admin/rule/delete'),('admin-configuracion','/admin/rule/index'),('admin-configuracion','/admin/rule/update'),('admin-configuracion','/admin/rule/view'),('admin-configuracion','/admin/user/activate'),('admin-configuracion','/admin/user/change-password'),('admin-configuracion','/admin/user/delete'),('admin-configuracion','/admin/user/index'),('admin-configuracion','/admin/user/login'),('admin-configuracion','/admin/user/logout'),('admin-configuracion','/admin/user/request-password-reset'),('admin-configuracion','/admin/user/reset-password'),('admin-configuracion','/admin/user/signup'),('admin-configuracion','/admin/user/view'),('admin-configuracion','/broker/index'),('admin-configuracion','/catalogo-general/create'),('admin-configuracion','/catalogo-general/delete'),('admin-configuracion','/catalogo-general/index'),('admin-configuracion','/catalogo-general/update'),('admin-configuracion','/catalogo-general/view'),('admin-configuracion','/clase/index'),('admin-configuracion','/cliente/index'),('admin-configuracion','/color/index'),('site-general','/dato/consultar'),('datos-personales','/dato/create'),('datos-personales','/dato/delete'),('admin-configuracion','/dato/index'),('datos-personales','/dato/index'),('datos-personales','/dato/update'),('datos-personales','/dato/view'),('admin-configuracion','/debug/default/index'),('admin-configuracion','/empleado/index'),('configuracion','/empresa/create'),('configuracion','/empresa/delete'),('admin-configuracion','/empresa/index'),('configuracion','/empresa/index'),('configuracion','/empresa/update'),('configuracion','/empresa/view'),('admin-configuracion','/gii/default/index'),('admin-configuracion','/marca/index'),('admin-configuracion','/menu/create'),('admin-configuracion','/menu/delete'),('admin-configuracion','/menu/index'),('admin-configuracion','/menu/update'),('admin-configuracion','/menu/view'),('admin-configuracion','/modelo/index'),('administracion','/orden-estado/create'),('administracion','/orden-estado/delete'),('admin-configuracion','/orden-estado/index'),('administracion','/orden-estado/index'),('administracion','/orden-estado/update'),('administracion','/orden-estado/view'),('admin-configuracion','/orden-notificacion/index'),('admin-configuracion','/orden-proceso/index'),('admin-configuracion','/orden-trabajo-imagen/index'),('admin-configuracion','/orden-trabajo/index'),('proceso-orden','/orden/create'),('proceso-orden','/orden/delete'),('admin-configuracion','/orden/index'),('proceso-orden','/orden/index'),('proceso-orden','/orden/update'),('proceso-orden','/orden/view'),('admin-configuracion','/pais/index'),('admin-configuracion','/proceso-estado/index'),('administracion','/proceso/create'),('administracion','/proceso/delete'),('admin-configuracion','/proceso/index'),('administracion','/proceso/index'),('administracion','/proceso/update'),('administracion','/proceso/view'),('admin-configuracion','/seguro/index'),('admin-configuracion','/servicio/index'),('site-general','/site/error'),('admin-configuracion','/site/index'),('site-general','/site/index'),('site-general','/site/login'),('site-general','/site/logout'),('admin-configuracion','/sucursal/index'),('configuracion','/tipo-dato/create'),('configuracion','/tipo-dato/delete'),('admin-configuracion','/tipo-dato/index'),('configuracion','/tipo-dato/index'),('configuracion','/tipo-dato/update'),('configuracion','/tipo-dato/view'),('configuracion','/tipo-documento/create'),('configuracion','/tipo-documento/delete'),('admin-configuracion','/tipo-documento/index'),('configuracion','/tipo-documento/index'),('configuracion','/tipo-documento/update'),('configuracion','/tipo-documento/view'),('admin-configuracion','/vehiculo/index'),('super-admin','admin-configuracion'),('administrador','administracion'),('super-admin','administracion'),('administrador','configuracion'),('super-admin','configuracion'),('administrador','datos-personales'),('super-admin','datos-personales'),('administrador','proceso-orden'),('gerente','proceso-orden'),('super-admin','proceso-orden'),('administrador','site-general'),('empleado','site-general'),('gerente','site-general'),('super-admin','site-general');

/*Table structure for table `auth_rule` */

DROP TABLE IF EXISTS `auth_rule`;

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_rule` */

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob,
  `icon` varchar(250) DEFAULT NULL,
  `option` varchar(250) DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

/*Data for the table `menu` */

insert  into `menu`(`id`,`name`,`parent`,`route`,`order`,`data`,`icon`,`option`,`estado`) values (1,'Dashboard',NULL,'/site/index',1,NULL,'tachometer',NULL,1),(2,'Administracion',NULL,NULL,3,NULL,'wrench',NULL,1),(3,'Rutas',11,'/admin/route/index',2,NULL,'code-fork',NULL,1),(4,'Permisos',11,'/admin/permission/index',4,NULL,'lock',NULL,1),(5,'Roles',11,'/admin/role/index',5,NULL,'id-card',NULL,1),(6,'Asignaciones',11,'/admin/assignment/index',6,NULL,'key',NULL,1),(7,'Menu',11,'/menu/index',7,NULL,'bars',NULL,1),(8,'Catalogo',11,'/catalogo-general/index',1,NULL,'sort-amount-asc',NULL,1),(10,'Tipo Documento',11,'/tipo-documento/index',3,NULL,'address-book-o',NULL,1),(11,'Configuracion',NULL,NULL,2,NULL,'gears',NULL,1),(12,'Empresa',2,'/empresa/index',6,NULL,'home',NULL,1),(13,'Estado Orden',11,'/orden-estado/index',8,NULL,'random',NULL,1),(14,'Clientes',2,'/cliente/index',4,NULL,'id-card-o',NULL,1),(15,'Proveedor Seguro',2,'/seguro/index',5,NULL,'gavel',NULL,1),(16,'Empleado',2,'/empleado/index',7,NULL,'user-o',NULL,1),(17,'Ordenes',NULL,NULL,4,NULL,'folder-open',NULL,1),(18,'Ingreso',17,'/orden/index',1,NULL,'file-text-o',NULL,1),(19,'Procesos',11,'/proceso/index',9,NULL,'gear',NULL,1),(20,'Avance',17,'/orden/index',2,NULL,'handshake-o',NULL,1),(21,'Notificacion',17,'/orden/index',3,NULL,'envelope-o',NULL,1),(22,'Reportes',NULL,NULL,5,NULL,'newspaper-o',NULL,1),(23,'Analisis Ordenes',22,'/admin/default/index',1,NULL,'bar-chart',NULL,1),(24,'Marca',31,'/marca/index',9,NULL,'',NULL,1),(25,'Modelo',31,'/modelo/index',10,NULL,'',NULL,1),(26,'Clase',31,'/clase/index',11,NULL,'',NULL,1),(27,'Pais',31,'/pais/index',12,NULL,'',NULL,1),(28,'Servicio',31,'/servicio/index',13,NULL,'',NULL,1),(29,'Color',31,'/color/index',14,NULL,'',NULL,1),(30,'Vehiculo',2,'/vehiculo/index',8,NULL,'',NULL,1),(31,'Vehiculos',11,NULL,10,NULL,'',NULL,1),(32,'Estado Procesos',11,'/proceso-estado/index',11,NULL,'',NULL,1),(33,'Broker',2,'/broker/index',6,NULL,'',NULL,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
