/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.19 : Database - cdiabeji_autosalc
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cdiabeji_autosalc` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `cdiabeji_autosalc`;

/*Table structure for table `auth_assignment` */

DROP TABLE IF EXISTS `auth_assignment`;

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `auth_assignment_user_id_idx` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_assignment` */

insert  into `auth_assignment`(`item_name`,`user_id`,`created_at`) values ('Asesor','12',1576809763),('Asesor','2',1576892574),('Enderezado','16',1577055039),('Sistemas','2',1573779327),('super-admin','1',1559913173);

/*Table structure for table `auth_item` */

DROP TABLE IF EXISTS `auth_item`;

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_item` */

insert  into `auth_item`(`name`,`type`,`description`,`rule_name`,`data`,`created_at`,`updated_at`) values ('/*',2,NULL,NULL,NULL,1571970978,1571970978),('/admin/assignment/assign',2,NULL,NULL,NULL,1559912163,1559912163),('/admin/assignment/index',2,NULL,NULL,NULL,1559912162,1559912162),('/admin/assignment/revoke',2,NULL,NULL,NULL,1559912163,1559912163),('/admin/assignment/view',2,NULL,NULL,NULL,1559912163,1559912163),('/admin/default/index',2,NULL,NULL,NULL,1559912163,1559912163),('/admin/menu/index',2,NULL,NULL,NULL,1567128763,1567128763),('/admin/permission/assign',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/permission/create',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/permission/delete',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/permission/index',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/permission/remove',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/permission/update',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/permission/view',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/role/assign',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/role/create',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/role/delete',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/role/index',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/role/remove',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/role/update',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/role/view',2,NULL,NULL,NULL,1559912164,1559912164),('/admin/route/assign',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/route/create',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/route/index',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/route/refresh',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/route/remove',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/rule/create',2,NULL,NULL,NULL,1559912166,1559912166),('/admin/rule/delete',2,NULL,NULL,NULL,1559912166,1559912166),('/admin/rule/index',2,NULL,NULL,NULL,1559912165,1559912165),('/admin/rule/update',2,NULL,NULL,NULL,1559912166,1559912166),('/admin/rule/view',2,NULL,NULL,NULL,1559912166,1559912166),('/admin/user/activate',2,NULL,NULL,NULL,1559912167,1559912167),('/admin/user/change-password',2,NULL,NULL,NULL,1559912167,1559912167),('/admin/user/delete',2,NULL,NULL,NULL,1559912166,1559912166),('/admin/user/index',2,NULL,NULL,NULL,1559912166,1559912166),('/admin/user/login',2,NULL,NULL,NULL,1559912166,1559912166),('/admin/user/logout',2,NULL,NULL,NULL,1559912166,1559912166),('/admin/user/request-password-reset',2,NULL,NULL,NULL,1559912167,1559912167),('/admin/user/reset-password',2,NULL,NULL,NULL,1559912167,1559912167),('/admin/user/signup',2,NULL,NULL,NULL,1559912166,1559912166),('/admin/user/view',2,NULL,NULL,NULL,1559912166,1559912166),('/catalogo-general/anios',2,NULL,NULL,NULL,1571969231,1571969231),('/catalogo-general/create',2,NULL,NULL,NULL,1571969231,1571969231),('/catalogo-general/delete',2,NULL,NULL,NULL,1571969231,1571969231),('/catalogo-general/index',2,NULL,NULL,NULL,1571969231,1571969231),('/catalogo-general/update',2,NULL,NULL,NULL,1571969231,1571969231),('/catalogo-general/view',2,NULL,NULL,NULL,1571969231,1571969231),('/global/delete-generico-doc-img',2,NULL,NULL,NULL,1576811150,1576811150),('/global/delete-img',2,NULL,NULL,NULL,1576811150,1576811150),('/global/file-froala',2,NULL,NULL,NULL,1576811150,1576811150),('/global/guardar-generico-doc-img',2,NULL,NULL,NULL,1576811150,1576811150),('/global/reset',2,NULL,NULL,NULL,1576811150,1576811150),('/global/reset-email',2,NULL,NULL,NULL,1576811150,1576811150),('/global/upload',2,NULL,NULL,NULL,1576811047,1576811047),('/global/upload-file1',2,NULL,NULL,NULL,1576811047,1576811047),('/global/upload-generico-doc-img',2,NULL,NULL,NULL,1576811046,1576811046),('/gridview/export/download',2,NULL,NULL,NULL,1559912052,1559912052),('/menu/create',2,NULL,NULL,NULL,1559912169,1559912169),('/menu/delete',2,NULL,NULL,NULL,1559912169,1559912169),('/menu/index',2,NULL,NULL,NULL,1559912169,1559912169),('/menu/update',2,NULL,NULL,NULL,1559912169,1559912169),('/menu/view',2,NULL,NULL,NULL,1559912169,1559912169),('/orden-proceso/index',2,NULL,NULL,NULL,1577069980,1577069980),('/orden-proceso/update',2,NULL,NULL,NULL,1577069980,1577069980),('/orden-proceso/view',2,NULL,NULL,NULL,1577069980,1577069980),('/orden/asignacion',2,NULL,NULL,NULL,1577048346,1577048346),('/orden/consultar',2,NULL,NULL,NULL,1573782043,1573782043),('/orden/consultar-responsable',2,NULL,NULL,NULL,1573782043,1573782043),('/orden/create',2,NULL,NULL,NULL,1573782043,1573782043),('/orden/delete',2,NULL,NULL,NULL,1573782043,1573782043),('/orden/ejecucion',2,NULL,NULL,NULL,1573782043,1573782043),('/orden/index',2,NULL,NULL,NULL,1573782043,1573782043),('/orden/notificacion',2,NULL,NULL,NULL,1573782043,1573782043),('/orden/planificacion',2,NULL,NULL,NULL,1573782043,1573782043),('/orden/update',2,NULL,NULL,NULL,1573782043,1573782043),('/orden/vehiculos-cliente',2,NULL,NULL,NULL,1573782043,1573782043),('/orden/view',2,NULL,NULL,NULL,1573782043,1573782043),('/site/index',2,NULL,NULL,NULL,1571964172,1571964172),('Administrativo',1,'Personal de oficina',NULL,NULL,1571963373,1571963373),('Armado',1,'Seccion de armado',NULL,NULL,1571963223,1571963223),('Aseguradora',1,'Entidades aseguradoras',NULL,NULL,1571963988,1571963988),('Asesor',1,'Personal que recibe los autos',NULL,NULL,1571963312,1571963312),('asignar-ordenes',2,NULL,NULL,NULL,1577048260,1577048260),('Broker',1,'Entidad de servicio',NULL,NULL,1571963970,1571963970),('Cliente',1,'Para accesos y permisos de todos los clientes del sistema',NULL,NULL,1571963946,1571963946),('config-admin',2,'Modulo de configuraciones globales',NULL,NULL,1571969141,1571969141),('crear-ordenes',2,NULL,NULL,NULL,1576810240,1576810240),('ejecutar-ordenes',2,NULL,NULL,NULL,1577049397,1577049397),('Enderezado',1,'Para empleados del area de enderezado',NULL,NULL,1571963083,1571963083),('Gerencia',1,'Jefes y dueños',NULL,NULL,1571963348,1571963348),('Mecanica',1,'Para empleados del ares mecanica',NULL,NULL,1571963190,1571963190),('Pintura',1,'Para empleados del area de pintura',NULL,NULL,1571963206,1571963206),('planificar-ordenes',2,NULL,NULL,NULL,1576948003,1576948003),('Pre-Entrega',1,'Para area de pre entrega del porducto ',NULL,NULL,1571963291,1571963291),('Pulido',1,'Area de pulido',NULL,NULL,1571963260,1571963260),('Sistemas',1,'Para el encargado del software y funciones principales de la oficina',NULL,NULL,1571963163,1571964109),('site-general',2,'Para accesos basicos de los usuarios',NULL,NULL,1540252696,1540252696),('super-admin',1,'para administracion del sistema global',NULL,NULL,1559912879,1559912879),('super-admin-config',2,'para crear rutas, menus, roles, asignaciones y permisos',NULL,NULL,1559912232,1571963541),('Trabajos-Especiales',1,'Para empleados de trabajos especiales',NULL,NULL,1571963247,1571963247);

/*Table structure for table `auth_item_child` */

DROP TABLE IF EXISTS `auth_item_child`;

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_item_child` */

insert  into `auth_item_child`(`parent`,`child`) values ('super-admin-config','/*'),('super-admin-config','/admin/assignment/assign'),('super-admin-config','/admin/assignment/index'),('super-admin-config','/admin/assignment/revoke'),('super-admin-config','/admin/assignment/view'),('super-admin-config','/admin/default/index'),('super-admin-config','/admin/menu/index'),('super-admin-config','/admin/permission/assign'),('super-admin-config','/admin/permission/create'),('super-admin-config','/admin/permission/delete'),('super-admin-config','/admin/permission/index'),('super-admin-config','/admin/permission/remove'),('super-admin-config','/admin/permission/update'),('super-admin-config','/admin/permission/view'),('super-admin-config','/admin/role/assign'),('super-admin-config','/admin/role/create'),('super-admin-config','/admin/role/delete'),('super-admin-config','/admin/role/index'),('super-admin-config','/admin/role/remove'),('super-admin-config','/admin/role/update'),('super-admin-config','/admin/role/view'),('super-admin-config','/admin/route/assign'),('super-admin-config','/admin/route/create'),('super-admin-config','/admin/route/index'),('super-admin-config','/admin/route/refresh'),('super-admin-config','/admin/route/remove'),('super-admin-config','/admin/rule/create'),('super-admin-config','/admin/rule/delete'),('super-admin-config','/admin/rule/index'),('super-admin-config','/admin/rule/update'),('super-admin-config','/admin/rule/view'),('super-admin-config','/admin/user/activate'),('super-admin-config','/admin/user/change-password'),('super-admin-config','/admin/user/delete'),('super-admin-config','/admin/user/index'),('super-admin-config','/admin/user/login'),('super-admin-config','/admin/user/logout'),('super-admin-config','/admin/user/request-password-reset'),('super-admin-config','/admin/user/reset-password'),('super-admin-config','/admin/user/signup'),('super-admin-config','/admin/user/view'),('config-admin','/catalogo-general/anios'),('config-admin','/catalogo-general/create'),('config-admin','/catalogo-general/delete'),('config-admin','/catalogo-general/index'),('config-admin','/catalogo-general/update'),('config-admin','/catalogo-general/view'),('crear-ordenes','/global/delete-generico-doc-img'),('crear-ordenes','/global/delete-img'),('crear-ordenes','/global/file-froala'),('crear-ordenes','/global/guardar-generico-doc-img'),('crear-ordenes','/global/reset'),('crear-ordenes','/global/reset-email'),('crear-ordenes','/global/upload'),('crear-ordenes','/global/upload-file1'),('crear-ordenes','/global/upload-generico-doc-img'),('super-admin-config','/menu/create'),('super-admin-config','/menu/delete'),('super-admin-config','/menu/index'),('super-admin-config','/menu/update'),('super-admin-config','/menu/view'),('asignar-ordenes','/orden-proceso/index'),('asignar-ordenes','/orden-proceso/update'),('asignar-ordenes','/orden/asignacion'),('crear-ordenes','/orden/consultar'),('crear-ordenes','/orden/consultar-responsable'),('crear-ordenes','/orden/create'),('crear-ordenes','/orden/index'),('planificar-ordenes','/orden/planificacion'),('crear-ordenes','/orden/update'),('crear-ordenes','/orden/vehiculos-cliente'),('crear-ordenes','/orden/view'),('site-general','/site/index'),('Enderezado','asignar-ordenes'),('Sistemas','asignar-ordenes'),('super-admin','config-admin'),('Asesor','crear-ordenes'),('Sistemas','planificar-ordenes'),('Administrativo','site-general'),('Armado','site-general'),('Asesor','site-general'),('Enderezado','site-general'),('Gerencia','site-general'),('Mecanica','site-general'),('Pintura','site-general'),('Pre-Entrega','site-general'),('Pulido','site-general'),('Sistemas','site-general'),('super-admin','site-general'),('Trabajos-Especiales','site-general'),('super-admin','super-admin-config');

/*Table structure for table `auth_rule` */

DROP TABLE IF EXISTS `auth_rule`;

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_rule` */

/*Table structure for table `broker` */

DROP TABLE IF EXISTS `broker`;

CREATE TABLE `broker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `ruc` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagen` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_broker` (`user_id`),
  CONSTRAINT `usuario_broker` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `broker` */

insert  into `broker`(`id`,`user_id`,`nombre`,`ruc`,`imagen`,`estado`,`created_at`,`updated_at`) values (1,7,'AMB Seguros','',NULL,1,1567480528,1571598462),(2,30,'Asertec S.A.','',NULL,1,1571598490,1571598490),(3,31,'ATLANTICO Broker De Seguros','',NULL,1,1571598536,1571598536);

/*Table structure for table `catalogo_general` */

DROP TABLE IF EXISTS `catalogo_general`;

CREATE TABLE `catalogo_general` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `orden` int(11) NOT NULL DEFAULT '1' COMMENT 'orden de listar',
  `value` int(11) DEFAULT NULL COMMENT 'valor de select',
  `valor_1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'opciones adicionales',
  `valor_2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'opciones adicionales',
  `valor_3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'opciones adicionales',
  `catalogo_id` int(11) DEFAULT NULL COMMENT 'recursividad del select',
  `estado` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `catalogo_subcatalogo` (`catalogo_id`),
  CONSTRAINT `catalogo_subcatalogo` FOREIGN KEY (`catalogo_id`) REFERENCES `catalogo_general` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `catalogo_general` */

insert  into `catalogo_general`(`id`,`nombre`,`orden`,`value`,`valor_1`,`valor_2`,`valor_3`,`catalogo_id`,`estado`) values (1,'Estados del Sistema',1,NULL,'','','',NULL,1),(2,'Activo',1,1,'','','',1,1),(3,'Inactivo',2,0,'','','',1,1),(4,'Opciones del Sistema',1,NULL,NULL,NULL,NULL,NULL,1),(5,'Si',1,1,NULL,NULL,NULL,4,1),(6,'No',1,0,NULL,NULL,NULL,4,1),(11,'Listado de años',1,NULL,'','','',NULL,1),(12,'2000',1,2000,NULL,NULL,NULL,11,1),(13,'2001',2,2001,NULL,NULL,NULL,11,1),(14,'2002',3,2002,NULL,NULL,NULL,11,1),(15,'2003',4,2003,NULL,NULL,NULL,11,1),(16,'2004',5,2004,NULL,NULL,NULL,11,1),(17,'2005',6,2005,NULL,NULL,NULL,11,1),(18,'2006',7,2006,NULL,NULL,NULL,11,1),(19,'2007',8,2007,NULL,NULL,NULL,11,1),(20,'2008',9,2008,NULL,NULL,NULL,11,1),(21,'2009',10,2009,NULL,NULL,NULL,11,1),(22,'2010',11,2010,NULL,NULL,NULL,11,1),(23,'2011',12,2011,NULL,NULL,NULL,11,1),(24,'2012',13,2012,NULL,NULL,NULL,11,1),(25,'2013',14,2013,NULL,NULL,NULL,11,1),(26,'2014',15,2014,NULL,NULL,NULL,11,1),(27,'2015',16,2015,NULL,NULL,NULL,11,1),(28,'2016',17,2016,NULL,NULL,NULL,11,1),(29,'2017',18,2017,NULL,NULL,NULL,11,1),(30,'2018',19,2018,NULL,NULL,NULL,11,1),(31,'2019',20,2019,NULL,NULL,NULL,11,1),(32,'2020',21,2020,NULL,NULL,NULL,11,1);

/*Table structure for table `clase` */

DROP TABLE IF EXISTS `clase`;

CREATE TABLE `clase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `clase` */

insert  into `clase`(`id`,`nombre`,`estado`,`created_at`,`updated_at`) values (1,'Vehículos Ligeros',1,1567480912,1567480912),(2,'Vehículos Pesados',1,1567480930,1567480930),(3,'Vehículos Especiales Y Agrícolas',1,1567480946,1567480946),(4,'Otros Vehículos',1,1567480961,1567480961);

/*Table structure for table `cliente` */

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_documento_id` int(11) DEFAULT NULL,
  `numero_documento` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `celular` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documento_cliente` (`tipo_documento_id`),
  KEY `usuario_cliente` (`user_id`),
  CONSTRAINT `documento_cliente` FOREIGN KEY (`tipo_documento_id`) REFERENCES `tipo_documento` (`id`),
  CONSTRAINT `usuario_cliente` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `cliente` */

insert  into `cliente`(`id`,`user_id`,`nombre`,`apellido`,`tipo_documento_id`,`numero_documento`,`direccion`,`telefono`,`celular`,`estado`,`created_at`,`updated_at`) values (1,8,'Luis Armando','Caiza Caiza',4,'1717761892001','Barrio Ponceano Calle Oe3J N71-339','025119084','0999074870',1,1567480528,1571599502),(2,9,'Mayra','Viracocha',5,'1234567890','','','0999074870',1,1568597276,1571599529),(3,10,'Jose','Castro',5,'1817584892','','023458584','0999074870',1,1568597476,1571599993),(4,11,'Maria','Mena',5,'1414785479','','024587487','0999074870',1,1568597534,1571599621);

/*Table structure for table `color` */

DROP TABLE IF EXISTS `color`;

CREATE TABLE `color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `color` */

insert  into `color`(`id`,`nombre`,`estado`,`created_at`,`updated_at`) values (1,'Blanco',1,1567481107,1567481107),(2,'Negro',1,1567481125,1567481125),(3,'Rojo',1,1567481134,1567481134);

/*Table structure for table `dato` */

DROP TABLE IF EXISTS `dato`;

CREATE TABLE `dato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_dato_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tipo_documento_id` int(11) DEFAULT NULL,
  `num_documento` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellido` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `celular` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foto` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_dato_dato` (`tipo_dato_id`),
  KEY `user_dato` (`user_id`),
  KEY `tipo_documento_dato` (`tipo_documento_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `dato` */

insert  into `dato`(`id`,`tipo_dato_id`,`user_id`,`tipo_documento_id`,`num_documento`,`nombre`,`apellido`,`telefono`,`celular`,`direccion`,`foto`) values (1,1,1,1,'1717761892','Luis','Caiza','5119084','0999074870','Barrio Ponceano Calle Oe3J',NULL),(2,2,2,NULL,'','Andres','Mosquera','','','','2.jpg'),(3,2,3,NULL,NULL,'Bernard','Andino',NULL,NULL,NULL,NULL),(4,2,4,NULL,NULL,'Aurora','Caiza',NULL,NULL,NULL,NULL),(5,1,5,NULL,'123456789','abc','abc','','','','5.jpg'),(6,3,6,2,'1234567892001','Seguros 123',NULL,'','','','6.jpg'),(7,3,7,2,'1234567812001','Broker abc',NULL,'','','','7.jpg');

/*Table structure for table `empleado` */

DROP TABLE IF EXISTS `empleado`;

CREATE TABLE `empleado` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellido` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `celular` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empleado_usuario` (`user_id`),
  CONSTRAINT `empleado_usuario` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `empleado` */

insert  into `empleado`(`id`,`user_id`,`nombre`,`apellido`,`telefono`,`celular`,`foto`,`estado`,`created_at`,`updated_at`) values (3,3,'Bernardo','Andino','','',NULL,1,1567479515,1568756898),(4,2,'Andres','Mosquera','','',NULL,1,1571586330,1571586330),(6,12,'Asesor','Uno','','',NULL,1,1571589161,1571589161),(7,14,'Asesor','Dos','','',NULL,1,1571589181,1571589181),(8,15,'Asesor','Tres','','',NULL,1,1571589202,1571589202),(9,16,'Jefe','Enderezado','','',NULL,1,1571589619,1571589619),(10,17,'Enderezado','Uno','','',NULL,1,1571589668,1571589668),(11,18,'Enderezado','Dos','','',NULL,1,1571589686,1571589686),(12,19,'Enderezado','Tres','','',NULL,1,1571589715,1571589715),(13,20,'Mecanica','Uno','','',NULL,1,1571589776,1571589776),(14,21,'Mecanica','Dos','','',NULL,1,1571590015,1571590015),(15,22,'Mecanica','Tres','','',NULL,1,1571590045,1571590045),(16,23,'Pintura','Uno','','',NULL,1,1571590120,1571590153),(17,24,'Pintura','Dos','','',NULL,1,1571590172,1571590172),(18,25,'Pintura','Tres','','',NULL,1,1571590196,1571590196),(19,26,'Armado','Uno','','',NULL,1,1571591850,1571591850),(20,27,'Trabajos','Uno','','',NULL,1,1571591884,1571591920),(21,28,'Pulido','Uno','','',NULL,1,1571591907,1571591907);

/*Table structure for table `empleado_sucursal` */

DROP TABLE IF EXISTS `empleado_sucursal`;

CREATE TABLE `empleado_sucursal` (
  `sucursal_id` int(11) NOT NULL,
  `empleado_id` int(11) NOT NULL,
  `por_defecto` tinyint(1) NOT NULL DEFAULT '1',
  `proceso_id` int(11) NOT NULL,
  `encargado_proceso` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sucursal_id`,`empleado_id`,`proceso_id`),
  KEY `f3` (`empleado_id`),
  KEY `f2` (`proceso_id`),
  CONSTRAINT `f1` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursal` (`id`),
  CONSTRAINT `f2` FOREIGN KEY (`proceso_id`) REFERENCES `proceso` (`id`),
  CONSTRAINT `f3` FOREIGN KEY (`empleado_id`) REFERENCES `empleado` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `empleado_sucursal` */

insert  into `empleado_sucursal`(`sucursal_id`,`empleado_id`,`por_defecto`,`proceso_id`,`encargado_proceso`) values (1,3,1,9,1),(1,4,1,10,1),(1,6,1,7,0),(1,6,0,8,0),(1,7,1,7,0),(1,7,0,8,0),(1,8,0,7,0),(1,8,1,8,0),(1,9,1,2,1),(1,10,1,2,0),(1,11,1,2,0),(1,12,1,2,0),(1,13,1,1,0),(1,14,1,1,0),(1,15,1,1,0),(1,16,1,3,0),(1,17,1,3,0),(1,18,1,3,0),(1,19,1,4,0),(1,20,1,5,0),(1,21,1,6,0);

/*Table structure for table `empresa` */

DROP TABLE IF EXISTS `empresa`;

CREATE TABLE `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `siglas` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `celular` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wathshap` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `web` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `empresa` */

insert  into `empresa`(`id`,`nombre`,`siglas`,`direccion`,`telefono`,`celular`,`wathshap`,`web`,`email`,`logo`,`estado`,`created_at`,`updated_at`) values (2,'AUTOS ALC AUTO / LINEA / COLOR','AUTOS ALC S.A.','Capitán Ramón Borja OE2-229 Y Av. Galo Plaza Lasso (AV. 10 de agosto)','022410787','022402992','','http://www.autosalc.com/','luica20@gmail.com','empresa20191020102210.jpg',1,1567479133,1571585178);

/*Table structure for table `galeria` */

DROP TABLE IF EXISTS `galeria`;

CREATE TABLE `galeria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `alt` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagen` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `galeria` */

/*Table structure for table `marca` */

DROP TABLE IF EXISTS `marca`;

CREATE TABLE `marca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `marca` */

insert  into `marca`(`id`,`nombre`,`estado`,`created_at`,`updated_at`) values (1,'CHEVROLET',1,1567480731,1571584232),(2,'HYUNDAI',1,1567481513,1571584240),(3,'TOYOTA',1,1571584343,1571584343),(4,'FORD',1,1571584357,1571584357),(5,'NISSAN',1,1571584368,1571584368),(6,'KIA',1,1571584383,1571584383),(7,'VOLKSWAGEN',1,1571584419,1571584419),(8,'MAZDA',1,1571584430,1571584430),(9,'SUSUKI',1,1571584442,1571584442),(10,'RENAULT',1,1571584454,1571584454),(11,'MITSUBISHI',1,1571584467,1571584467),(12,'GREAT WALL',1,1571584479,1571584479),(13,'BMW',1,1571584489,1571584489),(14,'MECEDES BENZ',1,1571584510,1571584510),(15,'PEUGEOT',1,1571584522,1571584522),(16,'HONDA',1,1571584532,1571584532),(17,'JEEP',1,1571584543,1571584543),(18,'SKODA',1,1571584553,1571584553),(19,'FIAT',1,1571584567,1571584567),(20,'CHERRY',1,1571584574,1571584574);

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob,
  `icon` varchar(250) DEFAULT NULL,
  `option` varchar(250) DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

/*Data for the table `menu` */

insert  into `menu`(`id`,`name`,`parent`,`route`,`order`,`data`,`icon`,`option`,`estado`) values (1,'Dashboard',NULL,'/site/index',1,NULL,'tachometer',NULL,1),(2,'Administracion',NULL,NULL,3,NULL,'wrench',NULL,1),(3,'Rutas',34,'/admin/route/index',1,NULL,'code-fork',NULL,1),(4,'Permisos',34,'/admin/permission/index',2,NULL,'lock',NULL,1),(5,'Roles',34,'/admin/role/index',3,NULL,'id-card',NULL,1),(6,'Asignaciones',34,'/admin/assignment/index',4,NULL,'key',NULL,1),(7,'Menu',34,'/menu/index',5,NULL,'bars',NULL,1),(8,'Catalogo',11,'/catalogo-general/index',1,NULL,'sort-amount-asc',NULL,1),(10,'Tipo Documento',11,'/tipo-documento/index',5,NULL,'address-book-o',NULL,1),(11,'Configuracion',NULL,NULL,4,NULL,'gears',NULL,1),(12,'Empresa',2,'/empresa/index',1,NULL,'home',NULL,1),(13,'Estado Orden',11,'/orden-estado/index',4,NULL,'random',NULL,1),(14,'Clientes',2,'/cliente/index',6,NULL,'id-card-o',NULL,1),(15,'Aseguradora',2,'/seguro/index',4,NULL,'gavel',NULL,1),(16,'Empleado',2,'/empleado/index',3,NULL,'user-o',NULL,1),(17,'Procesos',NULL,NULL,5,NULL,'folder-open',NULL,1),(18,'Ingreso Orden',17,'/orden/index',1,NULL,'file-text-o',NULL,1),(19,'Procesos',11,'/proceso/index',2,NULL,'gear',NULL,1),(20,'Asignacion',17,'/orden/asignacion',3,NULL,'handshake-o',NULL,1),(21,'Notificacion',17,'/orden/notificacion',5,NULL,'envelope-o',NULL,1),(22,'Reportes',NULL,NULL,6,NULL,'newspaper-o',NULL,0),(23,'Analisis Ordenes',22,'/admin/default/index',1,NULL,'bar-chart',NULL,0),(24,'Marca',31,'/marca/index',9,NULL,'',NULL,1),(25,'Modelo',31,'/modelo/index',10,NULL,'',NULL,1),(26,'Clase',31,'/clase/index',11,NULL,'',NULL,1),(27,'Pais',31,'/pais/index',12,NULL,'',NULL,1),(28,'Servicio',31,'/servicio/index',13,NULL,'',NULL,1),(29,'Color',31,'/color/index',14,NULL,'',NULL,1),(30,'Vehiculo',2,'/vehiculo/index',7,NULL,'',NULL,1),(31,'Vehiculos',11,NULL,6,NULL,'',NULL,1),(32,'Estado Procesos',11,'/proceso-estado/index',3,NULL,'',NULL,1),(33,'Broker',2,'/broker/index',5,NULL,'',NULL,1),(34,'Seguridad',NULL,NULL,2,NULL,'lock',NULL,1),(36,'Planificacion',17,'/orden/planificacion',2,NULL,NULL,NULL,1),(37,'Ejecucion',17,'/orden/ejecucion',4,NULL,'',NULL,1),(38,'Resumen Procesos',22,NULL,2,NULL,NULL,NULL,0);

/*Table structure for table `message` */

DROP TABLE IF EXISTS `message`;

CREATE TABLE `message` (
  `id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `translation` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`,`language`),
  CONSTRAINT `fk_source_message_message` FOREIGN KEY (`id`) REFERENCES `source_message` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `message` */

insert  into `message`(`id`,`language`,`translation`) values (15,'en',NULL),(15,'es',NULL),(16,'en',NULL),(16,'es',NULL),(17,'en',NULL),(17,'es',NULL),(18,'en',NULL),(18,'es',NULL),(19,'en',NULL),(19,'es',NULL),(20,'en',NULL),(20,'es',NULL),(21,'en',NULL),(21,'es',NULL),(22,'en',NULL),(22,'es',NULL),(23,'en',NULL),(23,'es',NULL),(24,'en',NULL),(24,'es',NULL),(25,'en',NULL),(25,'es',NULL),(26,'en',NULL),(26,'es',NULL),(27,'en',NULL),(27,'es',NULL),(28,'en',NULL),(28,'es',NULL);

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `migration` */

/*Table structure for table `modelo` */

DROP TABLE IF EXISTS `modelo`;

CREATE TABLE `modelo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marca_id` int(11) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `modelo_marca` (`marca_id`),
  CONSTRAINT `modelo_marca` FOREIGN KEY (`marca_id`) REFERENCES `marca` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `modelo` */

insert  into `modelo`(`id`,`marca_id`,`nombre`,`estado`,`created_at`,`updated_at`) values (1,1,'AVEO',1,1567480746,1571584647),(2,1,'SAIL',1,1567481542,1571584667),(3,1,'GRAND VITARA 5P',1,1571584694,1571584694),(4,1,'CORZA',1,1571584730,1571584730),(5,8,'BT-50',1,1571584741,1571584764),(6,8,'2',1,1571584774,1571584774),(7,8,'3',1,1571584794,1571584794),(8,8,'323',1,1571584803,1571584803);

/*Table structure for table `orden` */

DROP TABLE IF EXISTS `orden`;

CREATE TABLE `orden` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sucursal_id` int(11) DEFAULT NULL,
  `numero_orden` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'secuencial por sucursal',
  `broker_id` int(11) DEFAULT NULL COMMENT 'broker',
  `seguro_id` int(11) DEFAULT NULL COMMENT 'aseguradora',
  `cliente_id` int(11) NOT NULL COMMENT 'cliente',
  `vehiculo_id` int(11) NOT NULL COMMENT 'vehiculo cliente',
  `empleado_recibe_id` int(11) DEFAULT NULL COMMENT 'usuario recibe',
  `fecha_recibe` datetime NOT NULL COMMENT 'fecha de ingreso',
  `fecha_salida` date NOT NULL COMMENT 'fecha tentativa de salida',
  `empleado_revisa_id` int(11) DEFAULT NULL,
  `fecha_revisa` datetime DEFAULT NULL,
  `empleado_entrega_id` int(11) DEFAULT NULL COMMENT 'usuario entrega',
  `fecha_entrega` datetime DEFAULT NULL COMMENT 'fecha de entrega real',
  `observacion_ingreso` text COLLATE utf8_unicode_ci COMMENT 'observacion al recibir',
  `observacion_entrega` text COLLATE utf8_unicode_ci COMMENT 'observacion al entregar',
  `avance` int(11) NOT NULL DEFAULT '0',
  `estado_id` int(11) NOT NULL COMMENT 'estado de la orden',
  `numero_servicio` int(11) DEFAULT '0' COMMENT 'total de numeros de servicios',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `estado_orden` (`estado_id`),
  KEY `broker_orden` (`broker_id`),
  KEY `seguro_orden` (`seguro_id`),
  KEY `emp_recibe_orden` (`empleado_recibe_id`),
  KEY `emp_entrega_orden` (`empleado_entrega_id`),
  KEY `cliente_orden` (`cliente_id`),
  KEY `emp_revisa_orden` (`empleado_revisa_id`),
  CONSTRAINT `broker_orden` FOREIGN KEY (`broker_id`) REFERENCES `broker` (`id`),
  CONSTRAINT `cliente_orden` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`),
  CONSTRAINT `emp_entrega_orden` FOREIGN KEY (`empleado_entrega_id`) REFERENCES `empleado` (`id`),
  CONSTRAINT `emp_recibe_orden` FOREIGN KEY (`empleado_recibe_id`) REFERENCES `empleado` (`id`),
  CONSTRAINT `emp_revisa_orden` FOREIGN KEY (`empleado_revisa_id`) REFERENCES `empleado` (`id`),
  CONSTRAINT `estado_orden` FOREIGN KEY (`estado_id`) REFERENCES `orden_estado` (`id`),
  CONSTRAINT `seguro_orden` FOREIGN KEY (`seguro_id`) REFERENCES `seguro` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `orden` */

insert  into `orden`(`id`,`sucursal_id`,`numero_orden`,`broker_id`,`seguro_id`,`cliente_id`,`vehiculo_id`,`empleado_recibe_id`,`fecha_recibe`,`fecha_salida`,`empleado_revisa_id`,`fecha_revisa`,`empleado_entrega_id`,`fecha_entrega`,`observacion_ingreso`,`observacion_entrega`,`avance`,`estado_id`,`numero_servicio`,`created_at`,`updated_at`) values (1,1,'0000000005',1,1,1,1,4,'2019-12-22 20:23:19','2019-12-31',NULL,NULL,NULL,NULL,'observacion 1',NULL,0,1,2,1577046199,1577046199),(2,1,'0000000006',2,2,2,3,4,'2019-12-22 20:24:40','2020-01-10',4,'2019-12-22 21:34:11',NULL,NULL,'observacion 2',NULL,0,3,3,1577046280,1577050451),(3,1,'0000000007',3,3,4,5,6,'2019-12-22 20:25:30','2020-01-30',4,'2019-12-22 20:55:15',NULL,NULL,'mensaje',NULL,0,4,2,1577046330,1577048116),(4,1,'0000000008',NULL,1,4,5,6,'2019-12-22 20:44:03','2019-12-31',NULL,NULL,NULL,NULL,'',NULL,0,1,1,1577047443,1577047443);

/*Table structure for table `orden_estado` */

DROP TABLE IF EXISTS `orden_estado`;

CREATE TABLE `orden_estado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orden` int(11) NOT NULL DEFAULT '1',
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `orden_estado` */

insert  into `orden_estado`(`id`,`nombre`,`codigo`,`orden`,`estado`,`created_at`,`updated_at`) values (1,'Ingresada','INGR',1,1,1567482145,1567482145),(2,'Aprobada','APRO',2,1,1567482145,1567482145),(3,'Asignada','ASIG',3,1,1567482145,1567482145),(4,'En Proceso','EPRO',4,1,1567482145,1567482145),(5,'En Revision','EREV',5,1,1567482145,1567482145),(6,'Terminada','TERM',6,1,1567482145,1567482145),(7,'Cancelada','CANC',7,1,1567482145,1567482145);

/*Table structure for table `orden_notificacion` */

DROP TABLE IF EXISTS `orden_notificacion`;

CREATE TABLE `orden_notificacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orden_proceso_id` int(11) NOT NULL,
  `fecha_envio` datetime NOT NULL,
  `titulo` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `mensaje` text COLLATE utf8_unicode_ci NOT NULL,
  `observacion` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `para` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `copia` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adjunto` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ordenproceso_ordennotificacion` (`orden_proceso_id`),
  CONSTRAINT `ordenproceso_ordennotificacion` FOREIGN KEY (`orden_proceso_id`) REFERENCES `orden_proceso` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `orden_notificacion` */

/*Table structure for table `orden_proceso` */

DROP TABLE IF EXISTS `orden_proceso`;

CREATE TABLE `orden_proceso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orden_id` int(11) NOT NULL,
  `proceso_id` int(11) NOT NULL,
  `estado_id` int(11) DEFAULT NULL,
  `fase` int(11) DEFAULT '1',
  `responsable_id` int(11) DEFAULT NULL,
  `asignado_id` int(11) DEFAULT NULL,
  `fecha_inicio` datetime DEFAULT NULL,
  `observacion` text COLLATE utf8_unicode_ci NOT NULL,
  `fecha_fin` datetime DEFAULT NULL,
  `resultado` text COLLATE utf8_unicode_ci,
  `notificacion` int(11) NOT NULL DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `proceso_ordenproceso` (`proceso_id`),
  KEY `orden_ordenproceso` (`orden_id`),
  KEY `proceso_estado` (`estado_id`),
  CONSTRAINT `orden_ordenproceso` FOREIGN KEY (`orden_id`) REFERENCES `orden` (`id`),
  CONSTRAINT `proceso_estado` FOREIGN KEY (`estado_id`) REFERENCES `proceso_estado` (`id`),
  CONSTRAINT `proceso_ordenproceso` FOREIGN KEY (`proceso_id`) REFERENCES `proceso` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `orden_proceso` */

insert  into `orden_proceso`(`id`,`orden_id`,`proceso_id`,`estado_id`,`fase`,`responsable_id`,`asignado_id`,`fecha_inicio`,`observacion`,`fecha_fin`,`resultado`,`notificacion`,`created_at`,`updated_at`) values (1,1,1,1,1,NULL,NULL,NULL,'mecanica 1',NULL,NULL,0,1577046199,1577046199),(2,1,6,1,2,NULL,NULL,NULL,'pulido 1',NULL,NULL,0,1577046199,1577046199),(3,2,1,3,1,NULL,NULL,'2019-12-23 00:00:00','mensaje de trabajo a realizar','2019-12-27 00:00:00',NULL,0,1577046280,1577050451),(4,2,2,2,2,9,NULL,'2019-12-30 00:00:00','mensaje enderezado','2020-01-03 00:00:00',NULL,0,1577046280,1577050451),(5,2,3,3,3,NULL,NULL,'2020-01-06 00:00:00','observacion de pinrura','2020-01-10 00:00:00',NULL,0,1577046280,1577050451),(6,3,3,3,1,NULL,NULL,'2020-01-07 00:00:00','observacion','2020-01-17 00:00:00',NULL,0,1577046330,1577048116),(7,3,6,3,2,NULL,NULL,'2020-01-20 00:00:00','tres','2020-01-30 00:00:00',NULL,0,1577046346,1577048116),(8,4,2,1,1,NULL,NULL,NULL,'asd',NULL,NULL,0,1577047443,1577047443);

/*Table structure for table `orden_trabajo` */

DROP TABLE IF EXISTS `orden_trabajo`;

CREATE TABLE `orden_trabajo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orden_proceso_id` int(11) NOT NULL,
  `empleado_id` int(11) DEFAULT NULL,
  `estado_id` int(1) NOT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_fin` datetime DEFAULT NULL,
  `trabajo` text COLLATE utf8_unicode_ci,
  `resultado` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ordenproceso_ordentrabajo` (`orden_proceso_id`),
  CONSTRAINT `ordenproceso_ordentrabajo` FOREIGN KEY (`orden_proceso_id`) REFERENCES `orden_proceso` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `orden_trabajo` */

insert  into `orden_trabajo`(`id`,`orden_proceso_id`,`empleado_id`,`estado_id`,`fecha_inicio`,`fecha_fin`,`trabajo`,`resultado`,`created_at`,`updated_at`) values (1,6,NULL,1,'2020-01-07 00:00:00','2020-01-17 00:00:00',NULL,NULL,NULL,NULL),(2,7,NULL,1,'2020-01-20 00:00:00','2020-01-30 00:00:00',NULL,NULL,NULL,NULL),(3,3,NULL,1,'2019-12-23 00:00:00','2019-12-27 00:00:00',NULL,NULL,NULL,NULL),(4,5,NULL,1,'2020-01-06 00:00:00','2020-01-10 00:00:00',NULL,NULL,NULL,NULL),(7,4,NULL,1,'2019-12-30 00:00:00','2020-01-03 00:00:00',NULL,NULL,NULL,NULL);

/*Table structure for table `orden_trabajo_imagen` */

DROP TABLE IF EXISTS `orden_trabajo_imagen`;

CREATE TABLE `orden_trabajo_imagen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orden_trabajo_id` int(11) NOT NULL,
  `detalle` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagen` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `envio_email` tinyint(1) NOT NULL DEFAULT '0',
  `estado` tinyint(1) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ordentrabajoimagen_ordentrabajo` (`orden_trabajo_id`),
  CONSTRAINT `ordentrabajoimagen_ordentrabajo` FOREIGN KEY (`orden_trabajo_id`) REFERENCES `orden_trabajo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `orden_trabajo_imagen` */

/*Table structure for table `pais` */

DROP TABLE IF EXISTS `pais`;

CREATE TABLE `pais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `pais` */

insert  into `pais`(`id`,`nombre`,`estado`,`created_at`,`updated_at`) values (1,'CHINO',1,1567481063,1571584865),(2,'JAPON',1,1571584858,1571584858),(3,'ALEMAN',1,1571584873,1571584873);

/*Table structure for table `proceso` */

DROP TABLE IF EXISTS `proceso`;

CREATE TABLE `proceso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `detalle` text COLLATE utf8_unicode_ci,
  `orden` int(11) DEFAULT NULL,
  `imagen` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icono` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `publicar` tinyint(1) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `proceso` */

insert  into `proceso`(`id`,`nombre`,`detalle`,`orden`,`imagen`,`icono`,`estado`,`publicar`,`created_at`,`updated_at`) values (1,'Mecanica','\r\n',1,NULL,'gear',1,1,1567480731,1567480731),(2,'Enderezado','\r\n',2,NULL,NULL,1,1,1567480731,1567480731),(3,'Pintura',NULL,3,NULL,NULL,1,1,1567480731,1567480731),(4,'Armado',NULL,4,NULL,NULL,1,1,1567480731,1567480731),(5,'Trabajos Especiales',NULL,5,NULL,NULL,1,0,1567480731,1567480731),(6,'Pulido',NULL,6,NULL,NULL,1,1,1567480731,1567480731),(7,'Pre Entrega',NULL,7,NULL,NULL,1,1,1567480731,1567480731),(8,'Asesoria','',8,NULL,'',1,0,1571597120,1571597120),(9,'Gerencia','',9,NULL,'',1,0,1571597139,1571597139),(10,'Sistemas','',10,NULL,'',1,0,1571597161,1571597348),(11,'Administrativo','',11,NULL,'',0,0,1571597363,1571597363);

/*Table structure for table `proceso_estado` */

DROP TABLE IF EXISTS `proceso_estado`;

CREATE TABLE `proceso_estado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `proceso_estado` */

insert  into `proceso_estado`(`id`,`nombre`,`codigo`,`estado`,`created_at`,`updated_at`) values (1,'Pendiente','P',1,1567482145,1567482145),(2,'Asignado','A',1,1567482145,1567482145),(3,'En Proceso','E',1,1567482145,1567482145),(4,'Terminada','T',1,1567482145,1567482145),(5,'Cancelada','C',1,1567482145,1567482145);

/*Table structure for table `seccion` */

DROP TABLE IF EXISTS `seccion`;

CREATE TABLE `seccion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `detalle` text COLLATE utf8_unicode_ci,
  `estado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `seccion` */

/*Table structure for table `seguro` */

DROP TABLE IF EXISTS `seguro`;

CREATE TABLE `seguro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `ruc` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagen` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_seguro` (`user_id`),
  CONSTRAINT `usuario_seguro` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `seguro` */

insert  into `seguro`(`id`,`user_id`,`nombre`,`ruc`,`imagen`,`estado`,`created_at`,`updated_at`) values (1,5,'Aseguradora Del Sur','123456789001',NULL,1,1567480528,1571598104),(2,6,'Seguros Sucre','123456788001',NULL,1,1567480528,1571598148),(3,29,'Seguros Equinoccial','123456787001',NULL,1,1571598390,1571598390);

/*Table structure for table `servicio` */

DROP TABLE IF EXISTS `servicio`;

CREATE TABLE `servicio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `servicio` */

insert  into `servicio`(`id`,`nombre`,`estado`,`created_at`,`updated_at`) values (1,'Publico',1,1567481166,1567481166),(2,'Privado',1,1567481174,1567481174);

/*Table structure for table `source_message` */

DROP TABLE IF EXISTS `source_message`;

CREATE TABLE `source_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `source_message` */

insert  into `source_message`(`id`,`category`,`message`) values (15,'rbac-admin','ID'),(16,'rbac-admin','Name'),(17,'rbac-admin','Parent'),(18,'rbac-admin','Parent Name'),(19,'rbac-admin','Route'),(20,'rbac-admin','Order'),(21,'rbac-admin','Data'),(22,'rbac-admin','Menus'),(23,'rbac-admin','Create Menu'),(24,'rbac-admin','Icon'),(25,'rbac-admin','Update Menu'),(26,'rbac-admin','Update'),(27,'rbac-admin','Delete'),(28,'rbac-admin','Create');

/*Table structure for table `sucursal` */

DROP TABLE IF EXISTS `sucursal`;

CREATE TABLE `sucursal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id` int(11) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `ruc` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `celular` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secuencial` bigint(20) NOT NULL DEFAULT '1',
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_sucursal` (`empresa_id`),
  CONSTRAINT `empresa_sucursal` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `sucursal` */

insert  into `sucursal`(`id`,`empresa_id`,`nombre`,`ruc`,`telefono`,`celular`,`secuencial`,`estado`,`created_at`,`updated_at`) values (1,2,'AUTOS ALC S.A. BAKER II','1717761892001','025119084','0999074870',8,1,1567479179,1577047443);

/*Table structure for table `testimonio` */

DROP TABLE IF EXISTS `testimonio`;

CREATE TABLE `testimonio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `profesion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mensaje` text COLLATE utf8_unicode_ci NOT NULL,
  `anonimo` tinyint(1) NOT NULL DEFAULT '0',
  `imagen` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `calificacion` int(11) NOT NULL DEFAULT '5',
  `orden` int(11) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `testimonio` */

/*Table structure for table `tipo_dato` */

DROP TABLE IF EXISTS `tipo_dato`;

CREATE TABLE `tipo_dato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` bit(1) NOT NULL DEFAULT b'1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tipo_dato` */

/*Table structure for table `tipo_documento` */

DROP TABLE IF EXISTS `tipo_documento`;

CREATE TABLE `tipo_documento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tipo_documento` */

insert  into `tipo_documento`(`id`,`nombre`,`codigo`,`estado`,`created_at`,`updated_at`) values (4,'Ruc','RUC',1,1567480393,1567480393),(5,'Cedula','CI',1,1567480411,1567480411),(6,'Pasaporte','PASS',1,1567480459,1567480459);

/*Table structure for table `trabajo_estado` */

DROP TABLE IF EXISTS `trabajo_estado`;

CREATE TABLE `trabajo_estado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `trabajo_estado` */

insert  into `trabajo_estado`(`id`,`nombre`,`codigo`,`estado`,`created_at`,`updated_at`) values (1,'Pendiente','P',1,1567482145,1567482145),(2,'En Proceso','E',1,1567482145,1567482145),(3,'Terminada','T',1,1567482145,1567482145),(4,'Cancelada','C',1,1567482145,1567482145);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`created_at`,`updated_at`,`verification_token`) values (1,'lcaiza','o1ARbn0yfsAudGhYVgPkCWSqVqiC4vh_','$2y$13$.QSy1ORkYPGBUx74xi8mvuWJCtWOnC882qiBtIy5sMfUJ65q8XzWG',NULL,'lcaiza@aurysolutions.com',1,1559097844,1568756856,NULL),(2,'amosquera','TflDsdHpEf0XfyPAPcMNmAu53mAhajr3','$2y$13$.QMtU69etjrihjiQAcaTQelX0ihcJ/mxh40RILXlGNlCnL81WkcqS',NULL,'andres.mosquera@autosalc.com',1,1559098233,1568756917,NULL),(3,'bandino','v_BYMbbNM505mp_rEaglmZZ3KGjwftM5','$2y$13$dWjZvMSou/bzSkHYeqi/9u5vY/IHfjnlwpH361mkQeaiOnl5vx./2',NULL,'bernardo.andino@autosalc.com',1,1559098751,1568756898,NULL),(4,'acaiza','ltU5BIeQnPTwTcquj3W7-aM87ucl3lZs','$2y$13$U.1KnuogVO1cMbu1jrqiiuXMYVScw9ms1AFM9TAyHHZJfsRS9X0m.',NULL,'info@aurysolutions.com',1,1559098819,1568756882,NULL),(5,'ASEG-293255','wglPWDoY_glLCexxxTp5rkSp1wSvx2n9','$2y$13$TLL4XWDfGYr/UaPteVO20OVdMI2aOnDyRK15Y7rUyynEYuGmeuWO6',NULL,'abc@abc.com',1,1560397361,1571598104,'46Pd-3DT8RQXXspF0N6gF4irCHdGvFrk_1560397361'),(6,'SEGU-4DE947','ZS5nDU-ooVNVmOBlgXwi2rLbRdMeJ3tx','$2y$13$3XBi8/EllRz5p.TK82A15O5dDGJ6zpV5LRGUSRkMY6H3F0t2VMkAK',NULL,'seguro123@correo.com',1,1560431955,1571598148,'3sx9rWzetz_E-8NfUTS0L9ssif4O2EJg_1560431955'),(7,'AMB -8CB9E7','lzYp-duG_y_AJqvy2FBqYxYcdOe8Dfb_','$2y$13$dr1eZE69sjrx185906hKsOCWGm4pJz4Z87UMVe89oqXb.E1p3IV/C',NULL,'broker@correo.com',1,1560433038,1571598462,'DmmN5jssKS98WOoR2NJmXkKEuMW6_IkT_1560433038'),(8,'luica_20','UWd3ozqH4wAmE57ETkOy5Zdht1SievsO','$2y$13$YZ/7M/attcNzwh0J1bQhP.Evzj3l5zB3PQbd1wsmbibaqeUax9bUC',NULL,'luica_20@hotmail.com',1,1567480528,1571599502,NULL),(9,'mviracocha','I8X3684vQ6AFzYAWqwuc385ZYve8Z2GI','$2y$13$Ar.o9sM9UMK7mv.0qbzYrO4MEUlUs8DuhWWEc0eOs9nyWn0Lf1Vsq',NULL,'mayraviracocha@gmail.com',1,1568597276,1571599529,NULL),(10,'jcastro','KTh4-KwuV_WKbigJsNMaZlPl-ljg2X8U','$2y$13$N5.Ti8SaiOOvmXcThMKFm.bdQ3EvvsAuiIxHK6MIz5.UsDsNsa1SW',NULL,'josecastro@gmail.com',1,1568597476,1571599993,NULL),(11,'MARI-6656EB','6PIBR9aBXT8EqA2wFxk5vdx3f2JPFC37','$2y$13$zPIMSUy9uXgQBccar0Nd/u5/PmkvZDxOnjZxbfewBPF4h4iBIQjUq',NULL,'mariamena@gmail.com',1,1568597533,1571599621,NULL),(12,'asesor1','UvFRBJaDm-0ytZXVGh9zzuZAtjcSb16T','$2y$13$eLNLG9LVEegRbVam4t5/H.pK0P8Pu9EC90egGxHNHrQibkVKutakO',NULL,'asesor1@autosalc.com',1,1571589161,1571589161,NULL),(14,'asesor2','jiq5b7tTTmrmhgqdQHLZsCKqUIBIONNX','$2y$13$6k7a6WIs5GzzD1VAtXWYFeX6Z3WkrmeqXm.AHySm8p9ZMdNIWB7BK',NULL,'asesor2@autosalc.com',1,1571589181,1571589181,NULL),(15,'asesor3','n3Sn68WCmxTsFJhIncZULV6draLI-2El','$2y$13$I5Ce.nTMkbmyrq2at9cf8.PRY2MsEB6T7c7lk0jQ/uO94LkplqBey',NULL,'asesor3@autosalc.com',1,1571589202,1571589202,NULL),(16,'jefeenderezado','U_5PTSL0d0p8awvbQzEVY41NYMX9Fdq1','$2y$13$E5nXBnCSFrPyOf4nh9ZGneuZHFSgQ3dbHch/pofzYhGO6Sqy2ts5C',NULL,'jefeenderezado@autosalc.com',1,1571589619,1571589619,NULL),(17,'enderezado1','YlHerWCL4I2nrELJICJnNk1wEWy405Tk','$2y$13$UJdemFAJMlK/kGKYSx2HP.XXQ6P4OieeO2BhQdfS9T5BljBhg2twu',NULL,'enderezado1@autosalc.com',1,1571589668,1571589668,NULL),(18,'enderezado2','NeYH_4rx7-W4HbSDe6IHJyLBK6m01QJi','$2y$13$bidIHZH48dR.S6iJFjxGOOYI8Tcsej7OCu5.UAYCosABIShY2Hx4K',NULL,'enderezado2@autosalc.com',1,1571589686,1571589686,NULL),(19,'enderezado3','W8tVn482Vv6Shg6xOTGaZaq86bCQ7tJb','$2y$13$80VSrul8tElt6Y9tk8SxmOYn6/oBfZVStzlrYfIIBf.kOU9rwgdh2',NULL,'enderezado3@autosalc.com',1,1571589715,1571589715,NULL),(20,'mecanica1','p0R1D5RLqOAvCXcDo3klsgNrqPWvO8ZC','$2y$13$08fHWpl8xQJcDfb08EmmZOtgs4J43zmK0Ggv/ZNWdPRPSS05iDzo2',NULL,'mecanca1@autosalc.com',1,1571589776,1571589776,NULL),(21,'mecanica2','qL60_4cIcwDo3WxgqMF-eUTYhDg8kD9c','$2y$13$kYZaMtmfVhff4xpiyT4LO.MGoEFx.pk3CLI77qWeuQvNnN5k7I1WK',NULL,'mecanica2@autosalc.com',1,1571590015,1571590015,NULL),(22,'mecanica3','g2wZ9suwDMWdnhfy_yeEUiJvs4yK1gtK','$2y$13$Ccalb5.zIGv1bcASESNHnubE7yd6xuwcp5DIoN6td.fXSxmTsIbhW',NULL,'mecanica3@autosalc.com',1,1571590045,1571590045,NULL),(23,'pintura1','4UMWnEISmu0UguO8x7QtqllvJljRbdLK','$2y$13$jYV8Hz8dxACaapfKhJbaten7RetT5YaAGRZimzb08PdT5.2dSidti',NULL,'pintura1@autosalc.com',1,1571590085,1571590153,NULL),(24,'pintura2','zLZyrgYOiAJVNBBi8kt9uwO_5-qr1XHA','$2y$13$3f72tUvOrXz80mVJnlZKeeFwKqIMPoc6WfzpWqSboI/f7DZovcgCi',NULL,'pintura2@autosalc.com',1,1571590172,1571590172,NULL),(25,'pintura3','81kJ0E3RiqVqPgEGn2wsvu12pmHemWuE','$2y$13$4kzvRESYAu7htxzxcwjvsOuirjAPtc4759RLa4HlaESgUzxpj2PWe',NULL,'pintura3@autosalc.com',1,1571590196,1571590196,NULL),(26,'armado1','-h-xzX2r0KD8L3SuJ9-bYZwZpioxK7ug','$2y$13$BM6Riy6grGIJzQW08Fa0OuPNrHgSkFXDmGdzOZBwAtRDY3Ldq9msu',NULL,'armado1@autosalc.com',1,1571591850,1571591850,NULL),(27,'trabajos1','wWs6SeRT4pG8NPHIaUR34Ot1q8-0nifU','$2y$13$pJ0yEAGSusCY0hzbCXTQO.jeCWVkICDtmKeaJfsVic8fy2hTJscHy',NULL,'trabajos1@autosalc.com',1,1571591884,1571591920,NULL),(28,'pulido1','kfLdVUIfes_UOajHhENS-31HFlMcfpzJ','$2y$13$Um2.naGIyx2NziZ1uUjNle9OlO7GBlAhlUp4X/zNVZdLGEFhW95zu',NULL,'pulido1@autosalc.com',1,1571591907,1571591907,NULL),(29,'SEGU-512A38','oiqSHb2fbGmqVLmD0BTN3rt7SlFIo1ne','$2y$13$q0T0xoTfVL6PB7F625NyBe.kgc8NRSfaj2H4Aq7FH20vgOGBX8AUq',NULL,'equinoccial@correo.com',1,1571598390,1571598390,NULL),(30,'ASER-0C154A','QxARNxuyiOyZyjNZUUY8-gHtlDm_jR4J','$2y$13$9XYXw7PkJ3oqCmii.eEPPuDNueE2lJg72KGy1m1dyfyvC7vaIU7T.',NULL,'sertec@correo.com',1,1571598490,1571598490,NULL),(31,'ATLA-189B33','b7ODsYbvooFlYjjwKGJl_wOoWqz0m9u9','$2y$13$bBOywdaj6Fys8STDudV9fe/OPW9PplJK.vQVlBRAMCFzqSgwnxNzi',NULL,'atlantico@correo.com',1,1571598536,1571598536,NULL);

/*Table structure for table `user_gerarquia` */

DROP TABLE IF EXISTS `user_gerarquia`;

CREATE TABLE `user_gerarquia` (
  `user_jefe_id` int(11) NOT NULL,
  `user_encargado_id` int(11) NOT NULL,
  PRIMARY KEY (`user_jefe_id`,`user_encargado_id`),
  KEY `fk2` (`user_encargado_id`),
  CONSTRAINT `fk1` FOREIGN KEY (`user_jefe_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk2` FOREIGN KEY (`user_encargado_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user_gerarquia` */

insert  into `user_gerarquia`(`user_jefe_id`,`user_encargado_id`) values (2,12);

/*Table structure for table `vehiculo` */

DROP TABLE IF EXISTS `vehiculo`;

CREATE TABLE `vehiculo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_id` int(11) NOT NULL,
  `marca_id` int(11) DEFAULT NULL,
  `modelo_id` int(11) DEFAULT NULL,
  `pais_id` int(11) DEFAULT NULL,
  `clase_id` int(11) DEFAULT NULL,
  `servicio_id` int(11) DEFAULT NULL,
  `color_1_id` int(11) DEFAULT NULL,
  `color_2_id` int(11) DEFAULT NULL,
  `anio` int(11) DEFAULT NULL,
  `placa` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `cilindraje` int(11) DEFAULT NULL,
  `ramv_cpn` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `matriculado` tinyint(1) DEFAULT '1',
  `desde` date DEFAULT NULL,
  `hasta` date DEFAULT NULL,
  `informacion` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cliente_vehiculo` (`cliente_id`),
  KEY `marca_vehiculo` (`marca_id`),
  KEY `modelo_vehiculo` (`modelo_id`),
  KEY `pais_vehiculo` (`pais_id`),
  KEY `color1_vehiculo` (`color_1_id`),
  KEY `color2_vehiculo` (`color_2_id`),
  KEY `clase_vehiculo` (`clase_id`),
  KEY `servicio_vehiculo` (`servicio_id`),
  CONSTRAINT `clase_vehiculo` FOREIGN KEY (`clase_id`) REFERENCES `clase` (`id`),
  CONSTRAINT `cliente_vehiculo` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`),
  CONSTRAINT `color1_vehiculo` FOREIGN KEY (`color_1_id`) REFERENCES `color` (`id`),
  CONSTRAINT `color2_vehiculo` FOREIGN KEY (`color_2_id`) REFERENCES `color` (`id`),
  CONSTRAINT `marca_vehiculo` FOREIGN KEY (`marca_id`) REFERENCES `marca` (`id`),
  CONSTRAINT `modelo_vehiculo` FOREIGN KEY (`modelo_id`) REFERENCES `modelo` (`id`),
  CONSTRAINT `pais_vehiculo` FOREIGN KEY (`pais_id`) REFERENCES `pais` (`id`),
  CONSTRAINT `servicio_vehiculo` FOREIGN KEY (`servicio_id`) REFERENCES `servicio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `vehiculo` */

insert  into `vehiculo`(`id`,`cliente_id`,`marca_id`,`modelo_id`,`pais_id`,`clase_id`,`servicio_id`,`color_1_id`,`color_2_id`,`anio`,`placa`,`cilindraje`,`ramv_cpn`,`matriculado`,`desde`,`hasta`,`informacion`,`created_at`,`updated_at`) values (1,1,8,6,2,1,2,1,1,12,'PLD-0623',1300,'',1,'2018-03-15','2023-03-18','',1567481461,1571599860),(2,1,1,3,2,1,2,3,3,15,'LBX-0851',2400,'',1,'2019-02-28','2024-04-28','',1567481597,1571599918),(3,2,1,1,NULL,1,NULL,NULL,NULL,NULL,'ABC-1234',NULL,'',0,NULL,NULL,'',1568597296,1571600016),(4,3,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'ABV-452',NULL,'',0,NULL,NULL,'',1568603755,1568603755),(5,4,1,4,NULL,NULL,NULL,NULL,NULL,NULL,'POI-0254',NULL,'',0,NULL,NULL,'',1568603781,1571599943);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
